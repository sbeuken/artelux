#!/bin/bash

# Configuration 
project_name="artelux.dev"
local_path="/Users/sebastien/Documents/Workspace/artelux/"

remote_host="artelux.netline.be"
remote_path="/www/studiobreakfast/be/sebisme/artelux/htdocs/"

remote_chown_user="apache"
remote_chown_group="apache"

ldb_name="artelux"
ldb_user="artelux"
ldb_pass="yo6s15"

dump_dir="_db"
LSQL="/Applications/MAMP/Library/bin/mysql"
LSQLDUMP="/Applications/MAMP/Library/bin/mysqldump"

rdb_name="sebisme_artelux"
rdb_user="sebisme"
rdb_pass="alfocOaths"


echo "+------------------------------------------------+"
echo "|    DB LOCAL SAVE                               |"
echo "|    Migration dev/local                         |"
echo "|    vers prod/serveur                           |"
echo "+------------------------------------------------+"
echo "| Configuration :                                |"
echo "+------------------------------------------------+"
echo "+------------------------------------------------+"
echo "  Nom de la DB locale :"
echo "  $ldb_name"
echo
echo "  Utilisateur pour la db locale :"
echo "  $ldb_user"
echo
read -p "  Ces informations sont-elles correctes (o/n) ? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Oo]$ ]]
then
	echo "  Bye !"
	echo
	exit 1
else
	echo
	if [ ! -d "$local_path$dump_dir" ]; then
		mkdir $local_path$dump_dir
		echo "Le répertoire \"$dump_dir\" a été créé"
		echo
	else
		echo "Le répertoire \"$dump_dir\" est présent"
		echo
	fi
	echo "+------------------------------------------------+"
	echo "  Dump de la base de données locale :"
	echo "  COMMANDE -> ${LSQLDUMP} --opt --user=$ldb_user -p$ldb_pass $ldb_name > $local_path$dump_dir/database.sql"
	${LSQLDUMP} --opt --user=$ldb_user -p$ldb_pass $ldb_name > $local_path$dump_dir/database.sql
	echo "  Effectué"
fi
echo "+------------------------------------------------+"
echo "[Export achevé]"
echo
exit 0
