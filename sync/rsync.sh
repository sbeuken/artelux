#!/bin/bash

# Configuration 
project_name="artelux.dev"
local_path="/Users/seba/Documents/Workspace/artelux/"

remote_host="egg.studiobreakfast.be"
remote_path="/www/studiobreakfast/be/sebisme/artelux/htdocs/"

remote_chown_user="apache"
remote_chown_group="apache"

echo "+------------------------------------------------+"
echo "|    Sebisme                                     |"
echo "|    Migration dev/local                         |"
echo "|    RSYNC ONLY                                  |"
echo "+------------------------------------------------+"
echo "| Configuration :                                |"
echo "+------------------------------------------------+"
echo "  Nom du project (a titre indicatif) :"
echo "  $project_name"
echo
echo "  Chemin local du dossier à transférer :"
echo "  $local_path"
echo
echo "+------------------------------------------------+"
echo "  Adresse du serveur :"
echo "  $remote_host"
echo
echo "  Chemin sur le serveur :"
echo "  $remote_path"
echo
echo "  User/Group pour le chown :"
echo "  $remote_chown_user:$remote_chown_group"
echo "+------------------------------------------------+"
echo
read -p "  Ces informations sont-elles correctes (o/n) ? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Oo]$ ]]
then
	echo "  Bye !"
	echo
	exit 1
else
	echo
	
	echo "+------------------------------------------------+"
	echo "  Fichiers : rsync général"
	echo "  COMMANDE -> rsync --exclude-from=$local_path.rsync_exclude -avz -e ssh ./ root@$remote_host:$remote_path"
	rsync --exclude-from=$local_path.rsync_exclude -az -e ssh $local_path root@$remote_host:$remote_path
	echo "  Effectué"
	
	echo "+------------------------------------------------+"
	echo "  Gestion des droits (chown)"
	echo "  COMMANDE -> ssh root@$remote_host 'sh -s' < ./userfix.sh $remote_path $remote_chown_user $remote_chown_group"
	ssh root@$remote_host 'sh -s' < ./userfix.sh $remote_path $remote_chown_user $remote_chown_group
	echo "  Effectué"
	
fi
echo "+------------------------------------------------+"
echo "[Transfert achevé - Merci beaucoup, ce fut un plaisir]"
echo
exit 0
