#!/bin/bash

# Configuration 
project_name="artelux.dev"
local_path="/Users/sebastien/Documents/Workspace/artelux/"

remote_host="egg.studiobreakfast.be"
remote_path="/www/studiobreakfast/be/sebisme/artelux/htdocs/"

remote_chown_user="apache"
remote_chown_group="apache"

ldb_name="artelux"
ldb_user="artelux"
ldb_pass="yo6s15"

dump_dir="_db"
LSQL="/Applications/MAMP/Library/bin/mysql"
LSQLDUMP="/Applications/MAMP/Library/bin/mysqldump"

rdb_name="sebisme_artelux"
rdb_user="sebisme"
rdb_pass="alfocOaths"


echo "+------------------------------------------------+"
echo "|    Sebisme                                     |"
echo "|    Migration dev/local                         |"
echo "|    vers prod/serveur                           |"
echo "+------------------------------------------------+"
echo "| Configuration :                                |"
echo "+------------------------------------------------+"
echo "  Nom du project (a titre indicatif) :"
echo "  $project_name"
echo
echo "  Chemin local du dossier à transférer :"
echo "  $local_path"
echo
echo "  Chemin local pour dump de la DB : "
echo "  $local_path$dump_dir"
echo "+------------------------------------------------+"
echo "  Adresse du serveur :"
echo "  $remote_host"
echo
echo "  Chemin sur le serveur :"
echo "  $remote_path"
echo
echo "  User/Group pour le chown :"
echo "  $remote_chown_user:$remote_chown_group"
echo "+------------------------------------------------+"
echo "  Nom de la DB locale :"
echo "  $ldb_name"
echo
echo "  Utilisateur pour la db locale :"
echo "  $ldb_user"
echo "+------------------------------------------------+"
echo "  Nom de la DB distante :"
echo "  $rdb_name"
echo
echo "  Utilisateur pour la db distante :"
echo "  $rdb_user"
echo "+------------------------------------------------+"
echo
read -p "  Ces informations sont-elles correctes (o/n) ? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Oo]$ ]]
then
	echo "  Bye !"
	echo
	exit 1
else
	echo
	if [ ! -d "$local_path$dump_dir" ]; then
		mkdir $local_path$dump_dir
		echo "Le répertoire \"$dump_dir\" a été créé"
		echo
	else
		echo "Le répertoire \"$dump_dir\" est présent"
		echo
	fi
	echo "+------------------------------------------------+"
	echo "  Dump de la base de données locale :"
	echo "  COMMANDE -> ${LSQLDUMP} --opt --user=$ldb_user -p$ldb_pass $ldb_name > $local_path$dump_dir/database.sql"
	${LSQLDUMP} --opt --user=$ldb_user -p$ldb_pass $ldb_name > $local_path$dump_dir/database.sql
	echo "  Effectué"
	
	echo "+------------------------------------------------+"
	echo "  Fichiers : rsync général"
	echo "  COMMANDE -> rsync --exclude-from=$local_path.rsync_exclude -avz -e ssh ./ root@$remote_host:$remote_path"
	rsync --exclude-from=$local_path.rsync_exclude -az -e ssh $local_path root@$remote_host:$remote_path
	echo "  Effectué"
	
	echo "+------------------------------------------------+"
	echo "  Gestion des droits (chown)"
	echo "  COMMANDE -> ssh root@$remote_host 'sh -s' < ./userfix.sh $remote_path $remote_chown_user $remote_chown_group"
	ssh root@$remote_host 'sh -s' < ./userfix.sh $remote_path $remote_chown_user $remote_chown_group
	echo "  Effectué"
	
	echo "+------------------------------------------------+"
	echo "  Chargement SQL de la DB"
	echo "  COMMANDE -> ssh root@$remote_host 'sh -s' < ./reloadsql.sh $rdb_user $rdb_pass $rdb_name $remote_path$dump_dir/database.sql"
	ssh root@$remote_host 'sh -s' < ./reloadsql.sh $rdb_user $rdb_pass $rdb_name $remote_path$dump_dir/database.sql
	echo "  Effectué"
	
	echo "+------------------------------------------------+"
	echo "  Suppression dump SQL"
	echo "  COMMANDE -> rm -Rf $local_path$dump_dir/"
	echo "  COMMANDE -> ssh root@$remote_host rm -Rf $remote_path$dump_dir/"
	read -p "  Voulez-vous effectivement appliquer les deux commandes ci-dessus (o/n) ? " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Oo]$ ]]
	then
		rm -Rf $local_path$dump_dir/
		ssh root@$remote_host "rm -Rf $remote_path$dump_dir/"
		echo "  Effectué"
	else
		echo "  Les dossiers suivants n'ont pas été supprimés :"
		echo "  Local : $local_path$dump_dir/"
		echo "  Distant : $remote_path$dump_dir/"
	fi
	
fi
echo "+------------------------------------------------+"
echo "[Transfert achevé]"
echo
exit 0
