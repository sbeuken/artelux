$(document).ready(function() {
	// alert('done')
	// Fancybox for sitenat inpage galery
	$('.galerie-th').fancybox();
	$('.doc-dl').tooltip();
	$("#site-header").carousel({
		interval: 3500,
	}).bind('slide', function() {
		// console.log("On y va")
		$(".carousel-caption").fadeOut();
	}).bind('slid', function() {
		// console.log("On y est");
		$(".carousel-caption").fadeIn();
	});
});