<?php

/**
 * Config file for NSM Better Meta
 *
 * @package			NsmBetterMeta
 * @version			1.1.4
 * @author			Leevi Graham <http://leevigraham.com> - Technical Director, Newism
 * @copyright 		Copyright (c) 2007-2012 Newism <http://newism.com.au>
 * @license 		Commercial - please see LICENSE file included with this distribution
 * @link			http://ee-garage.com/nsm-better-meta
 */

$config['name'] = 'NSM Better Meta';
$config['version'] = '1.1.4';

$config['nsm_addon_updater']['versions_xml'] = 'http://ee-garage.com/nsm-better-meta/release-notes/feed';
