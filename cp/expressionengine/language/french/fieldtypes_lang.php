<?php

$lang = array(

'add_file'	=>
'Ajouter un fichier',

'remove_file' =>
'Supprimer un fichier',

'directory_no_access' =>
'Vous n\'avez pas accès au répertoire spécifié pour ce champ',

'directory' =>
'Répertoire :',

// IGNORE
''=>'');
/* End of file fieldtypes_lang.php */
/* Location: ./system/expressionengine/language/french/fieldtypes_lang.php */