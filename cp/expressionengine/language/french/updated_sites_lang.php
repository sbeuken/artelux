<?php

$lang = array(

/* ----------------------------------------
/*  Required for MODULES page
/* ----------------------------------------*/
'updated_sites_module_name' =>
'Sites MAJ',

'updated_sites_module_description' =>
'Ce module autorise d\'autres sites à vous envoyer des pings.',

/*----------------------------------------*/
'updated_sites_home' =>
'Tableau de Bord',

'unauthorized_action' =>
'Vous n\'êtes pas autorisé à effectuer cette action.',

'too_many_pings' =>
'Ping refusé. Vous avez déjà envoyé un ping dans les %X dernières minutes.',

'invalid_access' =>
'Accès invalide',

'updated_sites_configurations' =>
'Configurations des sites MAJ',

'updated_sites_config_name' =>
'Nom',

'updated_sites_short_name' =>
'Nom court',

'updated_sites_short_name_taken' =>
'Ce nom court est déjà utilisé.',

'single_word_no_spaces'	=>
'Un seul mot, sans espaces',

'updated_sites_config_url' =>
'URL',

'no_ping_configs' =>
'Aucune configuration des sites MAJ n\'existe pour le moment',

'updated_sites_delete_confirm' =>
'Supprimer les configurations des sites MAJ',

'updated_sites_deleted' =>
'Les configurations des sites MAJ ont été supprimées',

'updated_site_deleted' =>
'La configuration des sites MAJ a été supprimée',

'metaweblogs_deleted' =>
'Les configurations des sites MAJ ont été supprimées',

'updated_sites_delete_question' =>
'Êtes-vous sûr de vouloir supprimer la(les) configuration(s) des sites MAJ ?',

'delete' =>
'Supprimer',

'updated_sites_missing_fields' =>
'Un champ est resté vide, merci de le renseigner et de soumettre de nouveau.',

'new_config' =>
'Nouvelle configuration',

'modify_config' =>
'Éditer la configuration',

'configuration_options' =>
'Options de configuration',

'updated_sites_pref_name' =>
'Nom de la configuration',

'updated_sites_allowed' =>
'Sites autorisés à envoyer des pings',

'updated_sites_allowed_subtext' =>
'Afin qu\'un site puisse vous envoyer des pings, une partie de leur URL (par exemple leur nom de domaine) doit être dans cette liste. Merci de séparez chaque site par un saut de ligne.',

'updated_sites_prune' =>
'Nombre maximum de ping à enregistrer ?',

'configuration_created' =>
'Configuration créée',

'configuration_updated' =>
'Configuration mise à jour',

'updated_sites_create_new' =>
'Créer une nouvelle configuration',

'successful_ping' =>
'Ping reçu avec succès !',

'view_pings' =>
'Voir les pings',

'no_pings' =>
'Aucun ping',

'total_pings' =>
'Nombre total de ping',

'ping_name' =>
'Nom du site',

'ping_url' =>
'URL du site',

'ping_rss' =>
'RS du site',

'ping_date' =>
'Date du ping',

'delete_pings_confirm' =>
'Confirmation de suppression des pings',

'ping_delete_question' =>
'Êtes-vous sûr de vouloir supprimer le(s) ping(s) sélectionné(s) ?',

'pings_deleted' =>
'Ping(s) supprimé(s)',

'preference' =>
'Préférence',

'setting' =>
'Paramètre',


''=>''
);

/* End of file updated_sites_lang.php */
/* Location: ./system/expressionengine/language/french/updated_sites_lang.php */