<?php

$lang = array(

//----------------------------
// Log-in
//----------------------------

"remember_me" =>
"Connexion automatique à l'avenir ?",

"no_username" =>
"Le champ utilisateur est requis.",

"no_password" =>
"Le champ mot de passe est requis.",

"no_email" =>
"Vous devez saisir une adresse email.",

"credential_missmatch" =>
"Nom d'utilisateur ou mot de passe invalide.",

"multi_login_warning" =>
"Quelqu'un est déjà connecté avec ce compte.",

"return_to_login" =>
"Retourner à la connexion",

"password_lockout_in_effect" =>
"Vous n'êtes autorisé à effectuer que 4 tentatives de connexion toutes les %x minutes",

"unauthorized_request" =>
"Vous n'êtes pas autorisé à exécuter cette action",

'new_password_request' =>
'Demande de nouveau mot de passe',

'session_auto_timeout' =>
'Votre session a expiré en raison de son inactivité',

''=>''
);

/* End of file login_lang.php */
/* Location: ./system/expressionengine/language/french/login_lang.php */