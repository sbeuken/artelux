-- MySQL dump 10.13  Distrib 5.5.29, for osx10.6 (i386)
--
-- Host: localhost    Database: artelux
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `exp_accessories`
--

DROP TABLE IF EXISTS `exp_accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_accessories`
--

LOCK TABLES `exp_accessories` WRITE;
/*!40000 ALTER TABLE `exp_accessories` DISABLE KEYS */;
INSERT INTO `exp_accessories` VALUES (1,'Expressionengine_info_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.0'),(2,'Cp_analytics_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','2.0.9'),(3,'Structure_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','3.3.8'),(4,'Devotee_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.2.1'),(5,'Nsm_morphine_theme_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','2.0.3');
/*!40000 ALTER TABLE `exp_accessories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_actions`
--

DROP TABLE IF EXISTS `exp_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_actions`
--

LOCK TABLES `exp_actions` WRITE;
/*!40000 ALTER TABLE `exp_actions` DISABLE KEYS */;
INSERT INTO `exp_actions` VALUES (1,'Comment','insert_new_comment'),(2,'Comment_mcp','delete_comment_notification'),(3,'Comment','comment_subscribe'),(4,'Comment','edit_comment'),(5,'Email','send_email'),(6,'Safecracker','submit_entry'),(7,'Safecracker','combo_loader'),(8,'Search','do_search'),(9,'Channel','insert_new_entry'),(10,'Channel','filemanager_endpoint'),(11,'Channel','smiley_pop'),(12,'Member','registration_form'),(13,'Member','register_member'),(14,'Member','activate_member'),(15,'Member','member_login'),(16,'Member','member_logout'),(17,'Member','retrieve_password'),(18,'Member','reset_password'),(19,'Member','send_member_email'),(20,'Member','update_un_pw'),(21,'Member','member_search'),(22,'Member','member_delete'),(23,'Rte','get_js'),(24,'Assets_mcp','upload_file'),(25,'Assets_mcp','get_files_view_by_folders'),(26,'Assets_mcp','get_props'),(27,'Assets_mcp','save_props'),(28,'Assets_mcp','get_ordered_files_view'),(29,'Assets_mcp','get_session_id'),(30,'Assets_mcp','start_index'),(31,'Assets_mcp','perform_index'),(32,'Assets_mcp','finish_index'),(33,'Assets_mcp','get_s3_buckets'),(34,'Assets_mcp','move_folder'),(35,'Assets_mcp','rename_folder'),(36,'Assets_mcp','create_folder'),(37,'Assets_mcp','delete_folder'),(38,'Assets_mcp','view_file'),(39,'Assets_mcp','move_file'),(40,'Assets_mcp','delete_file'),(41,'Assets_mcp','view_thumbnail'),(42,'Assets_mcp','build_sheet'),(43,'Assets_mcp','get_selected_files'),(44,'Structure','ajax_move_set_data'),(45,'Playa_mcp','filter_entries');
/*!40000 ALTER TABLE `exp_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_assets_files`
--

DROP TABLE IF EXISTS `exp_assets_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_assets_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `folder_id` int(10) unsigned NOT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned DEFAULT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  `date_modified` int(10) unsigned DEFAULT NULL,
  `kind` varchar(5) DEFAULT NULL,
  `width` int(2) DEFAULT NULL,
  `height` int(2) DEFAULT NULL,
  `size` int(3) DEFAULT NULL,
  `search_keywords` text,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `unq_folder_id__file_name` (`folder_id`,`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_assets_files`
--

LOCK TABLES `exp_assets_files` WRITE;
/*!40000 ALTER TABLE `exp_assets_files` DISABLE KEYS */;
INSERT INTO `exp_assets_files` VALUES (1,4,'ee',NULL,3,'journal-lumineux-pro-05-20-03.jpg','Journal lumineux PRO 05 20 03',NULL,'Journal lumineux PRO 05 20 03',NULL,NULL,NULL,NULL,NULL,1359694039,'image',800,600,58156,'journal-lumineux-pro-05-20-03.jpg,Journal lumineux PRO 05 20 03,Journal lumineux PRO 05 20 03'),(2,5,'ee',NULL,4,'jl-indoor-totem-1.jpg','Journal lumineux - Totem',NULL,'Journal lumineux - Totem',NULL,NULL,NULL,NULL,NULL,1359696264,'image',600,800,84878,'jl-indoor-totem-1.jpg,Journal lumineux - Totem,Journal lumineux - Totem'),(3,3,'ee',NULL,2,'AffComm.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359696428,'pdf',NULL,NULL,930393,'AffComm.pdf'),(4,5,'ee',NULL,4,'080410161103_foto.jpg','Journal lumineux multilignes',NULL,NULL,NULL,NULL,'Journal lumineux multilignes',NULL,NULL,1359722419,'image',800,600,53043,'080410161103_foto.jpg,Journal lumineux multilignes,Journal lumineux multilignes'),(5,5,'ee',NULL,4,'080410154713_foto.jpg','Journal lumineux multilignes',NULL,'Journal lumineux multilignes',NULL,NULL,NULL,NULL,NULL,1359722419,'image',800,600,71932,'080410154713_foto.jpg,Journal lumineux multilignes,Journal lumineux multilignes'),(6,5,'ee',NULL,4,'260509095237_foto.jpg','Journal lumineux multilignes',NULL,'Journal lumineux multilignes',NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,18154,'260509095237_foto.jpg,Journal lumineux multilignes,Journal lumineux multilignes'),(7,5,'ee',NULL,4,'260509095803_foto.jpg','Display multiligne LED',NULL,'Display multiligne LED',NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,28244,'260509095803_foto.jpg,Display multiligne LED,Display multiligne LED'),(8,5,'ee',NULL,4,'260509100413_foto.jpg','Display multiligne LED',NULL,'Display multiligne LED',NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,22548,'260509100413_foto.jpg,Display multiligne LED,Display multiligne LED'),(9,5,'ee',NULL,4,'260509094801_foto.jpg','Journal lumineux multilignes',NULL,'Journal lumineux multilignes',NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,22548,'260509094801_foto.jpg,Journal lumineux multilignes,Journal lumineux multilignes'),(10,5,'ee',NULL,4,'260509100947_foto.jpg','Display multiligne LED',NULL,'Display multiligne LED',NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,327,29955,'260509100947_foto.jpg,Display multiligne LED,Display multiligne LED'),(11,5,'ee',NULL,4,'260509161235_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359722419,'image',360,480,39217,'260509161235_foto.jpg'),(12,5,'ee',NULL,4,'260509103644_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,17337,'260509103644_foto.jpg'),(13,5,'ee',NULL,4,'260509103149_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,21139,'260509103149_foto.jpg'),(14,5,'ee',NULL,4,'260509102525_foto.jpg','Display multiligne LED',NULL,'Display multiligne LED',NULL,NULL,NULL,NULL,NULL,1359722419,'image',400,300,23286,'260509102525_foto.jpg,Display multiligne LED,Display multiligne LED'),(15,5,'ee',NULL,4,'260509162243_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359722419,'image',360,480,34942,'260509162243_foto.jpg'),(16,5,'ee',NULL,4,'260509161712_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359722419,'image',360,480,23601,'260509161712_foto.jpg'),(17,5,'ee',NULL,4,'020809192926_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1359723068,'image',600,442,66477,'020809192926_foto.jpg'),(18,5,'ee',NULL,4,'score-1.jpg','Marquoir de score',NULL,'Marquoir de score',NULL,NULL,NULL,NULL,NULL,1359693060,'image',400,300,30835,'score-1.jpg,Marquoir de score,Marquoir de score'),(19,5,'ee',NULL,4,'score-2.jpg','Marquoir de score',NULL,'Marquoir de score',NULL,NULL,NULL,NULL,NULL,1359693060,'image',400,298,21870,'score-2.jpg,Marquoir de score,Marquoir de score'),(20,5,'ee',NULL,4,'score-3.jpg','Marquoir de score',NULL,'Marquoir de score',NULL,NULL,NULL,NULL,NULL,1359693060,'image',400,272,31771,'score-3.jpg,Marquoir de score,Marquoir de score'),(21,5,'ee',NULL,4,'score-4.jpg','Marquoir de score',NULL,'Marquoir de score',NULL,NULL,NULL,NULL,NULL,1359693060,'image',400,300,25955,'score-4.jpg,Marquoir de score,Marquoir de score'),(22,5,'ee',NULL,4,'score-5.jpg','Marquoir de score',NULL,'Marquoir de score',NULL,NULL,NULL,NULL,NULL,1359693060,'image',400,300,19427,'score-5.jpg,Marquoir de score,Marquoir de score'),(23,5,'ee',NULL,4,'score-6.jpg','Marquoir de score',NULL,'Marquoir de score',NULL,NULL,NULL,NULL,NULL,1359693060,'image',400,300,23998,'score-6.jpg,Marquoir de score,Marquoir de score'),(24,5,'ee',NULL,4,'240310130402_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363527417,'image',400,272,31771,'240310130402_foto.jpg'),(25,4,'ee',NULL,3,'240310130402_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363527448,'image',400,272,31771,'240310130402_foto.jpg'),(26,4,'ee',NULL,3,'220509132621_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363527990,'image',400,300,13326,'220509132621_foto.jpg'),(27,4,'ee',NULL,3,'220509132753_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363528073,'image',400,300,23998,'220509132753_foto.jpg'),(28,4,'ee',NULL,3,'020809192926_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363528106,'image',600,442,66477,'020809192926_foto.jpg'),(29,4,'ee',NULL,3,'040112194559_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363530063,'image',800,500,61623,'040112194559_foto.jpg'),(30,4,'ee',NULL,3,'220509140954_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363530405,'image',400,300,27181,'220509140954_foto.jpg'),(31,5,'ee',NULL,4,'ind_070410194322_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',576,768,91428,'ind_070410194322_foto.jpg'),(32,5,'ee',NULL,4,'ind_080410153945_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',576,768,76920,'ind_080410153945_foto.jpg'),(33,5,'ee',NULL,4,'ind_080410153900_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',800,600,66680,'ind_080410153900_foto.jpg'),(34,5,'ee',NULL,4,'ind_080410154032_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',400,495,53845,'ind_080410154032_foto.jpg'),(35,5,'ee',NULL,4,'ind_080410154528_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',800,600,74625,'ind_080410154528_foto.jpg'),(36,5,'ee',NULL,4,'ind_080410154713_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',800,600,71932,'ind_080410154713_foto.jpg'),(37,5,'ee',NULL,4,'ind_080410161103_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',800,600,53043,'ind_080410161103_foto.jpg'),(38,5,'ee',NULL,4,'ind_150213150703_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',345,459,36638,'ind_150213150703_foto.jpg'),(39,5,'ee',NULL,4,'ind_150213150446_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',800,451,44961,'ind_150213150446_foto.jpg'),(40,5,'ee',NULL,4,'ind_190509120414_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',497,362,30094,'ind_190509120414_foto.jpg'),(41,5,'ee',NULL,4,'ind_210310223650_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',800,600,73756,'ind_210310223650_foto.jpg'),(42,5,'ee',NULL,4,'ind_210310224722_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',512,768,71487,'ind_210310224722_foto.jpg'),(43,5,'ee',NULL,4,'ind_260509094801_foto.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363533424,'image',400,300,22548,'ind_260509094801_foto.jpg'),(44,1,'ee',NULL,1,'slider_01.jpg','Panneau LED Full color',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363894833,'image',870,160,24559,'slider_01.jpg,Panneau LED Full color'),(45,1,'ee',NULL,1,'slider_02.jpg','Panneau d\'affichage communal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363894833,'image',870,160,37545,'slider_02.jpg,Panneau d\'affichage communal'),(46,1,'ee',NULL,1,'slider_03.jpg','Marquoirs de score',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363894833,'image',870,160,52129,'slider_03.jpg,Marquoirs de score'),(47,1,'ee',NULL,1,'slider_05.jpg','Enseigne pharmacie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363894906,'image',870,160,32320,'slider_05.jpg,Enseigne pharmacie'),(48,1,'ee',NULL,1,'slider_04.jpg','Enseigne de pharmacie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363894833,'image',870,160,34046,'slider_04.jpg,Enseigne de pharmacie'),(49,1,'ee',NULL,1,'slider_03_1.jpg','Marquoirs de score',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363895697,'image',870,160,52129,'slider_03_1.jpg,Marquoirs de score'),(50,1,'ee',NULL,1,'slider_02_1.jpg','Panneau d\'affichage communal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363895697,'image',870,160,37545,'slider_02_1.jpg,Panneau d\'affichage communal'),(51,1,'ee',NULL,1,'slider_01_1.jpg','Panneau LED Full Color',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363895697,'image',870,160,25676,'slider_01_1.jpg,Panneau LED Full Color'),(52,1,'ee',NULL,1,'slider_04_1.jpg','Enseigne pharmacie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363895698,'image',870,160,34046,'slider_04_1.jpg,Enseigne pharmacie'),(53,1,'ee',NULL,1,'slider_05_1.jpg','Enseigne pharmacie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363895698,'image',870,160,32320,'slider_05_1.jpg,Enseigne pharmacie'),(54,1,'ee',NULL,1,'slider_06.jpg','Artelux - Ecran Led',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1363919901,'image',870,160,30737,'slider_06.jpg,Artelux - Ecran Led');
/*!40000 ALTER TABLE `exp_assets_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_assets_folders`
--

DROP TABLE IF EXISTS `exp_assets_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_assets_folders` (
  `folder_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `folder_name` varchar(255) NOT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`folder_id`),
  UNIQUE KEY `unq_source_type__source_id__filedir_id__parent_id__folder_name` (`source_type`,`source_id`,`filedir_id`,`parent_id`,`folder_name`),
  UNIQUE KEY `unq_source_type__source_id__filedir_id__full_path` (`source_type`,`source_id`,`filedir_id`,`full_path`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_assets_folders`
--

LOCK TABLES `exp_assets_folders` WRITE;
/*!40000 ALTER TABLE `exp_assets_folders` DISABLE KEYS */;
INSERT INTO `exp_assets_folders` VALUES (1,'ee','Global (Images)','',NULL,NULL,1),(2,'ee','Actualités','',NULL,NULL,6),(3,'ee','Global (Documents)','',NULL,NULL,2),(4,'ee','Items (Produits)','',NULL,NULL,3),(5,'ee','Produits','',NULL,NULL,4),(6,'ee','Services','',NULL,NULL,5);
/*!40000 ALTER TABLE `exp_assets_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_assets_index_data`
--

DROP TABLE IF EXISTS `exp_assets_index_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_assets_index_data` (
  `session_id` char(36) DEFAULT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `offset` int(10) unsigned DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `filesize` int(10) unsigned DEFAULT NULL,
  `type` enum('file','folder') DEFAULT NULL,
  `record_id` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `unq__session_id__source_type__source_id__offset` (`session_id`,`source_type`,`source_id`,`offset`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_assets_index_data`
--

LOCK TABLES `exp_assets_index_data` WRITE;
/*!40000 ALTER TABLE `exp_assets_index_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_assets_index_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_assets_selections`
--

DROP TABLE IF EXISTS `exp_assets_selections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_assets_selections` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `sort_order` int(4) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  KEY `file_id` (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_assets_selections`
--

LOCK TABLES `exp_assets_selections` WRITE;
/*!40000 ALTER TABLE `exp_assets_selections` DISABLE KEYS */;
INSERT INTO `exp_assets_selections` VALUES (1,10,55,NULL,NULL,NULL,0,0),(4,10,56,NULL,NULL,NULL,2,0),(2,9,22,NULL,NULL,NULL,0,0),(14,9,23,NULL,NULL,NULL,7,0),(10,9,23,NULL,NULL,NULL,6,0),(8,9,23,NULL,NULL,NULL,5,0),(7,9,23,NULL,NULL,NULL,4,0),(6,9,23,NULL,NULL,NULL,3,0),(9,9,23,NULL,NULL,NULL,2,0),(4,9,23,NULL,NULL,NULL,1,0),(5,9,23,NULL,NULL,NULL,0,0),(17,11,22,NULL,NULL,NULL,0,0),(23,11,23,NULL,NULL,NULL,5,0),(22,11,23,NULL,NULL,NULL,4,0),(21,11,23,NULL,NULL,NULL,3,0),(20,11,23,NULL,NULL,NULL,2,0),(19,11,23,NULL,NULL,NULL,1,0),(18,11,23,NULL,NULL,NULL,0,0),(2,12,55,NULL,NULL,NULL,0,0),(2,10,56,NULL,NULL,NULL,1,0),(1,10,56,NULL,NULL,NULL,0,0),(25,17,55,NULL,NULL,NULL,0,0),(25,17,56,NULL,NULL,NULL,0,0),(26,18,55,NULL,NULL,NULL,0,0),(27,18,56,NULL,NULL,NULL,2,0),(26,18,56,NULL,NULL,NULL,1,0),(28,18,56,NULL,NULL,NULL,0,0),(29,19,55,NULL,NULL,NULL,0,0),(29,19,56,NULL,NULL,NULL,0,0),(30,20,55,NULL,NULL,NULL,0,0),(30,20,56,NULL,NULL,NULL,0,0),(31,13,22,NULL,NULL,NULL,0,0),(43,13,23,NULL,NULL,NULL,12,0),(42,13,23,NULL,NULL,NULL,11,0),(41,13,23,NULL,NULL,NULL,10,0),(40,13,23,NULL,NULL,NULL,9,0),(38,13,23,NULL,NULL,NULL,8,0),(39,13,23,NULL,NULL,NULL,7,0),(37,13,23,NULL,NULL,NULL,6,0),(36,13,23,NULL,NULL,NULL,5,0),(35,13,23,NULL,NULL,NULL,4,0),(34,13,23,NULL,NULL,NULL,3,0),(32,13,23,NULL,NULL,NULL,2,0),(33,13,23,NULL,NULL,NULL,1,0),(31,13,23,NULL,NULL,NULL,0,0),(53,25,67,NULL,NULL,NULL,4,0),(52,25,67,NULL,NULL,NULL,3,0),(49,25,67,NULL,NULL,NULL,2,0),(50,25,67,NULL,NULL,NULL,1,0),(54,25,67,NULL,NULL,NULL,0,0);
/*!40000 ALTER TABLE `exp_assets_selections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_assets_sources`
--

DROP TABLE IF EXISTS `exp_assets_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_assets_sources` (
  `source_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 's3',
  `name` varchar(255) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  PRIMARY KEY (`source_id`),
  UNIQUE KEY `unq_source_type__source_id` (`source_type`,`source_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_assets_sources`
--

LOCK TABLES `exp_assets_sources` WRITE;
/*!40000 ALTER TABLE `exp_assets_sources` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_assets_sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_captcha`
--

DROP TABLE IF EXISTS `exp_captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_captcha`
--

LOCK TABLES `exp_captcha` WRITE;
/*!40000 ALTER TABLE `exp_captcha` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_captcha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_categories`
--

DROP TABLE IF EXISTS `exp_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_categories`
--

LOCK TABLES `exp_categories` WRITE;
/*!40000 ALTER TABLE `exp_categories` DISABLE KEYS */;
INSERT INTO `exp_categories` VALUES (1,1,1,0,'Journaux lumineux et displays','journaux-lumineux-et-displays','','0',10),(2,1,1,0,'Marquoirs de score','marquoirs-de-score','','0',11),(3,1,1,0,'Affiicheurs de sécurité industriels','affiicheurs-de-securite-industriels','','0',4),(4,1,1,0,'Ecrans géants à diodes full color','ecrans-geants-a-diodes-full-color','','0',8),(5,1,1,0,'Signalisation routière et de parking','signalisation-routiere-et-de-parking','','0',13),(6,1,1,0,'Affichage prix carburants','affichage-prix-carburants','','0',1),(7,1,1,0,'Croix pour pharmacies','croix-pour-pharmacies','','0',6),(8,1,1,0,'Ecrans graphiques info ville - full Color','ecrans-graphiques-info-ville-full-color','','0',9),(9,1,1,0,'Afficheurs électroniques d\'informations communales','afficheurs-electroniques-informations-communales','','0',2),(10,1,1,0,'Ecrans d\'affichage heure et température','ecrans-affichage-heure-et-temperature','','0',7),(11,1,1,0,'Radars préventifs','radars-preventifs','','0',12),(12,1,1,0,'Zone 30 écrans','zone-30-ecrans','','0',14),(13,1,1,0,'Afficheurs sur mesure et divers','afficheurs-sur-mesure-et-divers','','0',3),(14,1,1,0,'City Information and Prevention System (CIPS)','city-information-and-prevention-system-cips','','0',5);
/*!40000 ALTER TABLE `exp_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_field_data`
--

DROP TABLE IF EXISTS `exp_category_field_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_field_data`
--

LOCK TABLES `exp_category_field_data` WRITE;
/*!40000 ALTER TABLE `exp_category_field_data` DISABLE KEYS */;
INSERT INTO `exp_category_field_data` VALUES (1,1,1),(2,1,1),(3,1,1),(4,1,1),(5,1,1),(6,1,1),(7,1,1),(8,1,1),(9,1,1),(10,1,1),(11,1,1),(12,1,1),(13,1,1),(14,1,1);
/*!40000 ALTER TABLE `exp_category_field_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_fields`
--

DROP TABLE IF EXISTS `exp_category_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_fields`
--

LOCK TABLES `exp_category_fields` WRITE;
/*!40000 ALTER TABLE `exp_category_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_category_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_groups`
--

DROP TABLE IF EXISTS `exp_category_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_groups`
--

LOCK TABLES `exp_category_groups` WRITE;
/*!40000 ALTER TABLE `exp_category_groups` DISABLE KEYS */;
INSERT INTO `exp_category_groups` VALUES (1,1,'Produits','a',0,'all','6','6');
/*!40000 ALTER TABLE `exp_category_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_category_posts`
--

DROP TABLE IF EXISTS `exp_category_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_category_posts`
--

LOCK TABLES `exp_category_posts` WRITE;
/*!40000 ALTER TABLE `exp_category_posts` DISABLE KEYS */;
INSERT INTO `exp_category_posts` VALUES (10,1),(12,1),(17,2),(18,2);
/*!40000 ALTER TABLE `exp_category_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_data`
--

DROP TABLE IF EXISTS `exp_channel_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` text,
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_36` text,
  `field_ft_36` tinytext,
  `field_id_37` text,
  `field_ft_37` tinytext,
  `field_id_38` text,
  `field_ft_38` tinytext,
  `field_id_39` text,
  `field_ft_39` tinytext,
  `field_id_40` text,
  `field_ft_40` tinytext,
  `field_id_41` text,
  `field_ft_41` tinytext,
  `field_id_42` text,
  `field_ft_42` tinytext,
  `field_id_43` text,
  `field_ft_43` tinytext,
  `field_id_44` text,
  `field_ft_44` tinytext,
  `field_id_45` text,
  `field_ft_45` tinytext,
  `field_id_46` text,
  `field_ft_46` tinytext,
  `field_id_47` text,
  `field_ft_47` tinytext,
  `field_id_48` text,
  `field_ft_48` tinytext,
  `field_id_49` text,
  `field_ft_49` tinytext,
  `field_id_50` text,
  `field_ft_50` tinytext,
  `field_id_51` text,
  `field_ft_51` tinytext,
  `field_id_52` text,
  `field_ft_52` tinytext,
  `field_id_53` text,
  `field_ft_53` tinytext,
  `field_id_54` text,
  `field_ft_54` tinytext,
  `field_id_55` text,
  `field_ft_55` tinytext,
  `field_id_56` text,
  `field_ft_56` tinytext,
  `field_id_57` text,
  `field_ft_57` tinytext,
  `field_id_58` text,
  `field_ft_58` tinytext,
  `field_id_59` text,
  `field_ft_59` tinytext,
  `field_id_60` text,
  `field_ft_60` tinytext,
  `field_id_61` text,
  `field_ft_61` tinytext,
  `field_id_62` text,
  `field_ft_62` tinytext,
  `field_id_63` text,
  `field_ft_63` tinytext,
  `field_id_64` text,
  `field_ft_64` tinytext,
  `field_id_65` text,
  `field_ft_65` tinytext,
  `field_id_66` text,
  `field_ft_66` tinytext,
  `field_id_67` text,
  `field_ft_67` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_data`
--

LOCK TABLES `exp_channel_data` WRITE;
/*!40000 ALTER TABLE `exp_channel_data` DISABLE KEYS */;
INSERT INTO `exp_channel_data` VALUES (3,1,1,'','xhtml','','xhtml','','xhtml','<h1 class=\"embossed-on-lightgrey\">\n	Donnez vie &agrave; vos messages</h1>\n<p>\n	Artelux con&ccedil;oit, d&eacute;veloppe et commercialise des solutions compl&egrave;tes d&rsquo;affichage dynamique pour:</p>\n<ul>\n	<li>\n		l&rsquo;accueil, la communication interne et la signal&eacute;tique des organisations publiques et priv&eacute;es.</li>\n	<li>\n		la communication ext&eacute;rieure des villes et communes (&eacute;crans g&eacute;ants), des sites industriels et tertiaires.</li>\n	<li>\n		la pr&eacute;vention de la vitesse excessive (radars pr&eacute;ventifs) et la sensibilisation des conducteurs (messages dynamiques)</li>\n</ul>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(4,1,1,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(5,1,1,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(6,1,1,'','xhtml','','xhtml','','xhtml','<p>\n	Artelux est une entreprise active !</p>\n<p>\n	Par le biais de ces actualit&eacute;s nous bvous informons quant &agrave; nos nouveaux produits, nos r&eacute;alisations remarquables ou encore au sujet des salons et foires commerciales auxquelles nous participons.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(7,1,1,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(8,1,1,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(9,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Journaux Lumineux et Displays</h1>\n<p>\n	Artelux vous propose une <strong>game &eacute;tendue</strong> de <strong>journaux lumineux et Displays</strong>.</p>\n<p>\n	Notre gamme de journaux lumineux est <strong>largement d&eacute;clinable</strong> en fonction des diverses utilisations fr&eacute;quemment sollicit&eacute;es. Notre catalogue propose des <strong>journaux lumineux multi-lignes</strong> ou des <strong>journaux lumineux mono-ligne</strong>, destin&eacute;s &agrave; des usages <strong>indoor</strong> ou <strong>outdoor</strong>.</p>\n<h2>\n	Journaux Lumineux d&#39;int&eacute;rieur</h2>\n<p>\n	Nos <strong>journaux lumineux int&eacute;rieurs</strong> (indoor) disposent, selon les d&eacute;clinaisons, d&#39;une ou plusieurs ligne de texte, d&#39;une taille de caract&egrave;re variant de 1,8 &agrave; 10cm. Ces <strong>&eacute;crans d&#39;affichage</strong> pour bulletins d&#39;int&eacute;rieur rel&egrave;vent des <strong>technologies les plus avanc&eacute;es</strong> et sont compos&eacute;s de <strong>mat&eacute;riaux haut-de-gamme</strong>. Ces <strong>&eacute;crans LED</strong> (pour&nbsp;Light-Emitting Diode, diode d&#39;&eacute;mission lumineuse) se r&eacute;v&egrave;lent le <strong>m&eacute;dia id&eacute;al</strong> pour, par exemple, la promotion d&#39;offres et d&#39;actions, ou encore pour proposer une <strong>signal&eacute;tique</strong> dynamique et &eacute;volutive.</p>\n<p>\n	Leurs <strong>effets graphiques</strong>, tels que clignotement, d&eacute;filement, ou effets plus complexes, captent inmanquablement le regard de vos visiteurs vers votre message d&#39;annonce, publicitaire ou informatif. Ils constituent par exemple l&#39;outil id&eacute;al en terme de <strong>publicit&eacute; sur le lieu de vente</strong> (PLV).</p>\n<p>\n	Artelux propose une vaste gamme de mod&egrave;le standards, mais nous pouvons &eacute;galement vous proposer des <strong>installation sur mesure</strong>.</p>\n<p>\n	Sur l&#39;ensemble de nos displays, vous pouvez tr&egrave;s simplement programmer vos annonces &agrave; l&#39;aide d&#39;un <strong>logiciel adapt&eacute;</strong>, ou par l&#39;usage d&#39;un <strong>clavier IR</strong>.</p>\n<h2>\n	Journaux Lumineux d&#39;ext&eacute;rieur</h2>\n<p>\n	Nos <strong>journaux lumineux d&#39;ext&eacute;rieur</strong>, vous offrent une ou plusieurs lignes de texte d&#39;une hauteur variant de 3,3 &agrave; 60cm. Ils constituent la <strong>solution parfaite</strong> pour la diffusion de vos <strong>messages publicitaires</strong>.</p>\n<p>\n	D&#39;une <strong>grande robustesse</strong>, pertinents et r&eacute;sistants dans <strong>toutes les situations climatiques</strong>, ces <strong>displays outdoors</strong> sont &eacute;galement compos&eacute;s de <strong>LED</strong> permettant un <strong>contraste adaptatif</strong> en fonction de l&#39;&eacute;clairage ambiant. Le niveau de <strong>luminescence</strong> est ainsi d&eacute;termin&eacute; en fonction de l&#39;environnement .</p>\n<p>\n	Pour le cas de nos <strong>journaux lumineux d&#39;ext&eacute;rieur</strong> &eacute;galement, outre notre large gamme de produits standards, nous pouvons vous proposer des <strong>solutions sur-mesure</strong> adapt&eacute;es aux besoins du contexte architectural ou urbanistique de leur implantation.</p>','none','','none','','none','','none','jl-indoor-totem-1.jpg','none','080410161103_foto.jpg\n080410154713_foto.jpg\n260509095237_foto.jpg\n260509095803_foto.jpg\n260509100413_foto.jpg\n260509094801_foto.jpg\n260509100947_foto.jpg\n260509102525_foto.jpg','none','1','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','Notre catalogue propose des&nbsp;<b>journaux lumineux multi-lignes</b>&nbsp;ou des&nbsp;<b>journaux lumineux&nbsp;</b><b>mono-ligne</b>, destinés à des usages&nbsp;<b>indoor</b>&nbsp;ou&nbsp;<b>outdoor</b>.','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(10,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Journal lumineux PRO 05.20.03</h1>\n<p>\n	Ce mod&egrave;le de journal lumineux s&#39;adapte parfaitement dans un mobilier contemporain, tel qu&#39;au coeur d&#39;une salle d&#39;attente bancaire.</p>\n<p>\n	Il servira alors &agrave; afficher un message type de bienvenue.</p>','none','','none','','none','','none','<ul>\n	<li>\n		<strong>Mod&egrave;le</strong> :&nbsp;PRO 05.20.03</li>\n	<li>\n		<strong>Hauteur de caract&egrave;res</strong> : 3 cm</li>\n	<li>\n		<strong>Nombre de caract&egrave;res&nbsp;</strong>: 20</li>\n	<li>\n		<strong>Nombre de lignes&nbsp;</strong>: 5</li>\n	<li>\n		<strong>Couleur de caract&egrave;res&nbsp;</strong>: Normal Bright Green</li>\n</ul>','none','','none','','none','','none','journal-lumineux-pro-05-20-03.jpg','none','journal-lumineux-pro-05-20-03.jpg\njl-indoor-totem-1.jpg\n080410161103_foto.jpg','none','','none','[9] [journaux-lumineux-et-displays] Journaux lumineux et displays','none','','xhtml','','xhtml','','xhtml','','xhtml','Ce modèle de journal lumineux s\'adapte parfaitement dans un mobilier contemporain, tel qu\'au coeur d\'une salle d\'attente bancaire.<p>Il servira alors à afficher un message type de bienvenue.</p>','xhtml','','xhtml','','xhtml','','xhtml','','none'),(11,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Les Marquoirs de Scores</h1>\n<p>\n	Un <strong>club sportif </strong>digne de ce nom souhaite fournir &agrave; ses supporters l&#39;information la plus limpide durant une rencontre sportive. Nos <strong>marquoirs de scores</strong> sont l&#39;outil indispensable pour leur permettre de suivre la partie sans &eacute;quivoque.</p>\n<p>\n	Ces marquoirs vont du <strong>panneau d&#39;affichage &agrave; diode</strong> &agrave; l&#39;<strong>&eacute;cran led full color</strong> ou &agrave; l<strong>&#39;afficheur lumineux</strong> standard. Nos mod&egrave;les d&#39;entr&eacute;e de gamme vous permettent un affichage simple et limpide de l&#39;&eacute;volution des scores durant le jeu.</p>\n<p>\n	Gr&acirc;ce &agrave; nos mod&egrave;les haut-de-gamme, les <strong>&eacute;crans d&#39;affichage &agrave; diode full color</strong>, vous permettent en outre d&#39;afficher des logos, ou des <strong>s&eacute;quences vid&eacute;os</strong>. Un partenariat de <strong>sponsoring</strong> &agrave; honorer ? Vous pourrez proposer &agrave; votre sponsor de diffuser son spot publicitaire &agrave; la mi-temps. Ce type de produit permet donc d&#39;engranger un <strong>retour sur investissement</strong> et une r&eacute;elle rentabilit&eacute;</p>','none','','none','','none','','none','020809192926_foto.jpg','none','score-1.jpg\nscore-2.jpg\nscore-3.jpg\nscore-4.jpg\nscore-5.jpg\nscore-6.jpg','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>​Les marquoirs de score sont des afficheurs led dévolus à l\'affichage des résultats lors de rencontres sportives&nbsp;</p>','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(12,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Journal lumineux PRO 05.20.03</h1>\n<p>\n	Ce mod&egrave;le est la version totem du&nbsp;Journal lumineux PRO 05.20.03.</p>\n<p>\n	Ce mod&egrave;le de journal lumineux s&#39;adapte parfaitement dans un mobilier contemporain, tel qu&#39;au coeur d&#39;une salle d&#39;attente bancaire.</p>\n<p>\n	Il servira alors &agrave; afficher un message type de bienvenue.</p>','none','','none','','none','','none','<p>\n	<strong>Mod&egrave;le</strong> : PRO 05.20.03 - Totem<br />\n	<strong>Hauteur de caract&egrave;res</strong> : 3 cm<br />\n	<strong>Nombre de caract&egrave;res</strong> : 20<br />\n	<strong>Nombre de lignes</strong> : 5<br />\n	<strong>Couleur de caract&egrave;res</strong> : Normal Bright Green</p>','none','','none','','none','','none','jl-indoor-totem-1.jpg','none','','none','','none','[9] [journaux-lumineux-et-displays] Journaux lumineux et displays','none','',NULL,'',NULL,'',NULL,'',NULL,'Ce modèle est la version totem du&nbsp;Journal lumineux PRO 05.20.03. Ce modèle de journal lumineux s\'adapte parfaitement dans un mobilier contemporain, tel qu\'au coeur d\'une salle d\'attente bancaire. Il servira alors à afficher un message type de bienvenue.','xhtml','','xhtml','','xhtml','','xhtml','','none'),(13,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Afficheur de s&eacute;curit&eacute; industriel</h1>\n<p>\n	Nos <strong>afficheurs de s&eacute;curit&eacute;</strong> et d&#39;information <strong>industriels</strong> sont l&#39;outil par excellence pour visualiser vos <strong>messages de s&eacute;curit&eacute;</strong> et de pr&eacute;vention, pour <strong>anticiper les incidents</strong>, pour <strong>informer</strong> votre personnel et vos collaborateurs, pour <strong>annoncer</strong> vos informations de production, vos statistiques de s&eacute;curit&eacute;, vos recrutements, etc &hellip;</p>\n<p>\n	Ces <strong>afficheurs de s&eacute;curit&eacute;</strong> sont con&ccedil;us avec des zones programmables combin&eacute;es avec des textes fixes et peuvent &ecirc;tre personnalis&eacute;s par votre logo de soci&eacute;t&eacute;. Afin de visualiser l&#39;information voulue et correcte au bon moment et au bon endroit, la pr&eacute;sentation de l&#39;afficheur est d&eacute;finie en collaboration avec le client.</p>\n<p>\n	Le logiciel pour PC est r&eacute;alis&eacute; sur mesure ce qui rend la gestion des afficheurs tr&egrave;s ais&eacute;e. Les dimensions, la taille des caract&egrave;res, la couleur, l&#39;intensit&eacute; lumineuse, la communication (&eacute;ventuellement sans fil) sont d&eacute;termin&eacute;es en fonction de l&#39;application et des souhaits du client.</p>','none','','none','','none','','none','ind_070410194322_foto.jpg','none','ind_070410194322_foto.jpg\nind_080410153945_foto.jpg\nind_080410153900_foto.jpg\nind_080410154032_foto.jpg\nind_080410154528_foto.jpg\nind_080410154713_foto.jpg\nind_080410161103_foto.jpg\nind_150213150703_foto.jpg\nind_150213150446_foto.jpg\nind_190509120414_foto.jpg\nind_210310223650_foto.jpg\nind_210310224722_foto.jpg\nind_260509094801_foto.jpg','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>Message de sécurités en environnement industriel, chantiers, usines.</p>','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(14,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Ecrans g&eacute;ants &agrave; diodes full color</h1>\n<p>\n	Les &eacute;crans g&eacute;ants full color &agrave; led couvrent une tr&egrave;s <strong>large gamme de besoins et d&rsquo;applications</strong>. Artelux vous propose une game &eacute;tendue d&#39;&eacute;crans g&eacute;ants &agrave; diodes full color destin&eacute;s &agrave; des usages<strong> indoor </strong>ou<strong> outdoor</strong> et offrant des r&eacute;solutions diff&eacute;rentes ainsi qu&#39;une tr<strong>&egrave;s haute qualit&eacute; d&rsquo;image</strong>.&nbsp;</p>\n<p>\n	Outre le <strong>faible co&ucirc;t de maintenance</strong>, le <strong>contr&ocirc;le &agrave; distance</strong>, la facilit&eacute; de montage et de d&eacute;montage,&nbsp;ces <strong>&eacute;crans LED</strong>&nbsp;sont <strong>con&ccedil;us sur mesure </strong>pour s&rsquo;adapter &agrave; chaque environnement sp&eacute;cifique et r&eacute;pondre aux besoins particuliers de tout utilisateur.</p>\n<p>\n	Dans le contexte &eacute;conomique actuel, particuli&egrave;rement concurrentiel, les responsables des points de vente sont &agrave; la recherche de moyens d&#39;attirer l&rsquo;attention, de se d&eacute;marquer, d&rsquo;animer le point de vente et d&rsquo;informer la client&egrave;le. Les &eacute;crans g&eacute;ants &agrave; diodes full color s&rsquo;int&eacute;grent parfaitement dans l&rsquo;univers commercial et sont indispensables pour se d&eacute;velopper, prendre des parts de march&eacute;s et placer les clients dans un environnement favorable &agrave; la consommation.</p>\n<p>\n	Les &eacute;crans g&eacute;ants vid&eacute;o full color permettent &eacute;galement une <strong>communication</strong> et une <strong>diffusion publicitaire</strong> id&eacute;ales dans le domaine du sport.&nbsp;Les &eacute;crans g&eacute;ants full color &agrave; led r&eacute;pondent aux besoins et aux imp&eacute;ratifs de l&rsquo;environnement sportif et repr&eacute;sentent des <strong>&eacute;quipements id&eacute;aux</strong> pour les stades de football, de basket et chacun des emplacements o&ugrave; ont lieu des performances sportives.</p>\n<p>\n	Un partenariat de sponsoring &agrave; honorer ? Vous pourrez proposer &agrave; votre sponsor de diffuser son spot publicitaire sur votre &eacute;cran g&eacute;ant led full color. Ce type de produit permet donc d&#39;engranger un <strong>retour sur investissement</strong> et une r&eacute;elle <strong>rentabilit&eacute;</strong>.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<span>Les écrans géants full color à led couvrent une très large gamme de besoins et d’application et représentent des outils indispensables pour se démarquer dans l\'univers commercial et les événements sportifs.&nbsp;</span>','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(15,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Signalisation routi&egrave;re et de parking</h1>\n<p>\n	Artelux vous &eacute;quipe en signalisation lumineuse routi&egrave;re et de paking. Nos panneaux routiers permettent de guider les automobiliste vers une destination d&eacute;finie. Guider et Informer les usagers dans les parkings au moyen des <strong>panneaux d&#39;affichage</strong>, <strong>totems</strong>, <strong>panneau routier</strong>,<strong> journaux lumineux</strong>, <strong>afficheurs led</strong> s&#39;av&egrave;re particuli&egrave;rement &eacute;fficace.</p>\n<p>\n	Artelux propose une gamme de produits de <strong>signal&eacute;tique ext&eacute;ieure</strong>, <strong>int&eacute;rieure</strong> et de <strong>s&eacute;curit&eacute; de parking</strong> particuli&egrave;rement large et adaptative.</p>\n<p>\n	Gr&acirc;ce aux produits de <strong>signal&eacute;tique</strong> propos&eacute;s par Artelux votre <strong>signalisation</strong> sera la plus <strong>visible</strong> possible. Les dimensions (standards ou sur-mesure) sont adapt&eacute;es a la configuration du site, en fonction du trafic et de la distance de visibilit&eacute;. La densit&eacute; de la signl&eacute;tique est adapt&eacute;e &agrave; la complexit&eacute; de l&rsquo;acheminement &agrave; r&eacute;aliser.</p>\n<p>\n	Les <strong>panneaux &agrave; message variables</strong> sont particuli&egrave;rement destin&eacute;s &agrave; informer les automobilistes de travaux, d&eacute;viations, conditions m&eacute;t&eacute;orologiques particuli&egrave;res, ou toute source de danger inhabituel.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(16,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Affichage prix carburants</h1>\n<p>\n	Les <strong>&eacute;crans de stations-service</strong> permettent d&#39;afficher les prix du carburant de mani&egrave;re <strong>visible</strong> et <strong>exacte</strong>.&nbsp;</p>\n<p>\n	Ces <strong>afficheurs lumineux sont</strong> disponibles en plusieurs couleurs et tailles de chiffres et sont con&ccedil;us selon les <strong>technologies les plus avanc&eacute;es</strong> et avec des <strong>mat&eacute;riaux haut de gamme</strong>. Ils vous permettent ainsi visualiser rapidement et simplement les prix exacts.</p>\n<p>\n	Artelux vous propose une vaste gamme d&#39;<strong>afficheurs &eacute;lectoniques</strong> allant des&nbsp;<strong>mod&egrave;les standards</strong> aux <strong>mod&egrave;les sur-mesure haut de gamme</strong>.</p>\n<p>\n	Les <strong>&eacute;crans d&rsquo;affichage lumineu</strong>x de prix offre une solution sur mesure qui tient compte de la distance de lecture, des couleurs, de la clart&eacute;, etc.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<span>Les écrans de stations-service permettent d\'afficher les prix du carburant de manière visible et exacte.&nbsp;</span>','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','xhtml','','none'),(17,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Afficheur de score pour basketball</h1>\n<p>\n	Cet afficheur de score pour basketball est un&nbsp;<strong>panneau affichage diodes</strong>&nbsp;proposant une grande lisibilit&eacute;</p>','none','','none','','none','','none','','none','','none','','none','','none','240310130402_foto.jpg','none','240310130402_foto.jpg','none','','none','[11] [marquoirs-de-score] Marquoirs de score','none','',NULL,'',NULL,'',NULL,'',NULL,'<span>Cet afficheur de score pour basketball est un&nbsp;</span><b>panneau affichage diodes</b><span>&nbsp;proposant une grande lisibilité</span>','xhtml','','xhtml','','xhtml','','xhtml','','none'),(18,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Afficheur de score football avec 2 panneaux rotatifs et journal lumineux</h1>\n<p>\n	<strong>Afficheur de score</strong> destin&eacute; au <strong>football.</strong>&nbsp;Ces afficheurs lumineux sont compos&eacute;s de journaux lumineux mono-ligne et de panneaux d&#39;affichage.</p>','none','','none','','none','','none','<p>\n	<strong>Dimensions</strong> (lxh):<br />\n	1120cm x 220cm</p>\n<p>\n	<strong>Affichage de score digitale:</strong><br />\n	Hauteur de chiffres: 41 cm</p>\n<p>\n	<strong>Horloge digitale:</strong><br />\n	Type LK41</p>\n<p>\n	<strong>Journal lumineux:</strong><br />\n	Type: QLSO 12.45<br />\n	Hauteur de caract&egrave;res: 45 cm<br />\n	Nombre de caract&egrave;res: 12<br />\n	Couleur de caract&egrave;res: Ultra Bright Rouge</p>','none','','none','','none','','none','220509132621_foto.jpg','none','220509132621_foto.jpg\n220509132753_foto.jpg\n020809192926_foto.jpg','none','','none','[11] [marquoirs-de-score] Marquoirs de score','none','',NULL,'',NULL,'',NULL,'',NULL,'<b>Afficheur de score</b><span>&nbsp;</span><b>football.</b><span>&nbsp;Ces afficheurs lumineux sont composés de journaux lumineux mono-ligne et de panneaux d\'affichage.</span>','xhtml','','xhtml','','xhtml','','xhtml','','none'),(19,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Marquoir de score pour hockey 31VS1.2.1</h1>\n<p>\n	Afficheur de score destin&eacute; au Hockey. Ces afficheurs lumineux sont compos&eacute;s de panneaux d&#39;affichage diode</p>','none','','none','','none','','none','<p>\n	<strong>Hauteur de caract&egrave;res</strong>: 31 cm<br />\n	<strong>Nombre de caract&egrave;res</strong>: 5<br />\n	<strong>Couleur de caract&egrave;res</strong>: rouge haute luminosit&eacute;</p>','none','','none','','none','','none','040112194559_foto.jpg','none','040112194559_foto.jpg','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'<span>Afficheur de score destiné au Hockey. Ces afficheurs lumineux sont composés de panneaux d\'affichage diode</span>','xhtml','','xhtml','','xhtml','','xhtml','','none'),(20,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h2>\n	Panneau de score pour athl&eacute;tisme</h2>\n<p>\n	Cet afficheur de score pour athl&eacute;tisme est un panneau d&#39;affichage led multi-lignes.</p>','none','','none','','none','','none','<p>\n	<strong>Dimensions</strong> (lxh): 890cm x 380cm</p>\n<p>\n	<strong>Journal lumineux:</strong><br />\n	QLMI 10.40.20<br />\n	Hauteur de caract&egrave;res: 20 cm<br />\n	Nombre de lignes: 10<br />\n	Nombre de caract&egrave;res: 40<br />\n	Couleur de caract&egrave;res: Super Bright Rouge</p>\n<p>\n	<strong>Affichage de l&#39;heure:</strong><br />\n	Heures - Minutes - Secondes<br />\n	Hauteur de chiffres: 31cm<br />\n	Couleur de chiffres: Super Bright Jaune</p>','none','','none','','none','','none','220509140954_foto.jpg','none','220509140954_foto.jpg','none','','none','[11] [marquoirs-de-score] Marquoirs de score','none','',NULL,'',NULL,'',NULL,'',NULL,'<span>Cet afficheur de score pour athlétisme est un panneau d\'affichage led multi-lignes.</span>','xhtml','','xhtml','','xhtml','','xhtml','','none'),(21,1,5,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','xhtml','','none'),(22,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Croix de pharmacie</h1>\n<p>\n	Artelux est un acteur central dans la distribution de <strong>croix lumineuses pour pharmacies</strong> et de <strong>croix pour v&eacute;t&eacute;rinaire</strong> depuis plus de 20 ans.</p>\n<h2>\n	Croix de pharmacie</h2>\n<p>\n	Etre <strong>imm&eacute;diatement identifiable</strong> dans la rue est d&#39;une importance capitale pour un&nbsp;<strong>pharmacien</strong>, surtout &agrave; l&rsquo;&eacute;gard des clients : la vision d&#39;une <strong>croix lumineuse de pharmacie</strong>, en effet, permet au client en recherche une <strong>identification imm&eacute;diate</strong> de l&#39;officine. Nos <strong>croix de pharmacie</strong> joignent les <strong>technologies de l&rsquo;information</strong> et &nbsp;un <strong>design soign&eacute; et contemporain</strong>.</p>\n<p>\n	Nos <strong>croix pour pharmacie</strong> sont r&eacute;alis&eacute;es suivant les <strong>technologies les plus avanc&eacute;es</strong> et au moyen de <strong>mat&eacute;riaux hautement qualitatifs</strong>. Artelux propose au pharmacien l&#39;opportunit&eacute; de <strong>personnaliser</strong> ce signe distinctif gr&acirc;ce &agrave; des <strong>messages attractifs</strong> et qui relaient de l&#39;information par le biais de l&rsquo;animation. Les <strong>croix de pharmacie</strong> artelux permettent d&#39;accro&icirc;tre la renom&eacute;e de votre officine.</p>\n<p>\n	Nous vous proposons des mod&egrave;les de croix d&eacute;clin&eacute;es selon une tr&egrave;s large gamme : <strong>Croix LED</strong>, <strong>croix diodes</strong>, <strong>croix double face</strong>, <strong>croix n&eacute;on</strong>.</p>\n<p>\n	Nous proposons &eacute;galement d&eacute;sormais un <strong>produit haut-de-gamme</strong>, les <strong>croix lumineuses de pharmacie &agrave; diode full-color</strong>. Ces croix de pharmacies vous permettent de diffuser des <strong>messages informatifs</strong> enti&egrave;rement personnalis&eacute;s, et param&eacute;trables <strong>&agrave; distance.</strong></p>\n<h2>\n	Croix pour v&eacute;t&eacute;rinaires</h2>\n<p>\n	Les <strong>croix pour v&eacute;trinaire LED&nbsp;bleues</strong> propos&eacute;es par artelux permettent aux v&eacute;t&eacute;rinaires de s&#39;<strong>afficher avec clart&eacute;</strong>.</p>\n<p>\n	Une <strong>croix v&eacute;t&eacute;rinaire LED</strong> devant votre dispensaire ou <strong>clinique v&eacute;t&eacute;rinaire</strong> permet aux propri&eacute;taires d&#39;animaux&nbsp;qui recherchent un v&eacute;t&eacute;rinaire d&#39;&ecirc;tre attir&eacute;s directement par le <strong>bleu caract&eacute;ristique de ces croix</strong>.</p>\n<p>\n	Ces croix bleues luminescentes sont extremement durables et &eacute;nergiquement &eacute;conomes. La croix v&eacute;t&eacute;rinaire allie technologie d&rsquo;information avec un design soign&eacute;. Les croix sont assembl&eacute;es avec des mat&eacute;rieaux de haute qualit&eacute;. Il est possible d&rsquo;afficher des animations et des informations comme le temps, la date et la temperature.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'Artelux est un acteur central dans la distribution de&nbsp;<b>croix lumineuses pour pharmacies</b>&nbsp;et de&nbsp;<b>croix pour vétérinaire</b>&nbsp;depuis plus de 20 ans. Nous vous proposons des modèles de croix déclinées selon une très large gamme :&nbsp;<b>Croix LED</b>,&nbsp;<b>croix diodes</b>,&nbsp;<b>croix double face</b>,&nbsp;<b>croix néon</b>.<p>Les&nbsp;<b>croix pour vétrinaire LED&nbsp;bleues</b>&nbsp;proposées par artelux permettent aux vétérinaires de s\'<b>afficher avec clarté</b>.</p>','xhtml','','xhtml','','xhtml','','xhtml','',NULL,'',NULL,'',NULL,'',NULL,'','none'),(23,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Ecrans graphiques info ville - Full color</h1>\n<p>\n	Les <strong>&eacute;crans graphiques info ville</strong> sont des <strong>&eacute;crans g&eacute;ants full color &agrave; led</strong> &agrave; usage <strong>outdoor</strong> qui vous permettront d&#39;attirer l&#39;attention des passants en alternant des s&eacute;quences d&#39;images avec des messages vid&eacute;o ! Offrant une <strong>tr&egrave;s haute qualit&eacute; de r&eacute;solution</strong> et un panel de 16 millions de couleurs, ces panneaux d&#39;affichage diode constituent un <strong>outil id&eacute;al en terme de publicit&eacute; et de communication</strong>.</p>\n<p>\n	Nos <strong>&eacute;crans graphiques</strong> info villes existent en plusieurs dimensions et configurations selon leurs endroits d&#39;implantation, que ce soit le long d&#39;une route nationale, sur une place ou dans un pi&eacute;tonnier.</p>\n<p>\n	Leurs <strong>effets graphiques</strong>, tels que clignotement, d&eacute;filement, ou effets plus complexes, captent inmanquablement le regard des passants et usagers de la route vers votre message d&#39;annonce, publicitaire ou informatif.</p>\n<p>\n	Outre le faible co&ucirc;t de maintenance, le contr&ocirc;le &agrave; distance, la facilit&eacute; de montage et de d&eacute;montage, ces <strong>&eacute;crans d&#39;affichage diode</strong> sont con&ccedil;us <strong>sur mesure</strong> pour s&rsquo;adapter &agrave; chaque environnement sp&eacute;cifique et r&eacute;pondre aux besoins particuliers de tout utilisateur.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p><span>Nos écrans graphiques info ville sont des écrans</span>&nbsp;géants full color à led qui vous permettront d\'attirer l\'attention des passants en alternant des séquences d\'images avec des messages vidéo !&nbsp;<span>Offrant une très haute qualité de résolution et un panel de 16 millions de couleurs, ces panneaux d\'affichage diode</span>&nbsp;constituent un outil idéal en terme de&nbsp;publicité et de communication.&nbsp;</p>\n','xhtml','','xhtml','','xhtml','','xhtml','',NULL,'',NULL,'',NULL,'',NULL,'','none'),(24,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Radars Pr&eacute;ventifs</h1>\n<p>\n	Chaque collectivit&eacute;, chaque commune, est aujourd&rsquo;hui pr&eacute;occup&eacute;e par les exc&egrave;s de vitesse des automobilistes qui circulent sur leur territoire.&nbsp;&#8232;&#8232;G&eacute;n&eacute;ralement plac&eacute;s sur les lieux tr&egrave;s accidentog&egrave;nes, les <strong>panneaux d&rsquo;affichage de vitesse</strong> LED r&eacute;pondent &agrave; une n&eacute;cessit&eacute; d&rsquo;ordre pr&eacute;ventif, dissuasif mais aussi p&eacute;dagogique.</p>\n<p>\n	Les <strong>radars pr&eacute;ventifs</strong> permettent aux utilisateurs routiers de visualiser en temps r&eacute;el leur vitesse de circulation. Clignotant lorsque que la vitesse limit&eacute;e est d&eacute;pass&eacute;e, ce <strong>syst&egrave;me d&rsquo;affichage diodes</strong> attire l&rsquo;attention des conducteurs, d&rsquo;une mani&egrave;re non r&eacute;pressive, sur leur comportement de conduite.</p>\n<p>\n	Au moyen de brefs messages (&eacute;cole, danger, ralentir), les panneaux d&rsquo;affichages de vitesse ont pour objectif d&rsquo;inciter les conducteurs &agrave; respecter la vitesse autoris&eacute;e dans l&#39;agglom&eacute;ration et, au besoin, &agrave; ralentir. Ces radars peuvent, en outre, enregistrer les vitesses des v&eacute;hicules pour &eacute;tablir des statistiques de circulation. Certains mod&egrave;les, toujours dans un but &eacute;ducatif, permettent &eacute;galement d&#39;afficher un smiley suivant le respect ou non de la vitesse.</p>\n<p>\n	Artelux vous propose une large gamme de panneaux d&rsquo;affichage de vitesse tr&egrave;s <strong>performants</strong> con&ccedil;us pour assurer une <strong>visibilit&eacute; optimale</strong> gr&acirc;ce &agrave; leurs dimensions alliant <strong>efficacit&eacute;</strong> et <strong>compacit&eacute;</strong>. Visibles en toutes circonstances (m&ecirc;me par exposition directe &agrave; la lumi&egrave;re du soleil), pr&eacute;cis et efficaces, les panneaux d&rsquo;affichage propos&eacute;s par Artelux sauront offrir aux communes et aux villes un mat&eacute;riel relevant des <strong>technologies les plus avanc&eacute;es</strong> et compos&eacute; de <strong>mat&eacute;riaux haut-de-gamme</strong>. Ces &eacute;crans LED sont le m&eacute;dia id&eacute;al pour proposer une <strong>signal&eacute;tique dynamique et &eacute;volutive</strong>.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<span>Les&nbsp;</span><b>radars préventifs</b><span>&nbsp;permettent aux utilisateurs routiers de visualiser en temps réel leur vitesse de circulation. Clignotant lorsque que la vitesse limitée est dépassée, ce&nbsp;</span><b>système d’affichage diodes</b><span>&nbsp;attire l’attention des conducteurs, d’une manière non répressive, sur leur comportement de conduite.</span>','xhtml','','xhtml','','xhtml','','xhtml','',NULL,'',NULL,'',NULL,'',NULL,'','none'),(25,1,6,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'slider_03_1.jpg\nslider_02_1.jpg\nslider_04_1.jpg\nslider_05_1.jpg\nslider_06.jpg','none'),(26,1,2,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<h1>\n	Afficheurs &eacute;lectroniques d&#39;informations communales</h1>\n<p>\n	Artelux propose aux communes des&nbsp;<strong>afficheurs &eacute;lectroniques d&#39;informations communales.&nbsp;</strong>Il s&#39;agit de <strong>syst&egrave;mes d&#39;affichages</strong> allant des <strong>displays</strong> et <strong>journaux lumineux&nbsp;</strong>multi-lignes au <strong>&eacute;crans LED full color.</strong></p>\n<p>\n	A notre &eacute;poque, une <strong>autorit&eacute; communale</strong> doit &ecirc;tre &agrave; m&ecirc;me d&#39;<strong>informer avec rapidit&eacute;</strong> et <strong>dynamisme</strong> ses administr&eacute;s. Nos <strong>syst&egrave;mes &eacute;lectroniques d&#39;informations communales</strong> vous permettent de mettre efficacement en &eacute;vidence des messages relevant tant de la <strong>vie communale</strong> que de l&#39;<strong>&eacute;venementiel</strong>. Ces <strong>displays</strong> ou <strong>&eacute;crans diodes</strong> vous permettent d&#39;afficher de mani&egrave;re incontournable les <strong>informations essentielles</strong> de la vie de votre commune.</p>\n<p>\n	Les <strong>&eacute;crans &agrave; &eacute;criture lumineuse</strong> Artelux s&#39;imposent agr&eacute;ablement au coeur des <strong>lieux de passage</strong>&nbsp;incontournables tels que place communale, centres commerciaux ou frontons de commerces.</p>\n<p>\n	Ils peuvent en outre vous permettre d&#39;engranger un <strong>retour sur investissement</strong> en proposant une <strong>r&eacute;gie publicitaire</strong> pour tous types d&#39;annonceurs.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<span>Artelux propose aux communes des&nbsp;</span><b>afficheurs électroniques d\'informations communales.&nbsp;</b><span>Il s\'agit de&nbsp;</span><b>systèmes d\'affichages</b><span>&nbsp;allant des&nbsp;</span><b>displays</b><span>&nbsp;et&nbsp;</span><b>journaux lumineux&nbsp;</b><span>multi-lignes au&nbsp;</span><b>écrans LED full color.</b>','xhtml','','xhtml','','xhtml','','xhtml','',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(27,1,1,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(28,1,3,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(29,1,3,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(30,1,3,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(31,1,1,'','xhtml','','xhtml','','xhtml','<h1>\n	Mentions L&eacute;gales</h1>\n<p>\n	Ce site web est la propri&eacute;t&eacute; de <strong>Artelux bvba</strong></p>\n<h2>\n	Informations personnelles:</h2>\n<p>\n	<strong>Adresse du si&egrave;ge social:</strong><br />\n	La Picherotte 28 - B-4053 Embourg<br />\n	T&eacute;l&eacute;phone: +32 4 365 17 00</p>\n<p>\n	<strong>E-mail:</strong> <a href=\"mailto:info@artelux.be\">info@artelux.be</a></p>\n<p>\n	<strong>Num&eacute;ro d&#39;entreprise:</strong> BTW BE 0474.567.154</p>\n<p>\n	En acc&eacute;dant &agrave; ce site web et en l&#39;utilisant, vous marquez explicitement votre accord avec les conditions g&eacute;n&eacute;rales suivantes.</p>\n<h2>\n	Droits de propri&eacute;t&eacute; intellectuelle</h2>\n<p>\n	Le contenu de ce site, y compris les marques, logos, dessins, donn&eacute;es, noms de produits ou d&#39;entreprises, textes, images, etc. sont prot&eacute;g&eacute;s par des droits de propri&eacute;t&eacute; intellectuelle appartenant &agrave; Artelux bvba ou &agrave; des tiers titulaires de ces droits.</p>\n<h2>\n	Limite de responsabilit&eacute;</h2>\n<p>\n	Les informations contenues sur ce site web sont de nature g&eacute;n&eacute;rale. Ces informations ne sont pas adapt&eacute;es &agrave; des circonstances personnelles ou sp&eacute;cifiques et ne peuvent donc &ecirc;tre consid&eacute;r&eacute;es comme un conseil personnel, professionnel ou juridique &agrave; l&#39;utilisateur.</p>\n<p>\n	Artelux bvba consent de gros efforts pour que les informations mises &agrave; disposition soient compl&egrave;tes, correctes, exhaustives et &agrave; jour. Malgr&eacute; ces efforts, des erreurs peuvent figurer dans les informations mises &agrave; votre disposition. Si les informations diffus&eacute;es sur le site comportaient des erreurs ou si certaines informations &eacute;taient indisponibles sur ou via le site, Artelux bvba mettra tout en oeuvre pour rectifier la situation.</p>\n<p>\n	Artelux bvba ne peut toutefois pas &ecirc;tre tenue responsable de dommages directs ou indirects r&eacute;sultant de l&#39;utilisation des informations mises &agrave; disposition sur le site.</p>\n<p>\n	Si vous veniez &agrave; constater des erreurs dans les informations mises &agrave; disposition sur le site, vous pouvez contacter le gestionnaire du site.</p>\n<p>\n	Le contenu du site (y compris les liens) peut &ecirc;tre adapt&eacute;, modifi&eacute; ou compl&eacute;t&eacute; &agrave; tout moment sans avertissement ni communication aucune. Artelux bvba n&#39;octroie aucune garantie de bon fonctionnement du site web et ne peut en aucune mani&egrave;re &ecirc;tre tenue responsable d&#39;un dysfonctionnement du site, d&#39;une non-disponibilit&eacute; provisoire du site ou de toute forme de dommages, directs ou indirects, pouvant r&eacute;sulter de l&#39;acc&agrave;s au site et de l&#39;usage du site.</p>\n<p>\n	En aucun cas, Artelux bvba ne pourra &ecirc;tre tenue pour responsable envers quiconque, d&#39;une mani&egrave;re directe, indirecte, sp&eacute;ciale ou autre, des dommages qui pourraient r&eacute;sulter de l&#39;usage de ce site ou d&#39;un autre, en raison notamment des connexions ou des liens hypertextes, incluant, sans limitation, toute perte, interruption du travail, d&eacute;t&eacute;rioration de programmes ou de donn&eacute;es sur le syst&egrave;me informatique, le mat&eacute;riel, les logiciels, etc. de l&#39;utilisateur.</p>\n<p>\n	Le site web peut contenir des liens hypertextes vers des sites ou pages web de tiers ou y faire r&eacute;f&eacute;rence de mani&egrave;re indirecte. La pr&eacute;sence de liens vers ces sites ou pages web ne signifie nullement une approbation implicite du contenu de ces sites ou pages.</p>\n<p>\n	Artelux bvba d&eacute;clare explicitement qu&#39;elle n&#39;a aucune autorit&eacute; sur le contenu ou sur les autres caract&eacute;ristiques de ces sites ou pages web et ne peut en aucun cas &ecirc;tre tenue responsable du contenu ou des caract&eacute;ristiques desdits sites ou pages web, ni de dommages pouvant r&eacute;sulter de leur utilisation.</p>\n<h2>\n	Droit applicable et tribunaux comp&eacute;tents</h2>\n<p>\n	Ce site est r&eacute;gi par le droit belge en vigueur. Seuls les tribunaux de l&#39;arrondissement de Hasselt sont comp&eacute;tents en cas de litige.</p>\n<h2>\n	Politique de respect de la vie priv&eacute;e</h2>\n<p>\n	Artelux bvba se soucie du respect de votre vie priv&eacute;e. Bien que la plupart des informations contenues sur ce site soient disponibles sans que les donn&eacute;es personnelles de l&#39;utilisateur ne soient n&eacute;cessaires, il est possible que l&#39;utilisateur soit invit&eacute; &agrave; fournir des informations personnelles. Ces donn&eacute;es seront uniquement trait&eacute;es dans le cadre de notre gestion clients, pour vous tenir au courant de nos activit&Atilde;&copy;s. L&#39;utilisateur peut toujours s&#39;opposer, gratuitement et sur demande, &agrave; l&#39;utilisation de ses donn&eacute;es personnelles &agrave; des fins de marketing direct. Pour ce faire, il devra s&#39;adresser &agrave; Artelux bvba, La Picherotte 28 - B-4053 Embourg, info@artelux.be. Vos donn&eacute;es personnelles ne sont jamais communiqu&eacute;es &agrave; des tiers.</p>\n<p>\n	Conform&eacute;ment &agrave; la loi du 08/12/1992 relative &agrave; la protection des donn&eacute;es &agrave; caract&egrave;re personnel, l&#39;utilisateur dispose d&#39;un droit l&eacute;gal de consultation et &eacute;ventuellement de correction de ses donn&eacute;es personnelles. Sur preuve de votre identit&eacute; (copie de la carte d&#39;identit&eacute;), vous pouvez, sur demande &eacute;crite, sign&eacute;e et dat&eacute;e adress&eacute;e &agrave; Artelux bvba, La Picherotte 28 - B-4053 Embourg, info@artelux.be, recevoir gratuitement un compte rendu par &eacute;crit de vos donn&eacute;es personnelles. Le cas &eacute;ch&eacute;ant, vous pouvez &eacute;galement demander la correction des donn&eacute;es personnelles vous concernant qui sont incorrectes, incompl&egrave;tes ou erron&eacute;es.</p>\n<p>\n	Artelux bvba peut r&eacute;colter des informations anonymes ou agr&eacute;g&eacute;es non personnelles telles que la version du navigateur ou l&#39;adresse IP, le syst&egrave;me d&#39;exploitation que vous utilisez ou le nom de domaine du site gr&acirc;ce auquel vous visitez le site ou vers lequel vous le quittez. Ceci nous permet d&#39;optimaliser en permanence le site web pour les utilisateurs.</p>\n<h2>\n	Utilisation de \"cookies\"</h2>\n<p>\n	Durant une visite sur le site, des &#39;cookies&#39; peuvent &ecirc;tre install&eacute;s sur le disque dur de votre ordinateur, et ce uniquement dans le but de mieux adapter le site web aux besoins du visiteur r&eacute;current. Ces minifichiers ou cookies ne sont pas utilis&eacute;s pour analyser la mani&egrave;re dont le visiteur surfe sur d&#39;autres sites. Votre navigateur internet vous permet de bloquer l&#39;utilisation de cookies, d&#39;&ecirc;tre inform&eacute; au pr&eacute;alable de l&#39;installation d&#39;un cookie, ou de supprimer ult&eacute;rieurement les cookies de votre disque dur. Consultez la fonction d&#39;aide de votre navigateur internet pour en savoir plus &agrave; ce sujet.</p>','none','','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(32,1,4,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<p>\n	Un concept nouveau pour l&rsquo;information sur la s&eacute;curit&eacute; routi&egrave;re, le civisme, le respect :</p>\n<p>\n	City Information and Prevention Screen, <strong>CIPS</strong>.</p>','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL),(33,1,4,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','xhtml','','xhtml','','xhtml','<p>\n	Zone 30 : panneaux (F4a et F4b) dynamiques programmables.</p>\n<p>\n	D&eacute;couvrez la brochure ci-jointe.</p>','none','','none','','none','','none','','none','','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL);
/*!40000 ALTER TABLE `exp_channel_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_entries_autosave`
--

DROP TABLE IF EXISTS `exp_channel_entries_autosave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_entries_autosave`
--

LOCK TABLES `exp_channel_entries_autosave` WRITE;
/*!40000 ALTER TABLE `exp_channel_entries_autosave` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_channel_entries_autosave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_fields`
--

DROP TABLE IF EXISTS `exp_channel_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_fields`
--

LOCK TABLES `exp_channel_fields` WRITE;
/*!40000 ALTER TABLE `exp_channel_fields` DISABLE KEYS */;
INSERT INTO `exp_channel_fields` VALUES (1,1,1,'page_titre_nl','Titre (NL)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(2,1,1,'page_titre_en','Titre (EN)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(3,1,1,'page_titre_de','Titre (DE)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(4,1,1,'page_contenu_fr','Contenu (FR)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,128,'n','ltr','y','n','none','n',4,'any','YTo5OntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(5,1,1,'page_contenu_nl','Contenu (NL)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',5,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(6,1,1,'page_contenu_en','Contenu (EN)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',6,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(7,1,1,'page_contenu_de','Contenu (DE)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',7,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(8,1,1,'page_thumbnail','Thumbnail','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',8,'any','YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToxIjt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToieSI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToibiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),(9,1,1,'page_galerie','Galerie','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',9,'any','YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToxIjt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToieSI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),(18,1,2,'prod_contenu_fr','Contenu (FR)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',4,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(17,1,2,'prod_titre_de','Titre (DE)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(16,1,2,'prod_titre_en','Titre (EN)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(14,1,1,'page_documents','Documents','','matrix','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',10,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MToiMSI7aToxO3M6MToiMiI7aToyO3M6MToiMyI7aTozO3M6MToiNCI7fX0='),(15,1,2,'prod_titre_nl','Titre (NL)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(19,1,2,'prod_contenu_nl','Contenu (NL)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',5,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(20,1,2,'prod_contenu_en','Contenu (EN)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',6,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(21,1,2,'prod_contenu_de','Contenu (DE)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',7,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(22,1,2,'prod_thumbnail','Thumbnail','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',12,'any','YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6InkiO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(23,1,2,'prod_galerie','Galerie','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',13,'any','YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6InkiO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(24,1,2,'prod_documents','Documents','','matrix','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',14,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MToiNSI7aToxO3M6MToiNiI7aToyO3M6MToiNyI7aTozO3M6MToiOCI7fX0='),(25,1,3,'serv_titre_nl','Titre (NL)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(26,1,3,'serv_titre_en','Titre (EN)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(27,1,3,'serv_titre_de','Titre (DE)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(28,1,3,'serv_contenu_fr','Contenu (FR)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',4,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(29,1,3,'serv_contenu_nl','Contenu (NL)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',5,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(30,1,3,'serv_contenu_en','Contenu (EN)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',6,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(31,1,3,'serv_contenu_de','Contenu (DE)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',7,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(32,1,3,'serv_thumbnail','Thumbnail','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',8,'any','YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6InkiO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(33,1,3,'serv_galerie','Galerie','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',9,'any','YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6InkiO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(34,1,3,'serv_documents','Documents','','matrix','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',10,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MToiOSI7aToxO3M6MjoiMTAiO2k6MjtzOjI6IjExIjtpOjM7czoyOiIxMiI7fX0='),(35,1,4,'actu_titre_nl','Titre (NL)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(36,1,4,'actu_titre_en','Titre (EN)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(37,1,4,'actu_titre_de','Titre (DE)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(38,1,4,'actu_contenu_fr','Contenu (FR)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',4,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(39,1,4,'actu_contenu_nl','Contenu (NL)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',5,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(40,1,4,'actu_contenu_en','Contenu (EN)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',6,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(41,1,4,'actu_contenu_de','Contenu (DE)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',7,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(42,1,4,'actu_documents','Documents','','matrix','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',8,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0IjtpOjI7czoyOiIxNSI7aTozO3M6MjoiMTYiO319'),(43,1,4,'actu_galerie','Galerie','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',9,'any','YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZTo2Ijt9czo0OiJ2aWV3IjtzOjY6InRodW1icyI7czoxMDoidGh1bWJfc2l6ZSI7czo1OiJzbWFsbCI7czoxNDoic2hvd19maWxlbmFtZXMiO3M6MToibiI7czo5OiJzaG93X2NvbHMiO2E6MTp7aTowO3M6NDoibmFtZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),(44,1,5,'item_titre_nl','Titre (NL)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(45,1,5,'item_titre_en','Titre (EN)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(46,1,5,'item_titre_de','Titre (DE)','','text','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','xhtml','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(47,1,5,'item_contenu_fr','Contenu (FR)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',8,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(48,1,5,'item_contenu_nl','Contenu (NL)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',9,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(49,1,5,'item_contenu_en','Contenu (EN)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',10,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(50,1,5,'item_contenu_de','Contenu (DE)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',11,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(51,1,5,'item_tech_fr','Infos techniques (FR)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',12,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(52,1,5,'item_tech_nl','Infos techniques (NL)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',13,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(53,1,5,'item_tech_en','Infos techniques (EN)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',14,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(54,1,5,'item_tech_de','Infos techniques (DE)','','wygwam','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','y','n','none','n',15,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(55,1,5,'item_thumbnail','Thumbnail','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',17,'any','YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6InkiO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(56,1,5,'item_galerie','Galerie','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',18,'any','YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6InkiO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),(57,1,5,'item_documents','Documents','','matrix','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',19,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MjoiMTciO2k6MTtzOjI6IjE4IjtpOjI7czoyOiIxOSI7aTozO3M6MjoiMjAiO319'),(58,1,5,'item_parent','Produit parent','','playa','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',16,'any','YToxMjp7czo1OiJtdWx0aSI7czoxOiJ5IjtzOjc6ImV4cGlyZWQiO3M6MToieSI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJjaGFubmVscyI7YToxOntpOjA7czoxOiIyIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(59,1,2,'prod_intro_fr','Intro (FR)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',8,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(60,1,2,'prod_intro_nl','Intro (NL)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',9,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(61,1,2,'prod_intro_en','Intro (EN)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',10,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(62,1,2,'prod_intro_de','Intro (DE)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',11,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(63,1,5,'item_intro_fr','Intro (FR)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',4,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(64,1,5,'item_intro_nl','Intro (NL)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',5,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(65,1,5,'item_intro_en','Intro (EN)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',6,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(66,1,5,'item_intro_de','Intro (DE)','','rte','','0',0,0,'channel',0,'0','0',0,4,0,'n','ltr','y','n','xhtml','n',7,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoxOiI0IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),(67,1,6,'image_slider','Image Slider','','assets','','0',0,0,'channel',0,'0','0',0,0,0,'n','ltr','n','n','none','n',1,'any','YToxMjp7czo4OiJmaWxlZGlycyI7YToxOntpOjA7czo0OiJlZToxIjt9czo0OiJ2aWV3IjtzOjQ6Imxpc3QiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6Im4iO3M6OToic2hvd19jb2xzIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjY6ImZvbGRlciI7aToyO3M6NDoiZGF0ZSI7aTozO3M6NDoic2l6ZSI7fXM6NToibXVsdGkiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
/*!40000 ALTER TABLE `exp_channel_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_member_groups`
--

DROP TABLE IF EXISTS `exp_channel_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_member_groups`
--

LOCK TABLES `exp_channel_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_channel_member_groups` DISABLE KEYS */;
INSERT INTO `exp_channel_member_groups` VALUES (6,1),(6,2),(6,3),(6,4);
/*!40000 ALTER TABLE `exp_channel_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channel_titles`
--

DROP TABLE IF EXISTS `exp_channel_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channel_titles`
--

LOCK TABLES `exp_channel_titles` WRITE;
/*!40000 ALTER TABLE `exp_channel_titles` DISABLE KEYS */;
INSERT INTO `exp_channel_titles` VALUES (1,1,1,1,0,NULL,'127.0.0.1','Test','test','open','y',0,0,0,0,'y','n',1359526979,'n','2013','01','30',0,0,20130130072259,0,0),(2,1,1,1,0,NULL,'127.0.0.1','Test 1','test-1','open','y',0,0,0,0,'y','n',1359527215,'n','2013','01','30',0,0,20130130072655,0,0),(3,1,1,1,0,NULL,'127.0.0.1','Accueil','accueil','open','y',0,0,0,0,'y','n',1359559315,'n','2013','01','30',0,0,20130130162756,0,0),(4,1,1,1,0,NULL,'127.0.0.1','Produits','produits','open','y',0,0,0,0,'y','n',1359563942,'n','2013','01','30',0,0,20130130173902,0,0),(5,1,1,1,0,NULL,'127.0.0.1','Services','services','open','y',0,0,0,0,'y','n',1359570768,'n','2013','01','30',0,0,20130130193248,0,0),(6,1,1,1,0,NULL,'127.0.0.1','Actualites','actualites','open','y',0,0,0,0,'y','n',1359570816,'n','2013','01','30',0,0,20130130190237,0,0),(7,1,1,1,0,NULL,'127.0.0.1','Contact','contact','open','y',0,0,0,0,'y','n',1359573126,'n','2013','01','30',0,0,20130130201206,0,0),(8,1,1,1,0,NULL,'127.0.0.1','Liens','liens','open','y',0,0,0,0,'y','n',1359574136,'n','2013','01','30',0,0,20130130202856,0,0),(9,1,2,1,0,NULL,'127.0.0.1','Journaux lumineux et displays','journaux-lumineux-et-displays','open','y',0,0,0,0,'y','n',1359688740,'n','2013','02','01',0,0,20130317132401,0,0),(10,1,5,1,0,NULL,'127.0.0.1','Journal lumineux PRO 05.20.03','journal-lumineux-pro-05.20.03','open','y',0,0,0,0,'y','n',1359693583,'n','2013','02','01',0,0,20130317140844,0,0),(11,1,2,1,0,NULL,'127.0.0.1','Marquoirs de score','marquoirs-de-score','open','y',0,0,0,0,'y','n',1359722883,'n','2013','02','01',0,0,20130317143104,0,0),(12,1,5,1,0,NULL,'127.0.0.1','Journal lumineux PRO 05.20.03 - Version totem','journal-lumineux-pro-05.20.03-version-totem','open','y',0,0,0,0,'y','n',1359724758,'n','2013','02','01',0,0,20130317140919,0,0),(13,1,2,1,0,NULL,'127.0.0.1','Afficheur de sécurité industriel','affiicheur-de-securite-industriel','open','y',0,0,0,0,'y','n',1359728092,'n','2013','02','01',0,0,20130321182853,0,0),(14,1,2,1,0,NULL,'192.168.1.6','Ecrans géants à diodes full color','ecrans-geants-a-diodes-full-color','open','y',0,0,0,0,'y','n',1363222783,'n','2013','03','14',0,0,20130317153044,0,0),(15,1,2,1,0,NULL,'127.0.0.1','Signalisation routière et de parking','signalisation-routiere-et-de-parking','open','y',0,0,0,0,'y','n',1363222814,'n','2013','03','14',0,0,20130317155515,0,0),(16,1,2,1,0,NULL,'127.0.0.1','Affichage prix carburants','affichage-prix-carburants','open','y',0,0,0,0,'y','n',1363223011,'n','2013','03','14',0,0,20130321183132,0,0),(17,1,5,1,0,NULL,'127.0.0.1','Afficheur de score pour basketball','afficheur-de-score-pour-basketball','open','y',0,0,0,0,'y','n',1363527006,'n','2013','03','17',0,0,20130317142807,0,0),(18,1,5,1,0,NULL,'127.0.0.1','Afficheur de score football avec 2 panneaux rotatifs et journal lumineux','afficheur-de-score-football-avec-2-panneaux-rotatifs-et-journal-lumineux','open','y',0,0,0,0,'y','n',1363527627,'n','2013','03','17',0,0,20130317142828,0,0),(19,1,5,1,0,NULL,'127.0.0.1','Marquoir de score pour hockey 31VS1.2.1','marquoir-de-score-pour-hockey-31vs1.2.1','open','y',0,0,0,0,'y','n',1363529835,'n','2013','03','17',0,0,20130317142116,0,0),(20,1,5,1,0,NULL,'127.0.0.1','Panneau de score pour athlétisme','panneau-de-score-pour-athletisme','open','y',0,0,0,0,'y','n',1363530086,'n','2013','03','17',0,0,20130317142727,0,0),(21,1,5,1,0,NULL,'127.0.0.1','Affichage de sécurité industriel','affichage-de-securite-industriel','open','y',0,0,0,0,'y','n',1363533520,'n','2013','03','17',0,0,20130317161840,0,0),(22,1,2,1,0,NULL,'127.0.0.1','Croix de pharmacie','croix-de-pharmacie','open','y',0,0,0,0,'y','n',1363535711,'n','2013','03','17',0,0,20130321183312,0,0),(23,1,2,1,0,NULL,'127.0.0.1','Ecrans graphiques info ville - Full color','ecrans-graphiques-info-ville-full-color','open','y',0,0,0,0,'y','n',1363535629,'n','2013','03','17',0,0,20130321183350,0,0),(24,1,2,1,0,NULL,'127.0.0.1','Radars Préventifs','radars-preventifs','open','y',0,0,0,0,'y','n',1363890128,'n','2013','03','21',0,0,20130321192208,0,0),(25,1,6,1,0,NULL,'127.0.0.1','Homepage','homepage','open','y',0,0,0,0,'y','n',1363894782,'n','2013','03','21',0,0,20130322023843,0,0),(26,1,2,1,0,NULL,'127.0.0.1','Afficheurs électroniques d\'informations communales','afficheurs-electroniques-dinformations-communales','open','y',0,0,0,0,'y','n',1363926099,'n','2013','03','22',0,0,20130322052139,0,0),(27,1,1,1,0,NULL,'127.0.0.1','Plan du site','sitemap','open','y',0,0,0,0,'y','n',1363927088,'n','2013','03','22',0,0,20130322044309,0,0),(28,1,3,1,0,NULL,'127.0.0.1','Placement','placement','open','y',0,0,0,0,'y','n',1363927511,'n','2013','03','22',0,0,20130322054511,0,0),(29,1,3,1,0,NULL,'127.0.0.1','Entretien','entretien','open','y',0,0,0,0,'y','n',1363927588,'n','2013','03','22',0,0,20130322054628,0,0),(30,1,3,1,0,NULL,'127.0.0.1','Garantie','garantie','open','y',0,0,0,0,'y','n',1363927605,'n','2013','03','22',0,0,20130322054645,0,0),(31,1,1,1,0,NULL,'127.0.0.1','Mentions Légales','disclaimer','open','y',0,0,0,0,'y','n',1363927797,'n','2013','03','22',0,0,20130322045458,0,0),(32,1,4,1,0,NULL,'127.0.0.1','City Information and Prevention Screen','city-information-and-prevention-screen','open','y',0,0,0,0,'y','n',1363928855,'n','2013','03','22',0,0,20130322060735,0,0),(33,1,4,1,0,NULL,'127.0.0.1','Zone 30 - panneaux dynamiques programmables','zone-30-panneaux-dynamiques-programmables','open','y',0,0,0,0,'y','n',1363928968,'n','2013','03','22',0,0,20130322060928,0,0);
/*!40000 ALTER TABLE `exp_channel_titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_channels`
--

DROP TABLE IF EXISTS `exp_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_channels`
--

LOCK TABLES `exp_channels` WRITE;
/*!40000 ALTER TABLE `exp_channels` DISABLE KEYS */;
INSERT INTO `exp_channels` VALUES (1,1,'pages','Pages','http://local.artelux.be/index.php',NULL,'en',10,0,1363927797,0,'',1,'open',1,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(2,1,'produits','Produits','http://local.artelux.be/index.php',NULL,'en',10,0,1363926099,0,'',1,'open',2,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(3,1,'services','Services','http://local.artelux.be/index.php',NULL,'en',3,0,1363927605,0,'',1,'open',3,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(4,1,'actus','Actualités','http://local.artelux.be/index.php',NULL,'en',2,0,1363928968,0,'',1,'open',4,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(5,1,'items','Items (Produits)','http://local.artelux.be/',NULL,'en',7,0,1363533520,0,'1',1,'open',5,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0),(6,1,'slider','Slider','http://local.artelux.be/',NULL,'en',1,0,1363894782,0,'',1,'open',6,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0);
/*!40000 ALTER TABLE `exp_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_comment_subscriptions`
--

DROP TABLE IF EXISTS `exp_comment_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_comment_subscriptions`
--

LOCK TABLES `exp_comment_subscriptions` WRITE;
/*!40000 ALTER TABLE `exp_comment_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_comment_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_comments`
--

DROP TABLE IF EXISTS `exp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_comments`
--

LOCK TABLES `exp_comments` WRITE;
/*!40000 ALTER TABLE `exp_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_cp_analytics`
--

DROP TABLE IF EXISTS `exp_cp_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_cp_analytics` (
  `site_id` int(5) unsigned NOT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `profile` text,
  `settings` text,
  `hourly_cache` text,
  `daily_cache` text,
  PRIMARY KEY (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_cp_analytics`
--

LOCK TABLES `exp_cp_analytics` WRITE;
/*!40000 ALTER TABLE `exp_cp_analytics` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_cp_analytics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_cp_log`
--

DROP TABLE IF EXISTS `exp_cp_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_cp_log`
--

LOCK TABLES `exp_cp_log` WRITE;
/*!40000 ALTER TABLE `exp_cp_log` DISABLE KEYS */;
INSERT INTO `exp_cp_log` VALUES (1,1,1,'sebisme','127.0.0.1',1359497433,'Logged in'),(2,1,1,'sebisme','127.0.0.1',1359517598,'Déconnecté'),(3,1,1,'sebisme','127.0.0.1',1359525273,'Connecté'),(4,1,1,'sebisme','127.0.0.1',1359526167,'Canal créé :&nbsp;&nbsp;Pages'),(5,1,1,'sebisme','127.0.0.1',1359559230,'Connecté'),(6,1,1,'sebisme','127.0.0.1',1359563124,'Canal créé :&nbsp;&nbsp;Produits'),(7,1,1,'sebisme','127.0.0.1',1359563815,'Canal créé :&nbsp;&nbsp;Services'),(8,1,1,'sebisme','127.0.0.1',1359568364,'Connecté'),(9,1,1,'sebisme','127.0.0.1',1359572368,'Canal créé :&nbsp;&nbsp;Actualités'),(10,1,1,'sebisme','127.0.0.1',1359687890,'Connecté'),(11,1,1,'sebisme','127.0.0.1',1359688423,'Groupe de membres créé :&nbsp;&nbsp;Administrateurs'),(12,1,1,'sebisme','127.0.0.1',1359688459,'Groupe de membres mis à jour :&nbsp;&nbsp;Administrateurs'),(13,1,1,'sebisme','127.0.0.1',1359692540,'Canal créé :&nbsp;&nbsp;Items (Produits)'),(14,1,1,'sebisme','127.0.0.1',1359692594,'Groupe de catégories créé :&nbsp;&nbsp;Produits'),(15,1,1,'sebisme','127.0.0.1',1359710252,'Connecté'),(16,1,1,'sebisme','127.0.0.1',1359717856,'Connecté'),(17,1,1,'sebisme','127.0.0.1',1359722891,'Connecté'),(18,1,1,'sebisme','127.0.0.1',1363120404,'Connecté'),(19,1,1,'sebisme','127.0.0.1',1363222716,'Connecté'),(20,1,1,'sebisme','127.0.0.1',1363526526,'Connecté'),(21,1,1,'sebisme','192.168.1.6',1363531735,'Connecté'),(22,1,1,'sebisme','127.0.0.1',1363540225,'Profil de membre créé :&nbsp;&nbsp;raymond'),(23,1,1,'sebisme','127.0.0.1',1363890101,'Connecté'),(24,1,1,'sebisme','127.0.0.1',1363891186,'Canal créé :&nbsp;&nbsp;Slider'),(25,1,1,'sebisme','127.0.0.1',1363902408,'Déconnecté'),(26,1,1,'sebisme','127.0.0.1',1363914827,'Connecté'),(27,1,1,'sebisme','127.0.0.1',1363914882,'Profil de membre créé :&nbsp;&nbsp;artelux');
/*!40000 ALTER TABLE `exp_cp_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_cp_search_index`
--

DROP TABLE IF EXISTS `exp_cp_search_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_cp_search_index`
--

LOCK TABLES `exp_cp_search_index` WRITE;
/*!40000 ALTER TABLE `exp_cp_search_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_cp_search_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_developer_log`
--

DROP TABLE IF EXISTS `exp_developer_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_developer_log`
--

LOCK TABLES `exp_developer_log` WRITE;
/*!40000 ALTER TABLE `exp_developer_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_developer_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_devotee_hidden_addons`
--

DROP TABLE IF EXISTS `exp_devotee_hidden_addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_devotee_hidden_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `package` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_devotee_hidden_addons`
--

LOCK TABLES `exp_devotee_hidden_addons` WRITE;
/*!40000 ALTER TABLE `exp_devotee_hidden_addons` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_devotee_hidden_addons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_cache`
--

DROP TABLE IF EXISTS `exp_email_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_cache`
--

LOCK TABLES `exp_email_cache` WRITE;
/*!40000 ALTER TABLE `exp_email_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_cache_mg`
--

DROP TABLE IF EXISTS `exp_email_cache_mg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_cache_mg`
--

LOCK TABLES `exp_email_cache_mg` WRITE;
/*!40000 ALTER TABLE `exp_email_cache_mg` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_cache_mg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_cache_ml`
--

DROP TABLE IF EXISTS `exp_email_cache_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_cache_ml`
--

LOCK TABLES `exp_email_cache_ml` WRITE;
/*!40000 ALTER TABLE `exp_email_cache_ml` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_cache_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_console_cache`
--

DROP TABLE IF EXISTS `exp_email_console_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_console_cache`
--

LOCK TABLES `exp_email_console_cache` WRITE;
/*!40000 ALTER TABLE `exp_email_console_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_console_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_email_tracker`
--

DROP TABLE IF EXISTS `exp_email_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_email_tracker`
--

LOCK TABLES `exp_email_tracker` WRITE;
/*!40000 ALTER TABLE `exp_email_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_email_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_entry_ping_status`
--

DROP TABLE IF EXISTS `exp_entry_ping_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_entry_ping_status`
--

LOCK TABLES `exp_entry_ping_status` WRITE;
/*!40000 ALTER TABLE `exp_entry_ping_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_entry_ping_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_entry_versioning`
--

DROP TABLE IF EXISTS `exp_entry_versioning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_entry_versioning`
--

LOCK TABLES `exp_entry_versioning` WRITE;
/*!40000 ALTER TABLE `exp_entry_versioning` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_entry_versioning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_extensions`
--

DROP TABLE IF EXISTS `exp_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_extensions`
--

LOCK TABLES `exp_extensions` WRITE;
/*!40000 ALTER TABLE `exp_extensions` DISABLE KEYS */;
INSERT INTO `exp_extensions` VALUES (1,'Safecracker_ext','form_declaration_modify_data','form_declaration_modify_data','',10,'2.1','y'),(2,'Rte_ext','myaccount_nav_setup','myaccount_nav_setup','',10,'1.0','y'),(3,'Rte_ext','cp_menu_array','cp_menu_array','',10,'1.0','y'),(4,'Rte_ext','publish_form_entry_data','publish_form_entry_data','',10,'1.0','y'),(5,'Assets_ext','channel_entries_query_result','channel_entries_query_result','',10,'2.0.3','y'),(6,'Assets_ext','file_after_save','file_after_save','',9,'2.0.3','y'),(7,'Assets_ext','files_after_delete','files_after_delete','',8,'2.0.3','y'),(8,'Field_editor_ext','cp_menu_array','cp_menu_array','a:2:{s:13:\"global_prefix\";s:0:\"\";s:14:\"group_prefixes\";a:5:{i:1;s:5:\"page_\";i:2;s:5:\"prod_\";i:3;s:5:\"serv_\";i:4;s:5:\"actu_\";i:5;s:5:\"item_\";}}',10,'1.0.3','y'),(9,'Structure_ext','entry_submission_redirect','entry_submission_redirect','',10,'3.3.8','y'),(10,'Structure_ext','cp_member_login','cp_member_login','',10,'3.3.8','y'),(11,'Structure_ext','sessions_start','sessions_start','',10,'3.3.8','y'),(12,'Structure_ext','channel_module_create_pagination','channel_module_create_pagination','',9,'3.3.8','y'),(13,'Structure_ext','wygwam_config','wygwam_config','',10,'3.3.8','y'),(14,'Structure_ext','core_template_route','core_template_route','',10,'3.3.8','y'),(15,'Structure_ext','entry_submission_end','entry_submission_end','',10,'3.3.8','y'),(16,'Structure_ext','safecracker_submit_entry_end','safecracker_submit_entry_end','',10,'3.3.8','y'),(17,'Structure_ext','template_post_parse','template_post_parse','',10,'3.3.8','y'),(18,'Playa_ext','channel_entries_tagdata','channel_entries_tagdata','',9,'4.3.3','y'),(19,'Matrix_ext','channel_entries_tagdata','channel_entries_tagdata','',10,'2.5.3','y'),(22,'Nsm_better_meta_ext','dummy_hook_function','dummy_hook_function','a:0:{}',10,'1.1.4','y'),(21,'Freebie_ext','Freebie_ext','sessions_start','a:7:{s:9:\"to_ignore\";s:21:\"success|error|preview\";s:13:\"ignore_beyond\";s:0:\"\";s:14:\"break_category\";s:2:\"no\";s:14:\"remove_numbers\";s:2:\"no\";s:12:\"always_parse\";s:0:\"\";s:23:\"always_parse_pagination\";s:2:\"no\";s:13:\"cache_cleared\";s:3:\"yes\";}',10,'0.2','y');
/*!40000 ALTER TABLE `exp_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_field_formatting`
--

DROP TABLE IF EXISTS `exp_field_formatting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_field_formatting`
--

LOCK TABLES `exp_field_formatting` WRITE;
/*!40000 ALTER TABLE `exp_field_formatting` DISABLE KEYS */;
INSERT INTO `exp_field_formatting` VALUES (1,1,'none'),(2,1,'br'),(3,1,'xhtml'),(4,2,'none'),(5,2,'br'),(6,2,'xhtml'),(7,3,'none'),(8,3,'br'),(9,3,'xhtml'),(10,4,'none'),(11,4,'br'),(12,4,'xhtml'),(13,5,'none'),(14,5,'br'),(15,5,'xhtml'),(16,6,'none'),(17,6,'br'),(18,6,'xhtml'),(19,7,'none'),(20,7,'br'),(21,7,'xhtml'),(22,8,'none'),(23,8,'br'),(24,8,'xhtml'),(25,9,'none'),(26,9,'br'),(27,9,'xhtml'),(53,18,'br'),(52,18,'none'),(51,17,'xhtml'),(50,17,'br'),(49,17,'none'),(48,16,'xhtml'),(45,15,'xhtml'),(44,15,'br'),(43,15,'none'),(47,16,'br'),(46,16,'none'),(40,14,'none'),(41,14,'br'),(42,14,'xhtml'),(54,18,'xhtml'),(55,19,'none'),(56,19,'br'),(57,19,'xhtml'),(58,20,'none'),(59,20,'br'),(60,20,'xhtml'),(61,21,'none'),(62,21,'br'),(63,21,'xhtml'),(64,22,'none'),(65,22,'br'),(66,22,'xhtml'),(67,23,'none'),(68,23,'br'),(69,23,'xhtml'),(70,24,'none'),(71,24,'br'),(72,24,'xhtml'),(73,25,'none'),(74,25,'br'),(75,25,'xhtml'),(76,26,'none'),(77,26,'br'),(78,26,'xhtml'),(79,27,'none'),(80,27,'br'),(81,27,'xhtml'),(82,28,'none'),(83,28,'br'),(84,28,'xhtml'),(85,29,'none'),(86,29,'br'),(87,29,'xhtml'),(88,30,'none'),(89,30,'br'),(90,30,'xhtml'),(91,31,'none'),(92,31,'br'),(93,31,'xhtml'),(94,32,'none'),(95,32,'br'),(96,32,'xhtml'),(97,33,'none'),(98,33,'br'),(99,33,'xhtml'),(100,34,'none'),(101,34,'br'),(102,34,'xhtml'),(103,35,'none'),(104,35,'br'),(105,35,'xhtml'),(106,36,'none'),(107,36,'br'),(108,36,'xhtml'),(109,37,'none'),(110,37,'br'),(111,37,'xhtml'),(112,38,'none'),(113,38,'br'),(114,38,'xhtml'),(115,39,'none'),(116,39,'br'),(117,39,'xhtml'),(118,40,'none'),(119,40,'br'),(120,40,'xhtml'),(121,41,'none'),(122,41,'br'),(123,41,'xhtml'),(124,42,'none'),(125,42,'br'),(126,42,'xhtml'),(127,43,'none'),(128,43,'br'),(129,43,'xhtml'),(130,44,'none'),(131,44,'br'),(132,44,'xhtml'),(133,45,'none'),(134,45,'br'),(135,45,'xhtml'),(136,46,'none'),(137,46,'br'),(138,46,'xhtml'),(139,47,'none'),(140,47,'br'),(141,47,'xhtml'),(142,48,'none'),(143,48,'br'),(144,48,'xhtml'),(145,49,'none'),(146,49,'br'),(147,49,'xhtml'),(148,50,'none'),(149,50,'br'),(150,50,'xhtml'),(151,51,'none'),(152,51,'br'),(153,51,'xhtml'),(154,52,'none'),(155,52,'br'),(156,52,'xhtml'),(157,53,'none'),(158,53,'br'),(159,53,'xhtml'),(160,54,'none'),(161,54,'br'),(162,54,'xhtml'),(163,55,'none'),(164,55,'br'),(165,55,'xhtml'),(166,56,'none'),(167,56,'br'),(168,56,'xhtml'),(169,57,'none'),(170,57,'br'),(171,57,'xhtml'),(172,58,'none'),(173,58,'br'),(174,58,'xhtml'),(175,59,'none'),(176,59,'br'),(177,59,'xhtml'),(178,60,'none'),(179,60,'br'),(180,60,'xhtml'),(181,61,'none'),(182,61,'br'),(183,61,'xhtml'),(184,62,'none'),(185,62,'br'),(186,62,'xhtml'),(187,63,'none'),(188,63,'br'),(189,63,'xhtml'),(190,64,'none'),(191,64,'br'),(192,64,'xhtml'),(193,65,'none'),(194,65,'br'),(195,65,'xhtml'),(196,66,'none'),(197,66,'br'),(198,66,'xhtml'),(199,67,'none'),(200,67,'br'),(201,67,'xhtml');
/*!40000 ALTER TABLE `exp_field_formatting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_field_groups`
--

DROP TABLE IF EXISTS `exp_field_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_field_groups`
--

LOCK TABLES `exp_field_groups` WRITE;
/*!40000 ALTER TABLE `exp_field_groups` DISABLE KEYS */;
INSERT INTO `exp_field_groups` VALUES (1,1,'pages'),(2,1,'products'),(3,1,'services'),(4,1,'actus'),(5,1,'items'),(6,1,'home-slider');
/*!40000 ALTER TABLE `exp_field_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_fieldtypes`
--

DROP TABLE IF EXISTS `exp_fieldtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_fieldtypes`
--

LOCK TABLES `exp_fieldtypes` WRITE;
/*!40000 ALTER TABLE `exp_fieldtypes` DISABLE KEYS */;
INSERT INTO `exp_fieldtypes` VALUES (1,'select','1.0','YTowOnt9','n'),(2,'text','1.0','YTowOnt9','n'),(3,'textarea','1.0','YTowOnt9','n'),(4,'date','1.0','YTowOnt9','n'),(12,'wygwam','2.7','YTowOnt9','y'),(6,'multi_select','1.0','YTowOnt9','n'),(7,'checkboxes','1.0','YTowOnt9','n'),(8,'radio','1.0','YTowOnt9','n'),(9,'rel','1.0','YTowOnt9','n'),(10,'rte','1.0','YTowOnt9','n'),(11,'assets','2.0.3','YTowOnt9','y'),(13,'structure','3.3.8','YTowOnt9','n'),(14,'playa','4.3.3','YTowOnt9','y'),(15,'matrix','2.5.3','YTowOnt9','y'),(16,'file','1.0','YTowOnt9','n'),(17,'pt_checkboxes','1.0.3','YTowOnt9','n'),(18,'pt_dropdown','1.0.3','YTowOnt9','n'),(19,'pt_list','1.0.3','YTowOnt9','n'),(20,'pt_multiselect','1.0.3','YTowOnt9','n'),(21,'pt_pill','1.0.3','YTowOnt9','n'),(22,'pt_radio_buttons','1.0.3','YTowOnt9','n'),(23,'pt_switch','1.0.4','YTowOnt9','n'),(24,'nsm_better_meta','1.1.4','YTowOnt9','n');
/*!40000 ALTER TABLE `exp_fieldtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_file_categories`
--

DROP TABLE IF EXISTS `exp_file_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_file_categories`
--

LOCK TABLES `exp_file_categories` WRITE;
/*!40000 ALTER TABLE `exp_file_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_file_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_file_dimensions`
--

DROP TABLE IF EXISTS `exp_file_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_file_dimensions`
--

LOCK TABLES `exp_file_dimensions` WRITE;
/*!40000 ALTER TABLE `exp_file_dimensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_file_dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_file_watermarks`
--

DROP TABLE IF EXISTS `exp_file_watermarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_file_watermarks`
--

LOCK TABLES `exp_file_watermarks` WRITE;
/*!40000 ALTER TABLE `exp_file_watermarks` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_file_watermarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_files`
--

DROP TABLE IF EXISTS `exp_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_files`
--

LOCK TABLES `exp_files` WRITE;
/*!40000 ALTER TABLE `exp_files` DISABLE KEYS */;
INSERT INTO `exp_files` VALUES (1,1,'affichage-communal.pdf',2,'/Users/seba/Documents/Workspace/artelux/docs/AffComm.pdf','application/pdf','AffComm.pdf',930396,'','','',1,1359696428,1,1359696460,' '),(2,1,'080410154713_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/080410154713_foto.jpg','image/jpeg','080410154713_foto.jpg',71932,NULL,NULL,NULL,1,1359717939,1,1359717939,'600 800'),(3,1,'080410161103_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/080410161103_foto.jpg','image/jpeg','080410161103_foto.jpg',53043,NULL,NULL,NULL,1,1359717939,1,1359717939,'600 800'),(4,1,'020809192926_foto.jpg',4,'/Users/seba/Documents/Workspace/artelux/images/uploads/products/020809192926_foto.jpg','image/jpeg','020809192926_foto.jpg',66477,NULL,NULL,NULL,1,1359723068,1,1359723068,'442 600'),(5,1,'240310130402_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/240310130402_foto.jpg','image/jpeg','240310130402_foto.jpg',31771,NULL,NULL,NULL,1,1363527417,1,1363527417,'272 400'),(6,1,'240310130402_foto.jpg',3,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/items/240310130402_foto.jpg','image/jpeg','240310130402_foto.jpg',31771,NULL,NULL,NULL,1,1363527448,1,1363527448,'272 400'),(7,1,'220509132621_foto.jpg',3,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/items/220509132621_foto.jpg','image/jpeg','220509132621_foto.jpg',13326,NULL,NULL,NULL,1,1363527990,1,1363527990,'300 400'),(8,1,'220509132753_foto.jpg',3,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/items/220509132753_foto.jpg','image/jpeg','220509132753_foto.jpg',23998,NULL,NULL,NULL,1,1363528073,1,1363528073,'300 400'),(9,1,'020809192926_foto.jpg',3,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/items/020809192926_foto.jpg','image/jpeg','020809192926_foto.jpg',66477,NULL,NULL,NULL,1,1363528106,1,1363528106,'442 600'),(10,1,'040112194559_foto.jpg',3,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/items/040112194559_foto.jpg','image/jpeg','040112194559_foto.jpg',61623,NULL,NULL,NULL,1,1363530063,1,1363530063,'500 800'),(11,1,'220509140954_foto.jpg',3,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/items/220509140954_foto.jpg','image/jpeg','220509140954_foto.jpg',27181,NULL,NULL,NULL,1,1363530405,1,1363530405,'300 400'),(12,1,'ind_070410194322_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_070410194322_foto.jpg','image/jpeg','ind_070410194322_foto.jpg',91428,NULL,NULL,NULL,1,1363533424,1,1363533424,'768 576'),(13,1,'ind_080410153945_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_080410153945_foto.jpg','image/jpeg','ind_080410153945_foto.jpg',76920,NULL,NULL,NULL,1,1363533424,1,1363533424,'768 576'),(14,1,'ind_080410153900_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_080410153900_foto.jpg','image/jpeg','ind_080410153900_foto.jpg',66680,NULL,NULL,NULL,1,1363533424,1,1363533424,'600 800'),(15,1,'ind_080410154032_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_080410154032_foto.jpg','image/jpeg','ind_080410154032_foto.jpg',53845,NULL,NULL,NULL,1,1363533424,1,1363533424,'495 400'),(16,1,'ind_080410154528_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_080410154528_foto.jpg','image/jpeg','ind_080410154528_foto.jpg',74625,NULL,NULL,NULL,1,1363533424,1,1363533424,'600 800'),(17,1,'ind_080410154713_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_080410154713_foto.jpg','image/jpeg','ind_080410154713_foto.jpg',71932,NULL,NULL,NULL,1,1363533424,1,1363533424,'600 800'),(18,1,'ind_080410161103_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_080410161103_foto.jpg','image/jpeg','ind_080410161103_foto.jpg',53043,NULL,NULL,NULL,1,1363533424,1,1363533424,'600 800'),(19,1,'ind_150213150703_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_150213150703_foto.jpg','image/jpeg','ind_150213150703_foto.jpg',36638,NULL,NULL,NULL,1,1363533424,1,1363533424,'459 345'),(20,1,'ind_150213150446_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_150213150446_foto.jpg','image/jpeg','ind_150213150446_foto.jpg',44961,NULL,NULL,NULL,1,1363533424,1,1363533424,'451 800'),(21,1,'ind_190509120414_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_190509120414_foto.jpg','image/jpeg','ind_190509120414_foto.jpg',30094,NULL,NULL,NULL,1,1363533424,1,1363533424,'362 497'),(22,1,'ind_210310223650_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_210310223650_foto.jpg','image/jpeg','ind_210310223650_foto.jpg',73756,NULL,NULL,NULL,1,1363533424,1,1363533424,'600 800'),(23,1,'ind_210310224722_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_210310224722_foto.jpg','image/jpeg','ind_210310224722_foto.jpg',71487,NULL,NULL,NULL,1,1363533424,1,1363533424,'768 512'),(24,1,'ind_260509094801_foto.jpg',4,'/Users/sebastien/Documents/Workspace/artelux/images/uploads/products/ind_260509094801_foto.jpg','image/jpeg','ind_260509094801_foto.jpg',22548,NULL,NULL,NULL,1,1363533424,1,1363533424,'300 400'),(25,1,'slider_01.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_01.jpg','image/jpeg','slider_01.jpg',24559,NULL,NULL,NULL,1,1363894833,1,1363894833,'160 870'),(26,1,'slider_02.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_02.jpg','image/jpeg','slider_02.jpg',37545,NULL,NULL,NULL,1,1363894833,1,1363894833,'160 870'),(27,1,'slider_03.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_03.jpg','image/jpeg','slider_03.jpg',52129,NULL,NULL,NULL,1,1363894833,1,1363894833,'160 870'),(28,1,'slider_05.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_05.jpg','image/jpeg','slider_05.jpg',24627,NULL,NULL,NULL,1,1363894833,1,1363894833,'160 870'),(29,1,'slider_04.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_04.jpg','image/jpeg','slider_04.jpg',34046,NULL,NULL,NULL,1,1363894833,1,1363894833,'160 870'),(30,1,'slider_03_1.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_03_1.jpg','image/jpeg','slider_03_1.jpg',52129,NULL,NULL,NULL,1,1363895697,1,1363895697,'160 870'),(31,1,'slider_02_1.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_02_1.jpg','image/jpeg','slider_02_1.jpg',37545,NULL,NULL,NULL,1,1363895697,1,1363895697,'160 870'),(32,1,'slider_01_1.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_01_1.jpg','image/jpeg','slider_01_1.jpg',25676,NULL,NULL,NULL,1,1363895697,1,1363895697,'160 870'),(33,1,'slider_04_1.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_04_1.jpg','image/jpeg','slider_04_1.jpg',34046,NULL,NULL,NULL,1,1363895698,1,1363895698,'160 870'),(34,1,'slider_05_1.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_05_1.jpg','image/jpeg','slider_05_1.jpg',32320,NULL,NULL,NULL,1,1363895698,1,1363895698,'160 870'),(35,1,'slider_06.jpg',1,'/Users/sebastien/Documents/Workspace/artelux/images/pages/slider_06.jpg','image/jpeg','slider_06.jpg',30737,NULL,NULL,NULL,1,1363919901,1,1363919901,'160 870');
/*!40000 ALTER TABLE `exp_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_global_variables`
--

DROP TABLE IF EXISTS `exp_global_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_global_variables`
--

LOCK TABLES `exp_global_variables` WRITE;
/*!40000 ALTER TABLE `exp_global_variables` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_global_variables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_html_buttons`
--

DROP TABLE IF EXISTS `exp_html_buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_html_buttons`
--

LOCK TABLES `exp_html_buttons` WRITE;
/*!40000 ALTER TABLE `exp_html_buttons` DISABLE KEYS */;
INSERT INTO `exp_html_buttons` VALUES (1,1,0,'b','<strong>','</strong>','b',1,'1','btn_b'),(2,1,0,'i','<em>','</em>','i',2,'1','btn_i'),(3,1,0,'blockquote','<blockquote>','</blockquote>','q',3,'1','btn_blockquote'),(4,1,0,'a','<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>','</a>','a',4,'1','btn_a'),(5,1,0,'img','<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />','','',5,'1','btn_img');
/*!40000 ALTER TABLE `exp_html_buttons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_ip2nation`
--

DROP TABLE IF EXISTS `exp_ip2nation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_ip2nation` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `ip_range_low` varbinary(16) NOT NULL DEFAULT '0',
  `ip_range_high` varbinary(16) NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ip_range_low` (`ip_range_low`),
  KEY `ip_range_high` (`ip_range_high`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_ip2nation`
--

LOCK TABLES `exp_ip2nation` WRITE;
/*!40000 ALTER TABLE `exp_ip2nation` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_ip2nation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_ip2nation_countries`
--

DROP TABLE IF EXISTS `exp_ip2nation_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_ip2nation_countries` (
  `code` varchar(2) NOT NULL DEFAULT '',
  `banned` varchar(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_ip2nation_countries`
--

LOCK TABLES `exp_ip2nation_countries` WRITE;
/*!40000 ALTER TABLE `exp_ip2nation_countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_ip2nation_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_layout_publish`
--

DROP TABLE IF EXISTS `exp_layout_publish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_layout_publish`
--

LOCK TABLES `exp_layout_publish` WRITE;
/*!40000 ALTER TABLE `exp_layout_publish` DISABLE KEYS */;
INSERT INTO `exp_layout_publish` VALUES (5,1,1,1,'a:10:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:3:{s:10:\"_tab_label\";s:2:\"NL\";i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:3:{s:10:\"_tab_label\";s:2:\"EN\";i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:3:{s:10:\"_tab_label\";s:2:\"DE\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:4:{s:10:\"_tab_label\";s:19:\"Images et documents\";i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:14;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"nsm_better_meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(6,1,6,1,'a:10:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:3:{s:10:\"_tab_label\";s:2:\"NL\";i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:3:{s:10:\"_tab_label\";s:2:\"EN\";i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:3:{s:10:\"_tab_label\";s:2:\"DE\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:4:{s:10:\"_tab_label\";s:19:\"Images et documents\";i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:14;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"nsm_better_meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(7,1,1,4,'a:10:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:38;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:3:{s:10:\"_tab_label\";s:2:\"NL\";i:35;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:39;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:3:{s:10:\"_tab_label\";s:2:\"EN\";i:36;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:40;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:3:{s:10:\"_tab_label\";s:2:\"DE\";i:37;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:41;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:3:{s:10:\"_tab_label\";s:19:\"Images et Documents\";i:42;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:43;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:3:{s:10:\"_tab_label\";s:9:\"Structure\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(8,1,6,4,'a:10:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:38;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:3:{s:10:\"_tab_label\";s:2:\"NL\";i:35;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:39;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:3:{s:10:\"_tab_label\";s:2:\"EN\";i:36;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:40;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:3:{s:10:\"_tab_label\";s:2:\"DE\";i:37;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:41;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:3:{s:10:\"_tab_label\";s:19:\"Images et Documents\";i:42;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:43;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:3:{s:10:\"_tab_label\";s:9:\"Structure\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(11,1,1,3,'a:10:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:28;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:3:{s:10:\"_tab_label\";s:2:\"NL\";i:25;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:3:{s:10:\"_tab_label\";s:2:\"EN\";i:26;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:3:{s:10:\"_tab_label\";s:2:\"DE\";i:27;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:4:{s:10:\"_tab_label\";s:19:\"Images et Documents\";i:32;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:34;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(12,1,6,3,'a:10:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:28;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:3:{s:10:\"_tab_label\";s:2:\"NL\";i:25;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:3:{s:10:\"_tab_label\";s:2:\"EN\";i:26;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:3:{s:10:\"_tab_label\";s:2:\"DE\";i:27;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:4:{s:10:\"_tab_label\";s:19:\"Images et Documents\";i:32;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:34;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(15,1,1,2,'a:10:{s:7:\"publish\";a:5:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:59;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:4:{s:10:\"_tab_label\";s:2:\"NL\";i:15;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:60;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:4:{s:10:\"_tab_label\";s:2:\"EN\";i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:61;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:4:{s:10:\"_tab_label\";s:2:\"DE\";i:17;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:62;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:18:\"image_et_documents\";a:4:{s:10:\"_tab_label\";s:18:\"Image et Documents\";i:22;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(16,1,6,2,'a:10:{s:7:\"publish\";a:5:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:59;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:4:{s:10:\"_tab_label\";s:2:\"NL\";i:15;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:60;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:4:{s:10:\"_tab_label\";s:2:\"EN\";i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:61;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:4:{s:10:\"_tab_label\";s:2:\"DE\";i:17;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:62;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:18:\"image_et_documents\";a:4:{s:10:\"_tab_label\";s:18:\"Image et Documents\";i:22;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(21,1,1,5,'a:10:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:63;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:47;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:51;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:58;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:5:{s:10:\"_tab_label\";s:2:\"NL\";i:44;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:64;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:48;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:52;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:5:{s:10:\"_tab_label\";s:2:\"EN\";i:45;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:65;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:49;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:53;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:5:{s:10:\"_tab_label\";s:2:\"DE\";i:46;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:66;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:50;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:54;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:4:{s:10:\"_tab_label\";s:19:\"Images et Documents\";i:55;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:56;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:57;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),(22,1,6,5,'a:10:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publier\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:63;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:47;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:51;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:58;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:2:\"nl\";a:5:{s:10:\"_tab_label\";s:2:\"NL\";i:44;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:64;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:48;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:52;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:2:\"en\";a:5:{s:10:\"_tab_label\";s:2:\"EN\";i:45;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:65;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:49;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:53;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:2:\"de\";a:5:{s:10:\"_tab_label\";s:2:\"DE\";i:46;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:66;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:50;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:54;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"images_et_documents\";a:4:{s:10:\"_tab_label\";s:19:\"Images et Documents\";i:55;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:56;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:57;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:11:\"Catégories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"nsm_better_meta\";a:2:{s:10:\"_tab_label\";s:15:\"NSM Better Meta\";s:21:\"nsm_better_meta__meta\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
/*!40000 ALTER TABLE `exp_layout_publish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_matrix_cols`
--

DROP TABLE IF EXISTS `exp_matrix_cols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_matrix_cols`
--

LOCK TABLES `exp_matrix_cols` WRITE;
/*!40000 ALTER TABLE `exp_matrix_cols` DISABLE KEYS */;
INSERT INTO `exp_matrix_cols` VALUES (1,1,14,NULL,'doc_title','Titre du document','','text','n','n',0,'40%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),(2,1,14,NULL,'doc_desc','Description du document','','rte','n','n',1,'40%','YToxOntzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7fQ=='),(3,1,14,NULL,'doc_lang','Langue du document','','pt_multiselect','n','n',2,'10%','YToxOntzOjc6Im9wdGlvbnMiO2E6NDp7czoyOiJmciI7czo5OiJGcmFuw6dhaXMiO3M6MjoibmwiO3M6MTE6Ik5lZWRlcmxhbmRzIjtzOjI6ImVuIjtzOjc6IkVuZ2xpc2giO3M6MjoiZGUiO3M6NzoiRGV1dHNjaCI7fX0='),(4,1,14,NULL,'doc_file','Fichier','','file','n','n',3,'10%','YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),(5,1,24,NULL,'doc_title','Titre du document','','text','n','n',0,'40%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),(6,1,24,NULL,'doc_desc','Description du document','','rte','n','n',1,'40%','YToxOntzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7fQ=='),(7,1,24,NULL,'doc_lang','Langue du document','','pt_multiselect','n','n',2,'10%','YToxOntzOjc6Im9wdGlvbnMiO2E6NDp7czoyOiJmciI7czo5OiJGcmFuw6dhaXMiO3M6MjoibmwiO3M6MTE6Ik5lZWRlcmxhbmRzIjtzOjI6ImVuIjtzOjc6IkVuZ2xpc2giO3M6MjoiZGUiO3M6NzoiRGV1dHNjaCI7fX0='),(8,1,24,NULL,'doc_file','Fichier','','file','n','n',3,'10%','YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),(9,1,34,NULL,'doc_title','Titre du document','','text','n','n',0,'40%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),(10,1,34,NULL,'doc_desc','Description du document','','rte','n','n',1,'40%','YToxOntzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7fQ=='),(11,1,34,NULL,'doc_lang','Langue du document','','pt_multiselect','n','n',2,'10%','YToxOntzOjc6Im9wdGlvbnMiO2E6NDp7czoyOiJmciI7czo5OiJGcmFuw6dhaXMiO3M6MjoibmwiO3M6MTE6Ik5lZWRlcmxhbmRzIjtzOjI6ImVuIjtzOjc6IkVuZ2xpc2giO3M6MjoiZGUiO3M6NzoiRGV1dHNjaCI7fX0='),(12,1,34,NULL,'doc_file','Fichier','','file','n','n',3,'10%','YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),(13,1,42,NULL,'doc_title','Titre du document','','text','n','n',0,'40%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),(14,1,42,NULL,'doc_desc','Description du document','','rte','n','n',1,'40%','YToxOntzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7fQ=='),(15,1,42,NULL,'doc_lang','Langue du document','','pt_multiselect','n','n',2,'10%','YToxOntzOjc6Im9wdGlvbnMiO2E6NDp7czoyOiJmciI7czo5OiJGcmFuw6dhaXMiO3M6MjoibmwiO3M6MTE6Ik5lZWRlcmxhbmRzIjtzOjI6ImVuIjtzOjc6IkVuZ2xpc2giO3M6MjoiZGUiO3M6NzoiRGV1dHNjaCI7fX0='),(16,1,42,NULL,'doc_file','Fichier','','file','n','n',3,'10%','YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),(17,1,57,NULL,'doc_title','Titre du document','','text','n','n',0,'40%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='),(18,1,57,NULL,'doc_desc','Description du document','','rte','n','n',1,'40%','YToxOntzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7fQ=='),(19,1,57,NULL,'doc_lang','Langue du document','','pt_multiselect','n','n',2,'10%','YToxOntzOjc6Im9wdGlvbnMiO2E6NDp7czoyOiJmciI7czo5OiJGcmFuw6dhaXMiO3M6MjoibmwiO3M6MTE6Ik5lZWRlcmxhbmRzIjtzOjI6ImVuIjtzOjc6IkVuZ2xpc2giO3M6MjoiZGUiO3M6NzoiRGV1dHNjaCI7fX0='),(20,1,57,NULL,'doc_file','Fichier','','file','n','n',3,'10%','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIyIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9');
/*!40000 ALTER TABLE `exp_matrix_cols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_matrix_data`
--

DROP TABLE IF EXISTS `exp_matrix_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  `col_id_14` text,
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_matrix_data`
--

LOCK TABLES `exp_matrix_data` WRITE;
/*!40000 ALTER TABLE `exp_matrix_data` DISABLE KEYS */;
INSERT INTO `exp_matrix_data` VALUES (1,1,9,24,NULL,0,1,NULL,NULL,NULL,NULL,'Brochure affichage communal','<p>​Cette brochure présente nos produits destinés à l\'affichage informatif communal</p>','fr','{filedir_2}AffComm.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `exp_matrix_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_bulletin_board`
--

DROP TABLE IF EXISTS `exp_member_bulletin_board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_bulletin_board`
--

LOCK TABLES `exp_member_bulletin_board` WRITE;
/*!40000 ALTER TABLE `exp_member_bulletin_board` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_member_bulletin_board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_data`
--

DROP TABLE IF EXISTS `exp_member_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_data`
--

LOCK TABLES `exp_member_data` WRITE;
/*!40000 ALTER TABLE `exp_member_data` DISABLE KEYS */;
INSERT INTO `exp_member_data` VALUES (1),(2),(3);
/*!40000 ALTER TABLE `exp_member_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_fields`
--

DROP TABLE IF EXISTS `exp_member_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_fields`
--

LOCK TABLES `exp_member_fields` WRITE;
/*!40000 ALTER TABLE `exp_member_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_member_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_groups`
--

DROP TABLE IF EXISTS `exp_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_groups`
--

LOCK TABLES `exp_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_member_groups` DISABLE KEYS */;
INSERT INTO `exp_member_groups` VALUES (1,1,'Super Admins','','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','','y','y','y',0,'y',20,60,'y','y','y','y','y'),(2,1,'Banned','','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','','n','n','n',60,'n',20,60,'n','n','n','n','n'),(3,1,'Guests','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),(4,1,'Pending','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),(5,1,'Members','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','n','','y','n','y',10,'y',20,60,'y','n','n','y','y'),(6,1,'Administrateurs','Les administrateur peuvent gérer l\'ensemble du contenu du site, ainsi que ses membres. Ils peuvent également éditer les templates. Les limitations appliquées à ce groupe concernent uniquement les fonctions de configuration avancées.','y','y','y','y','y','y','y','y','n','y','n','n','n','n','n','y','n','n','y','y','y','y','y','y','n','y','y','y','y','y','y','y','n','y','n','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','sebastien@sebisme.be','y','n','y',15,'y',20,60,'y','y','y','y','y');
/*!40000 ALTER TABLE `exp_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_homepage`
--

DROP TABLE IF EXISTS `exp_member_homepage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_homepage`
--

LOCK TABLES `exp_member_homepage` WRITE;
/*!40000 ALTER TABLE `exp_member_homepage` DISABLE KEYS */;
INSERT INTO `exp_member_homepage` VALUES (1,'l',1,'l',2,'n',0,'r',1,'n',0,'r',2,'r',0,'l',0),(2,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),(3,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0);
/*!40000 ALTER TABLE `exp_member_homepage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_member_search`
--

DROP TABLE IF EXISTS `exp_member_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_member_search`
--

LOCK TABLES `exp_member_search` WRITE;
/*!40000 ALTER TABLE `exp_member_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_member_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_members`
--

DROP TABLE IF EXISTS `exp_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_members`
--

LOCK TABLES `exp_members` WRITE;
/*!40000 ALTER TABLE `exp_members` DISABLE KEYS */;
INSERT INTO `exp_members` VALUES (1,1,'sebisme','sebastien','72fd61ec0862db392cfaeff77c04d0d4e8f5234c3d031142e9e57fde42fb19c993c5a47c6aea90f7e08af7cb7b9c97fdb0816e257ddf1d2b204188c317b58574','qy6lQ\'J>f*AG&F6%;4!6]g`=D9URat.Yo5:6pYBfhDQJ16mSMdSrVl|pI^W7Ln$XeOnJ49nq@e&fRy4lFG7o%8{}WA$t%r7?yZ0WU5pH3%LapaiR>`GpMA3;ky5yy[=t','b90ec2998fe4087d1c86ec6c6a908a54ce074048','c3c05914871cfb8cb374697bd2812f99ad8f37b7',NULL,'sebastien@sebisme.be',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1359497388,1363902408,1363928801,31,0,0,0,1363929029,0,0,0,'n','y','y','y','y','y','y','y','y','french','UP1','n','n','eu',NULL,NULL,NULL,NULL,'20',NULL,'18','','Structure|C=addons_modules&M=show_module_cp&module=structure|1\nMulti Language|C=addons_modules&M=show_module_cp&module=multi_language|2','y',0,'y',0),(2,1,'raymond','raymond','169e26cebf892d691b5529b11706411d6cbe0e2c3e19cd0d6a4d6fe5a4ba5cc2ae92ec87b5b1acdb3e5a2e094a1ed7576a11e19b5223a44db2501ebd9c234df2','}6R(w{g2PKZ*^U*50sPl_d>7XSnb#}c$rkNlTL::;OHIx8dL/?b7R*aLfQQ@N-xs=g<B}w.lT4^*USl[a\\Z6~&@m>TDO{]]faBHpknE\'f-J$:l*M;?vt.Wt7K~^#V5#:','b14d3670778cdbc185b61f612211dd063dae0031','cbaae8b59b671227c9246e79895864e3c263dcf4',NULL,'info@artelux.be','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1363540225,0,0,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','french','UP1','n','n','eu',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0,'y',0),(3,1,'artelux','artelux','465a7be6f5757f1ad8b6c04b9c23b3608b685c0ff4c8ce5787d3fde4841af0bd26fc23eccd2a006ee89b5d7ff7a206ed9652e0e7201b9b63874647f4f9535089','P@LEBV8;)HD>W..Ky,f8<Y\'B}<J<+]^OrgD{QP\'{0jTQ\\m8M[c.dQ<j|\\c(q6]QSXO([yT$:nRq+@?>.(G35~Dt%[|]rB7h|sfd05O2WoC!fW^WFCf[ATFno/>AqYV8,','bdb3234b58f9e5b71e0a0d97b598563a1d298b65','ae7439e8ff6f4a2ea11fa0f2c9304c83ae274308',NULL,'artelux@artelux.be','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1363914882,0,0,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','french','UP1','n','n','eu',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0,'y',0);
/*!40000 ALTER TABLE `exp_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_attachments`
--

DROP TABLE IF EXISTS `exp_message_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_attachments`
--

LOCK TABLES `exp_message_attachments` WRITE;
/*!40000 ALTER TABLE `exp_message_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_copies`
--

DROP TABLE IF EXISTS `exp_message_copies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_copies`
--

LOCK TABLES `exp_message_copies` WRITE;
/*!40000 ALTER TABLE `exp_message_copies` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_copies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_data`
--

DROP TABLE IF EXISTS `exp_message_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_data`
--

LOCK TABLES `exp_message_data` WRITE;
/*!40000 ALTER TABLE `exp_message_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_folders`
--

DROP TABLE IF EXISTS `exp_message_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_folders`
--

LOCK TABLES `exp_message_folders` WRITE;
/*!40000 ALTER TABLE `exp_message_folders` DISABLE KEYS */;
INSERT INTO `exp_message_folders` VALUES (1,'InBox','Sent','','','','','','','','');
/*!40000 ALTER TABLE `exp_message_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_message_listed`
--

DROP TABLE IF EXISTS `exp_message_listed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_message_listed`
--

LOCK TABLES `exp_message_listed` WRITE;
/*!40000 ALTER TABLE `exp_message_listed` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_message_listed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_module_member_groups`
--

DROP TABLE IF EXISTS `exp_module_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_module_member_groups`
--

LOCK TABLES `exp_module_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_module_member_groups` DISABLE KEYS */;
INSERT INTO `exp_module_member_groups` VALUES (6,1),(6,6),(6,11),(6,12),(6,13),(6,15),(6,16),(6,17),(6,18),(6,23);
/*!40000 ALTER TABLE `exp_module_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_modules`
--

DROP TABLE IF EXISTS `exp_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_modules`
--

LOCK TABLES `exp_modules` WRITE;
/*!40000 ALTER TABLE `exp_modules` DISABLE KEYS */;
INSERT INTO `exp_modules` VALUES (1,'Comment','2.3','y','n'),(2,'Email','2.0','n','n'),(3,'Emoticon','2.0','n','n'),(4,'Jquery','1.0','n','n'),(5,'Rss','2.0','n','n'),(6,'Safecracker','2.1','y','n'),(7,'Search','2.2','n','n'),(8,'Channel','2.0.1','n','n'),(9,'Member','2.1','n','n'),(10,'Stats','2.0','n','n'),(11,'Rte','1.0','y','n'),(12,'Assets','2.0.3','y','n'),(13,'Cp_analytics','2.0.9','y','n'),(14,'File','1.0.0','n','n'),(15,'Field_editor','1.0.3','y','n'),(16,'Wygwam','2.7','y','n'),(17,'Structure','3.3.8','y','y'),(18,'Ip_to_nation','3.0','y','n'),(19,'Query','2.0','n','n'),(20,'Playa','4.3.3','n','n'),(24,'Nsm_better_meta','1.1.4','n','y'),(23,'Multi_language','2.0','y','n');
/*!40000 ALTER TABLE `exp_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_multi_language`
--

DROP TABLE IF EXISTS `exp_multi_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_multi_language` (
  `phrase_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `phrase_index` varchar(200) NOT NULL DEFAULT '',
  `phrase_en` text NOT NULL,
  `phrase_de` text NOT NULL,
  `phrase_nl` text,
  `phrase_fr` text,
  PRIMARY KEY (`phrase_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_multi_language`
--

LOCK TABLES `exp_multi_language` WRITE;
/*!40000 ALTER TABLE `exp_multi_language` DISABLE KEYS */;
INSERT INTO `exp_multi_language` VALUES (1,'telecharger','Download','Download','Download','Télécharger'),(2,'more_infos','More infos','Mehr Infos','Meer infos','Plus d\'infos'),(3,'notre_catalogue','Our Catalogue','Unsere Katalog','Onze catalog','Notre catalogue'),(4,'declinaisons','Available versions','Déclinaisons disponibles','Déclinaisons disponibles','Déclinaisons disponibles');
/*!40000 ALTER TABLE `exp_multi_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_nsm_addon_settings`
--

DROP TABLE IF EXISTS `exp_nsm_addon_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_nsm_addon_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) unsigned NOT NULL DEFAULT '1',
  `addon_id` varchar(255) NOT NULL,
  `settings` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_nsm_addon_settings`
--

LOCK TABLES `exp_nsm_addon_settings` WRITE;
/*!40000 ALTER TABLE `exp_nsm_addon_settings` DISABLE KEYS */;
INSERT INTO `exp_nsm_addon_settings` VALUES (1,1,'nsm_better_meta','{\"enabled\":\"1\",\"channels\":{\"1\":{\"enabled_fields\":[\"title\",\"description\",\"keywords\",\"canonical_url\"],\"sitemap_include\":\"y\",\"sitemap_change_frequency\":\"Weekly\",\"sitemap_priority\":\"0.5\"}},\"default_site_meta\":{\"site_title\":\"Artelux - Tableaux d\'affichage & Journaux lumineux \",\"description\":\"\",\"keywords\":\"\",\"author\":\"\",\"publisher\":\"\",\"rights\":\"\",\"geo_region\":\"\",\"geo_placename\":\"\",\"geo_latitude\":\"\",\"geo_longitude\":\"\",\"robots_index\":\"y\",\"robots_archive\":\"y\",\"robots_follow\":\"y\"},\"divider\":\"1\",\"meta_template\":\"<title>{title}<\\/title>\\n<meta name=\\\"description\\\" content=\\\"{description}\\\" \\/>\\n<meta name=\\\"keywords\\\" content=\\\"{keywords}\\\" \\/>\\n<meta name=\\\"robots\\\" content=\\\"{robots}\\\" \\/>\\n\\n<meta name=\\\"geo.position\\\" content=\\\"{geo_latitude},{geo_longitude}\\\" \\/>\\n<meta name=\\\"geo.placename\\\" content=\\\"{geo_placename}\\\" \\/>\\n<meta name=\\\"geo.region\\\" content=\\\"{geo_region}\\\" \\/>\\n\\n{if canonical_url} <link rel=\\\"canonical_url\\\" href=\\\"{canonical_url}\\\" \\/> {\\/if}\"}');
/*!40000 ALTER TABLE `exp_nsm_addon_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_nsm_better_meta`
--

DROP TABLE IF EXISTS `exp_nsm_better_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_nsm_better_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `site_id` int(5) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(6) unsigned DEFAULT NULL,
  `language_id` varchar(255) DEFAULT NULL,
  `entry_default` varchar(1) DEFAULT 'n',
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `keywords_append_default` varchar(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `rights` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `canonical_url` varchar(255) DEFAULT NULL,
  `robots_index` varchar(1) DEFAULT NULL,
  `robots_follow` varchar(1) DEFAULT NULL,
  `robots_archive` varchar(1) DEFAULT NULL,
  `sitemap_priority` double(2,1) DEFAULT '0.5',
  `sitemap_change_frequency` varchar(20) DEFAULT NULL,
  `sitemap_include` varchar(1) DEFAULT NULL,
  `geo_region` varchar(255) DEFAULT NULL,
  `geo_placename` varchar(255) DEFAULT NULL,
  `geo_latitude` double(8,5) DEFAULT NULL,
  `geo_longitude` double(8,5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_nsm_better_meta`
--

LOCK TABLES `exp_nsm_better_meta` WRITE;
/*!40000 ALTER TABLE `exp_nsm_better_meta` DISABLE KEYS */;
INSERT INTO `exp_nsm_better_meta` VALUES (1,3,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,4,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,5,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,6,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,7,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,8,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,9,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,10,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,11,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,12,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,13,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,14,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,15,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,16,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,17,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,18,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,19,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,20,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,21,1,5,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,22,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,23,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,24,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,25,1,6,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,26,1,2,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,27,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,28,1,3,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,29,1,3,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,30,1,3,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,31,1,1,NULL,'y',NULL,NULL,'n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,32,1,4,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,33,1,4,NULL,'y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `exp_nsm_better_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_online_users`
--

DROP TABLE IF EXISTS `exp_online_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_online_users`
--

LOCK TABLES `exp_online_users` WRITE;
/*!40000 ALTER TABLE `exp_online_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_online_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_password_lockout`
--

DROP TABLE IF EXISTS `exp_password_lockout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_password_lockout`
--

LOCK TABLES `exp_password_lockout` WRITE;
/*!40000 ALTER TABLE `exp_password_lockout` DISABLE KEYS */;
INSERT INTO `exp_password_lockout` VALUES (1,1363120218,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22','seba'),(2,1363120224,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22','sebastien'),(3,1363120236,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22','breakfast'),(4,1363120243,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22','seba'),(5,1363120253,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22','sebastien');
/*!40000 ALTER TABLE `exp_password_lockout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_ping_servers`
--

DROP TABLE IF EXISTS `exp_ping_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_ping_servers`
--

LOCK TABLES `exp_ping_servers` WRITE;
/*!40000 ALTER TABLE `exp_ping_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_ping_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_playa_relationships`
--

DROP TABLE IF EXISTS `exp_playa_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_playa_relationships`
--

LOCK TABLES `exp_playa_relationships` WRITE;
/*!40000 ALTER TABLE `exp_playa_relationships` DISABLE KEYS */;
INSERT INTO `exp_playa_relationships` VALUES (14,10,58,NULL,NULL,NULL,9,0),(22,17,58,NULL,NULL,NULL,11,0),(23,18,58,NULL,NULL,NULL,11,0),(15,12,58,NULL,NULL,NULL,9,0),(21,20,58,NULL,NULL,NULL,11,0);
/*!40000 ALTER TABLE `exp_playa_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_relationships`
--

DROP TABLE IF EXISTS `exp_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_relationships`
--

LOCK TABLES `exp_relationships` WRITE;
/*!40000 ALTER TABLE `exp_relationships` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_remember_me`
--

DROP TABLE IF EXISTS `exp_remember_me`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_remember_me`
--

LOCK TABLES `exp_remember_me` WRITE;
/*!40000 ALTER TABLE `exp_remember_me` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_remember_me` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_reset_password`
--

DROP TABLE IF EXISTS `exp_reset_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_reset_password`
--

LOCK TABLES `exp_reset_password` WRITE;
/*!40000 ALTER TABLE `exp_reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_reset_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_revision_tracker`
--

DROP TABLE IF EXISTS `exp_revision_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_revision_tracker`
--

LOCK TABLES `exp_revision_tracker` WRITE;
/*!40000 ALTER TABLE `exp_revision_tracker` DISABLE KEYS */;
INSERT INTO `exp_revision_tracker` VALUES (1,1,'exp_templates','template_data',1359693468,1,'{embed=\"inc/head\"}\n<body id=\"index\" class=\"general\">\n	<div class=\"container\">\n		<div class=\"row\">\n			<div class=\"span3\">\n				{embed=\"inc/sidebar\"}\n			</div>\n			<div class=\"span9\">\n				{embed=\"inc/header\"}\n				<div class=\"page-header\">\n					{exp:channel:entries channel=\"pages\" limit=\"1\" entry_id=\"3\"}\n					{page_contenu_{user_language}}\n					{/exp:channel:entries}\n				</div>\n				\n				\n				<ul class=\"thumbnails\">\n					<li class=\"span3\">\n						<div class=\"thumbnail\">\n							<img src=\"http://placehold.it/260x200\">\n							<div class=\"caption\">\n								<h3>Thumbnail label</h3>\n								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n								<p><a href=\"#\" class=\"btn btn-primary\">Action</a> <a href=\"#\" class=\"btn\">Action</a></p>\n							</div>\n						</div>\n					</li>\n					<li class=\"span3\">\n						<div class=\"thumbnail\">\n							<img src=\"http://placehold.it/260x200\">\n							<div class=\"caption\">\n								<h3>Thumbnail label</h3>\n								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n								<p><a href=\"#\" class=\"btn btn-primary\">Action</a> <a href=\"#\" class=\"btn\">Action</a></p>\n							</div>\n						</div>\n					</li>\n					<li class=\"span3\">\n						<div class=\"thumbnail\">\n							<img src=\"http://placehold.it/260x200\">\n							<div class=\"caption\">\n								<h3>Thumbnail label</h3>\n								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n								<p><a href=\"#\" class=\"btn btn-primary\">Action</a> <a href=\"#\" class=\"btn\">Action</a></p>\n							</div>\n						</div>\n					</li>\n				</ul>\n				\n				\n			</div>\n		</div>\n		<div class=\"row\">\n			{embed=\"inc/footer\"}\n		</div>\n	</div>\n	\n	{embed=\"inc/jsinclusion\"}\n</body>\n</html>\n'),(2,11,'exp_templates','template_data',1359693468,1,'<div class=\"span9 offset3\">\n	<div class=\"copyright well well-small\">\n		&copy; 2009-2013 - Artelux - <a href=\"#\" title=\"Disclaimer &amp; Privacy\">Disclaimer &amp; Privacy</a> - <a href=\"#\" title=\"Map du site\">Map du site</a>\n	</div>\n</div>'),(3,3,'exp_templates','template_data',1359693468,1,'<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->\n<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->\n<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->\n<head>\n	<meta charset=\"utf-8\">\n	<title>Artelux</title>\n	<meta name=\"description\" content=\"\">\n	<meta name=\"viewport\" content=\"width=device-width\">\n	\n	<link rel=\"stylesheet\" href=\"{theme_folder_url}artelux/css/bootstrap-responsive.css\">\n	<link rel=\"stylesheet\" href=\"{theme_folder_url}artelux/css/screen.css\">\n	\n	<script src=\"{theme_folder_url}deux-ourthes/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js\"></script>\n</head>\n'),(4,9,'exp_templates','template_data',1359693468,1,'<div class=\"site-header\">\n	<h2>Solutions sur mesure pour journaux lumineux, displays et marquoirs de score</h2>\n</div>\n\n{embed=\"inc/navigation\"}\n\n{embed=\"inc/breadcrumb\"}'),(5,12,'exp_templates','template_data',1359693468,1,'	<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js\"></script>\n	<script>window.jQuery || document.write(\'<script src=\"{theme_folder_url}artelux/js/vendor/jquery-1.8.3.min.js\"><\\/script>\')</script>\n	\n	<script src=\"{theme_folder_url}artelux/js/vendor/bootstrap.min.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/plugins.js\"></script>\n	\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox-buttons.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox-media.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox-thumbs.js\"></script>\n	\n	<script src=\"{theme_folder_url}artelux/js/main.js\"></script>\n	\n	<script>\n		// var _gaq=[[\'_setAccount\',\'UA-XXXXX-X\'],[\'_trackPageview\']];\n		// (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n		// g.src=(\'https:\'==location.protocol?\'//ssl\':\'//www\')+\'.google-analytics.com/ga.js\';\n		// s.parentNode.insertBefore(g,s)}(document,\'script\'));\n	</script>'),(6,10,'exp_templates','template_data',1359693468,1,'<div class=\"navbar navbar-blued\">\n	<div class=\"navbar-inner\">\n		<div class=\"container\">\n			\n			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->\n			<a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">\n				<span class=\"icon-bar\"></span>\n				<span class=\"icon-bar\"></span>\n				<span class=\"icon-bar\"></span>\n			</a>\n			\n			<!-- Everything you want hidden at 940px or less, place within here -->\n			<div class=\"nav-collapse collapse\">\n				<!-- .nav, .navbar-search, .navbar-form, etc -->\n				<ul class=\"nav\">\n					<li class=\"active\">\n						<a href=\"#\">Accueil</a>\n					</li>\n					<li class=\"dropdown\">\n						<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Produits <b class=\"caret\"></b></a>\n						<ul class=\"dropdown-menu\">\n							<li class=\"nav-header\">\n								Notre gamme de produits\n							</li>\n							<li>\n								<a href=\"#\">Journaux lumineux et displays</a>\n							</li>\n							<li>\n								<a href=\"#\">Les marquoirs de score</a>\n							</li>\n							<li>\n								<a href=\"#\">Affiicheur de sécurité industriel</a>\n							</li>\n							<li>\n								<a href=\"#\">Ecrans géants à diodes full color</a>\n							</li>\n							<li>\n								<a href=\"#\">La signalisation routière et de parking</a>\n							</li>\n						</ul>\n					</li>\n					<li class=\"dropdown\">\n						<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Services <b class=\"caret\"></b></a>\n						<ul class=\"dropdown-menu\">\n							<li class=\"nav-header\">\n								Services et prestations\n							</li>\n							<li>\n								<a href=\"#\">Placement</a>\n							</li>\n							<li>\n								<a href=\"#\">Entretien</a>\n							</li>\n							<li>\n								<a href=\"#\">Garantie</a>\n							</li>\n						</ul>\n					</li>\n					<li>\n						<a href=\"#\">Actualité</a>\n					</li>\n					<li>\n						<a href=\"#\">Contact</a>\n					</li>\n					<li>\n						<a href=\"#\">Liens</a>\n					</li>\n				</ul>\n				\n				<ul class=\"nav pull-right\">\n					<li class=\"divider-vertical\"></li>\n					<li class=\"dropdown\">\n						<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Langue <b class=\"caret\"></b></a>\n						<ul class=\"dropdown-menu\">\n							<li>\n								<a href=\"#\">Français <i class=\"icon-ok\"></i></a>\n							</li>\n							<li>\n								<a href=\"#\">Neederlands</a>\n							</li>\n							<li>\n								<a href=\"#\">English</a>\n							</li>\n							<li>\n								<a href=\"#\">Deutsch</a>\n							</li>\n						</ul>\n					</li>\n				</ul>\n				\n			</div>\n			\n		</div>\n	</div>\n</div>\n'),(7,13,'exp_templates','template_data',1359693468,1,'<!-- Main logo -->\n<div class=\"main-logo\">\n	<img src=\"{theme_folder_url}artelux/img/logo_artelux.png\" width=\"270\" height=\"215\" alt=\"Artelux\" title=\"Artelux | No light, no business\">\n	<h1 class=\"company\">Artelux</h1>\n	<h2 class=\"baseline\">No light, no business</h2>\n</div>\n\n<!-- Address -->\n<div class=\"contact-infos\">\n	<div class=\"well well-small\">\n		<i class=\"icon-map-marker\"></i> <span class=\"blued embossed-on-lightgrey\">Contactez-nous</span>\n	</div>\n	<div class=\"vcard\">\n		<div class=\"org\"><strong>Artelux</strong></div>\n		<a class=\"email\" href=\"mailto:info@artelux.be\">info@artelux.be</a>\n		<div class=\"adr\">\n			<div class=\"street-address\">La Picherotte, 28</div>\n			<span class=\"postal-code\">B-4053</span>, <span class=\"locality\">Embourg</span><br>\n			<span class=\"region\">Liège</span>, <span class=\"country-name\">Belgium</span>\n		</div>\n		<div class=\"tel\">+32 4 365 17 00</div>\n		<div class=\"tel\">+32 4 367 27 07</div>\n		\n	</div>\n	<p>Vous pouvez aussi remplir le <a href=\"#\">formulaire de contact</a>.</p>\n</div>\n\n<!-- Actus -->\n<div class=\"actu-widget\">\n	<div class=\"well well-small\">\n		<i class=\"icon-tag\"></i> <span class=\"blued embossed-on-lightgrey\">Actualités</span>\n	</div>\n	<ul>\n		<li>\n			20/05/2009 - <a href=\"#\" title=\"City Information and Prevention Screen : CIPS\">City Information...</a>\n		</li>\n		<li>\n			20/05/2009 - <a href=\"#\" title=\"Panneaux zone 30\">Panneaux \"Zone 30\"...</a>\n		</li>\n		<li>\n			20/05/2009 - <a href=\"#\" title=\"Afficheurs communaux Info Ville\">Afficheurs commu...</a>\n		</li>\n	</ul>\n</div>\n\n<!-- Newsletter -->\n<div class=\"newsletter-enroll\">\n	<div class=\"well well-small\">\n		<i class=\"icon-envelope\"></i> <span class=\"blued embossed-on-lightgrey\">Newsletter</span>\n	</div>\n	<p>Abonnez vous à notre newsletter et restez au courant des dernières nouveautés!</p>\n	\n	<form class=\"\">\n		<fieldset>\n			<legend>Newsletter</legend>\n			<label for=\"firstname\">Prénom</label>\n			<input class=\"input-medium\" type=\"text\" name=\"firstname\" id=\"firstname\" placeholder=\"Prénom\">\n			<label for=\"name\">Nom</label>\n			<input class=\"input-medium\" type=\"text\" name=\"name\" id=\"name\" placeholder=\"Nom\">\n			<label for=\"email\">Email</label>\n			<input class=\"input-medium\" type=\"text\" name=\"email\" id=\"email\" placeholder=\"Email\">\n			<br>\n			<button type=\"submit\" class=\"btn\">Envoyer</button>\n		</fieldset>\n	</form>\n</div>'),(8,23,'exp_templates','template_data',1359730310,1,'{exp:structure:entries parent_id=\"{embed:entry_id}\"}\n						<p>{title}</p>\n						<p><a href=\"{page_uri}\">{page_uri}</a></p>\n						{/exp:structure:entries}'),(9,24,'exp_templates','template_data',1363927152,1,'{embed=\"inc/head\"}\n<body id=\"index\" class=\"general\">\n	<div class=\"container\">\n		<div class=\"row\">\n			<div class=\"span3\">\n				{embed=\"inc/sidebar\"}\n			</div>\n			<div class=\"span9\">\n				{embed=\"inc/header\"}\n				\n				{embed=\"inc/breadcrumb\"}\n				\n				<div class=\"row\">\n					<div class=\"span6\">\n						{exp:channel:entries}\n						<div class=\"maincol top-product-container\">\n							{prod_contenu_{user_language}}\n						</div>\n						\n						{embed=\"produits/itemlist\" entry_id=\"{entry_id}\"}\n						\n						{/exp:channel:entries}\n					</div>\n					\n					<div class=\"span3\">\n						{exp:channel:entries}\n						<!-- Galerie -->\n						{if \"{prod_galerie:total_files}\" >= 1}\n						<ul class=\"thumbnails\">\n							{prod_galerie}\n							<li>\n								<a href=\"{url}\" class=\"thumbnail galerie-th fancybox\" rel=\"inpage-galerie\" title=\"{title}\">\n									{exp:ce_img:pair:crpd src=\"{url}\" crop=\"yes\" max_width=\"60\" min_width=\"60\" max_height=\"60\" min_height=\"60\"}\n									<img src=\"{crpd:made}\" title=\"{title}\" width=\"{crpd:width}\" height=\"{crpd:height}\" class=\"\" alt=\"{alt_text}\"/>\n									{/exp:ce_img:pair:crpd}\n								</a>\n							</li>\n							{/prod_galerie}\n						</ul>\n						{/if}\n						\n						<!-- Documents -->\n						{if \"{prod_documents:total_rows}\" >= 1}\n						<ul class=\"linked-docs\">\n							<h3>{exp:multi_language:phrase index=\"documents_lies\"}</h3>\n							{prod_documents}\n							<li>\n								<h4>\n									<i class=\"icon-file\"></i> <a href=\"{doc_file}\" class=\"doc-dl\" title=\"{exp:multi_language:phrase index=\"telecharger\"} : {doc_title}\" data-placement=\"left\">{doc_title}</a>\n								</h4>\n								{doc_desc}\n							</li>\n							{/prod_documents}\n						</ul>\n						{/if}\n						{/exp:channel:entries}\n					</div>\n					\n				</div>\n			</div>\n		</div>\n		<div class=\"row\">\n			{embed=\"inc/footer\"}\n		</div>\n	</div>\n	\n	{embed=\"inc/jsinclusion\"}\n</body>\n</html>\n'),(10,24,'exp_templates','template_data',1363927156,1,'{embed=\"inc/head\"}\n<body id=\"index\" class=\"general\">\n	<div class=\"container\">\n		<div class=\"row\">\n			<div class=\"span3\">\n				{embed=\"inc/sidebar\"}\n			</div>\n			<div class=\"span9\">\n				{embed=\"inc/header\"}\n				\n				{embed=\"inc/breadcrumb\"}\n				\n				<div class=\"row\">\n					<div class=\"span6\">\n						{exp:channel:entries}\n						<div class=\"maincol top-product-container\">\n							{prod_contenu_{user_language}}\n						</div>\n						\n						{embed=\"produits/itemlist\" entry_id=\"{entry_id}\"}\n						\n						{/exp:channel:entries}\n					</div>\n					\n					<div class=\"span3\">\n						{exp:channel:entries}\n						<!-- Galerie -->\n						{if \"{prod_galerie:total_files}\" >= 1}\n						<ul class=\"thumbnails\">\n							{prod_galerie}\n							<li>\n								<a href=\"{url}\" class=\"thumbnail galerie-th fancybox\" rel=\"inpage-galerie\" title=\"{title}\">\n									{exp:ce_img:pair:crpd src=\"{url}\" crop=\"yes\" max_width=\"60\" min_width=\"60\" max_height=\"60\" min_height=\"60\"}\n									<img src=\"{crpd:made}\" title=\"{title}\" width=\"{crpd:width}\" height=\"{crpd:height}\" class=\"\" alt=\"{alt_text}\"/>\n									{/exp:ce_img:pair:crpd}\n								</a>\n							</li>\n							{/prod_galerie}\n						</ul>\n						{/if}\n						\n						<!-- Documents -->\n						{if \"{prod_documents:total_rows}\" >= 1}\n						<ul class=\"linked-docs\">\n							<h3>{exp:multi_language:phrase index=\"documents_lies\"}</h3>\n							{prod_documents}\n							<li>\n								<h4>\n									<i class=\"icon-file\"></i> <a href=\"{doc_file}\" class=\"doc-dl\" title=\"{exp:multi_language:phrase index=\"telecharger\"} : {doc_title}\" data-placement=\"left\">{doc_title}</a>\n								</h4>\n								{doc_desc}\n							</li>\n							{/prod_documents}\n						</ul>\n						{/if}\n						{/exp:channel:entries}\n					</div>\n					\n				</div>\n			</div>\n		</div>\n		<div class=\"row\">\n			{embed=\"inc/footer\"}\n		</div>\n	</div>\n	\n	{embed=\"inc/jsinclusion\"}\n</body>\n</html>\n');
/*!40000 ALTER TABLE `exp_revision_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_rte_tools`
--

DROP TABLE IF EXISTS `exp_rte_tools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_rte_tools`
--

LOCK TABLES `exp_rte_tools` WRITE;
/*!40000 ALTER TABLE `exp_rte_tools` DISABLE KEYS */;
INSERT INTO `exp_rte_tools` VALUES (1,'Blockquote','Blockquote_rte','y'),(2,'Bold','Bold_rte','y'),(3,'Headings','Headings_rte','y'),(4,'Image','Image_rte','y'),(5,'Italic','Italic_rte','y'),(6,'Link','Link_rte','y'),(7,'Ordered List','Ordered_list_rte','y'),(8,'Underline','Underline_rte','y'),(9,'Unordered List','Unordered_list_rte','y'),(10,'View Source','View_source_rte','y');
/*!40000 ALTER TABLE `exp_rte_tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_rte_toolsets`
--

DROP TABLE IF EXISTS `exp_rte_toolsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_rte_toolsets`
--

LOCK TABLES `exp_rte_toolsets` WRITE;
/*!40000 ALTER TABLE `exp_rte_toolsets` DISABLE KEYS */;
INSERT INTO `exp_rte_toolsets` VALUES (1,0,'Default','3|2|5|1|9|7|6|4|10','y');
/*!40000 ALTER TABLE `exp_rte_toolsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_search`
--

DROP TABLE IF EXISTS `exp_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_search`
--

LOCK TABLES `exp_search` WRITE;
/*!40000 ALTER TABLE `exp_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_search_log`
--

DROP TABLE IF EXISTS `exp_search_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_search_log`
--

LOCK TABLES `exp_search_log` WRITE;
/*!40000 ALTER TABLE `exp_search_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_search_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_security_hashes`
--

DROP TABLE IF EXISTS `exp_security_hashes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM AUTO_INCREMENT=1986 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_security_hashes`
--

LOCK TABLES `exp_security_hashes` WRITE;
/*!40000 ALTER TABLE `exp_security_hashes` DISABLE KEYS */;
INSERT INTO `exp_security_hashes` VALUES (1985,1363929030,'42de895361b52026200d959ed1c7fc5e8e288fb6','2302c1bdf92bbcbe4619f9f17351b42073e84530'),(1983,1363929024,'42de895361b52026200d959ed1c7fc5e8e288fb6','9036a4cd550e0a6e5ef2e067289e4a30107146ea'),(1982,1363929022,'42de895361b52026200d959ed1c7fc5e8e288fb6','4022fd407e0e505d46d141dae130132834956b07'),(1981,1363929020,'42de895361b52026200d959ed1c7fc5e8e288fb6','1a243fda1e4f28ed3a259dfcf0c12c5f2646938c'),(1978,1363929013,'42de895361b52026200d959ed1c7fc5e8e288fb6','6ee5de0862dd16ae873420ae66d0d5615f613ca8'),(1980,1363929018,'42de895361b52026200d959ed1c7fc5e8e288fb6','523c76ace29ed573eec07b5aebf27af4a9a66e92'),(1979,1363929016,'42de895361b52026200d959ed1c7fc5e8e288fb6','e7a7651351d9c0762fbd650c9afc676389657ce7'),(1977,1363929011,'42de895361b52026200d959ed1c7fc5e8e288fb6','12b14936c97bedb2a7a48f41368ea75630290794'),(1976,1363928960,'42de895361b52026200d959ed1c7fc5e8e288fb6','ac7ca6d4ed9a63ef3461289cb52206148f7fe389'),(1975,1363928960,'42de895361b52026200d959ed1c7fc5e8e288fb6','c977acc8e8ff7fb8397436f6c9d4bc0ddba8a202'),(1974,1363928960,'42de895361b52026200d959ed1c7fc5e8e288fb6','ea6bbfef68987d6ebe89f2255aa113147cf69cb9'),(1973,1363928960,'42de895361b52026200d959ed1c7fc5e8e288fb6','b3e11e03d93144941f3bb2b820c8d991d6d3a490'),(1984,1363929029,'42de895361b52026200d959ed1c7fc5e8e288fb6','78964547033427aaf1d6cacf15442ede19b0c734'),(1971,1363928917,'42de895361b52026200d959ed1c7fc5e8e288fb6','a144863a0efefe5484f5e8cf8f0074b61aff642f'),(1969,1363928861,'42de895361b52026200d959ed1c7fc5e8e288fb6','e7bb31020410a0f39b44f5ca9e3ff871a177eda5'),(1968,1363928861,'42de895361b52026200d959ed1c7fc5e8e288fb6','4a8099cedac0183e671dff3b46646d13bd91f8d3'),(1967,1363928861,'42de895361b52026200d959ed1c7fc5e8e288fb6','7f7f96152d2d2752154cd79fb928fbb00b2839b4'),(1966,1363928860,'42de895361b52026200d959ed1c7fc5e8e288fb6','acaae83a9330dc39c068b9ff86cd4e0e31c3c9fd'),(1970,1363928916,'42de895361b52026200d959ed1c7fc5e8e288fb6','e720096d2638daf32bfe7d6060868f26d060aaed'),(1964,1363928857,'42de895361b52026200d959ed1c7fc5e8e288fb6','779393ee040b1a4c0b401edd600366a24b5d780e'),(1963,1363928853,'42de895361b52026200d959ed1c7fc5e8e288fb6','855c8e9c305a9fbefc84679366d18e38686f62bb'),(1953,1363928077,'42de895361b52026200d959ed1c7fc5e8e288fb6','458b66af06339c1ade9d343ce24a22b034961216'),(1954,1363928077,'42de895361b52026200d959ed1c7fc5e8e288fb6','d9741f98e50859cea4218f7a12af9e6b2c5a2256'),(1955,1363928077,'42de895361b52026200d959ed1c7fc5e8e288fb6','599cd2ab2b4da09b07a277c17614adf00277f093'),(1957,1363928098,'42de895361b52026200d959ed1c7fc5e8e288fb6','1dce3d33452c7eb6d07cca0d80d33b1a7fc1c37a'),(1958,1363928166,'42de895361b52026200d959ed1c7fc5e8e288fb6','23dc9c690df3344bd78f9fb28e44bde6f76990f9'),(1959,1363928174,'42de895361b52026200d959ed1c7fc5e8e288fb6','e0fb039b3f8d98f18c0b14ff65f4790bb57ad800'),(1960,1363928314,'42de895361b52026200d959ed1c7fc5e8e288fb6','f8f1b572647368d0c93d22d5b67a489882866815'),(1961,1363928337,'42de895361b52026200d959ed1c7fc5e8e288fb6','00503602650ac06507e7d66ce107504f6e1292fe'),(1962,1363928338,'42de895361b52026200d959ed1c7fc5e8e288fb6','efc793129b35bae01e9f9793efbe0eb8197dace0'),(1952,1363928076,'42de895361b52026200d959ed1c7fc5e8e288fb6','d37ede40abe0ea9342be4d576bdb9cb837a918da'),(1956,1363928098,'42de895361b52026200d959ed1c7fc5e8e288fb6','ac4e131a1f07295a83a604bba7610574b14f6d36'),(1948,1363927919,'42de895361b52026200d959ed1c7fc5e8e288fb6','b4b109fa899d8b88ac1de139ac151e0f2682ec7a'),(1944,1363927790,'42de895361b52026200d959ed1c7fc5e8e288fb6','dcdc82f1b445df0a3f6b4d8b70afca7f064501d6'),(1947,1363927851,'42de895361b52026200d959ed1c7fc5e8e288fb6','c2697a23ba3b730f5b235df10b622d986b6209d4'),(1946,1363927790,'42de895361b52026200d959ed1c7fc5e8e288fb6','9485debc2e0e32f09f3caf8948202fb5232af512'),(1945,1363927790,'42de895361b52026200d959ed1c7fc5e8e288fb6','37974d7037569163afc0156f599793c11359c7f1'),(1943,1363927790,'42de895361b52026200d959ed1c7fc5e8e288fb6','a38e7f61acdfa8116e1d4769e5abaf9128984ba0'),(1949,1363928062,'42de895361b52026200d959ed1c7fc5e8e288fb6','774bab85cf55e9a2a768de2d29b66d5a8eb59cf9'),(1950,1363928062,'42de895361b52026200d959ed1c7fc5e8e288fb6','cf5fb2ae3935f230b2bfcc81b7ee59b7ba627b4e'),(1939,1363927653,'42de895361b52026200d959ed1c7fc5e8e288fb6','09943ea5d562180614baff902adeb3094f48d105'),(1941,1363927653,'42de895361b52026200d959ed1c7fc5e8e288fb6','6755a0a68383f3e9b4dab7da3c5372736aa9a61e'),(1940,1363927653,'42de895361b52026200d959ed1c7fc5e8e288fb6','0430b0d75f6626e07cca74b661db7d0221f09016'),(1933,1363927606,'42de895361b52026200d959ed1c7fc5e8e288fb6','6444fc8d8fab5a15292c151f6fd10abbdf9bb12d'),(1934,1363927608,'42de895361b52026200d959ed1c7fc5e8e288fb6','d9f37982fa78824573b86fa5771cd6431a61be3c'),(1935,1363927615,'42de895361b52026200d959ed1c7fc5e8e288fb6','a984ce84e91e82bf6cf867002623e964fb98a365'),(1936,1363927618,'42de895361b52026200d959ed1c7fc5e8e288fb6','6c67db773874bff2e28e4416c7d4072a8a2fb221'),(1937,1363927652,'42de895361b52026200d959ed1c7fc5e8e288fb6','d6a364ea0c43c854f6af5de54f719730da729a07'),(1938,1363927652,'42de895361b52026200d959ed1c7fc5e8e288fb6','d22127a54d83c33ea17653730de65967991a5857'),(1931,1363927594,'42de895361b52026200d959ed1c7fc5e8e288fb6','d18e18c72008a0f9edec1afdb0bbc91a5b10eedc'),(1930,1363927594,'42de895361b52026200d959ed1c7fc5e8e288fb6','c7a7f66da817526440f91799cbc56f74149581c7'),(1929,1363927594,'42de895361b52026200d959ed1c7fc5e8e288fb6','9f0ff532721f9b69442d55c787b4f6fe22763f4d'),(1928,1363927594,'42de895361b52026200d959ed1c7fc5e8e288fb6','b987ef0203a973930ea780cebbbf977fb77824f8'),(1932,1363927606,'42de895361b52026200d959ed1c7fc5e8e288fb6','b551da7da4781b36510ba1d130985dd6d5ac98bd'),(1926,1363927589,'42de895361b52026200d959ed1c7fc5e8e288fb6','bc9134d9ffcf8f32e0506b1af7c02326083223b5'),(1924,1363927578,'42de895361b52026200d959ed1c7fc5e8e288fb6','5a4e1da4f077b99470b2f28b6786f470b60799bf'),(1923,1363927578,'42de895361b52026200d959ed1c7fc5e8e288fb6','fc70748e47c2609fed57c318d558244d8af98a46'),(1922,1363927578,'42de895361b52026200d959ed1c7fc5e8e288fb6','bb23330c29f76541e6c17ae8d90749bf2c9a47ba'),(1921,1363927578,'42de895361b52026200d959ed1c7fc5e8e288fb6','6b45fe9a2a40770905929d5884f57ee2f4e9fe94'),(1925,1363927589,'42de895361b52026200d959ed1c7fc5e8e288fb6','0c14654284e977dee5d0a7cb751ee4f32dd4f770'),(1919,1363927572,'42de895361b52026200d959ed1c7fc5e8e288fb6','b6cd420484d667b058f743ab19818b34507d96b8'),(1917,1363927557,'42de895361b52026200d959ed1c7fc5e8e288fb6','1b6d1f92de0c219ffba0cc1a203d9a35f78fe1c9'),(1916,1363927557,'42de895361b52026200d959ed1c7fc5e8e288fb6','7580371b2bfa2703e54389a2238fd342b0e27c61'),(1915,1363927556,'42de895361b52026200d959ed1c7fc5e8e288fb6','f8c9f8cf8688fb59748bed176a919a942dc633d3'),(1905,1363927407,'42de895361b52026200d959ed1c7fc5e8e288fb6','0c1de6b81b900e8a83ca065466a3579168001828'),(1906,1363927407,'42de895361b52026200d959ed1c7fc5e8e288fb6','ac8b6634caa3e3cb8fdbccb54c13c2456f92dd6d'),(1907,1363927547,'42de895361b52026200d959ed1c7fc5e8e288fb6','f16f8e4f80a88db65370dab0dc2abb091a95205a'),(1908,1363927551,'42de895361b52026200d959ed1c7fc5e8e288fb6','9bc5c26ac4fc69050e797e61a4e13acfb73e1456'),(1909,1363927551,'42de895361b52026200d959ed1c7fc5e8e288fb6','5d607a1b9606183fb24749a77dfc41d14940d26f'),(1910,1363927551,'42de895361b52026200d959ed1c7fc5e8e288fb6','af3914814110b48e233a7b62d4b69a71f34bffa7'),(1911,1363927552,'42de895361b52026200d959ed1c7fc5e8e288fb6','6146c97fd402e1c72876a69342fa2b8564c9fbe6'),(1912,1363927552,'42de895361b52026200d959ed1c7fc5e8e288fb6','c0f5bdc7a2d43fab3f1ba5ea1d31d74699ebb1fe'),(1918,1363927572,'42de895361b52026200d959ed1c7fc5e8e288fb6','a6436052ea9806e65985f8770413196541a65e5b'),(1914,1363927556,'42de895361b52026200d959ed1c7fc5e8e288fb6','7be0dfa0efd06d68e94a2fefbf5a4be65566b6f1'),(1900,1363927370,'42de895361b52026200d959ed1c7fc5e8e288fb6','036b1a0f5acb287db95c5cd5e4796d7daf352219'),(1871,1363927085,'42de895361b52026200d959ed1c7fc5e8e288fb6','89c688d5400998d5462aedc164d5af66455f24cf'),(1891,1363927335,'42de895361b52026200d959ed1c7fc5e8e288fb6','403107993f0fec05348506363a43283e88092659'),(1869,1363927079,'42de895361b52026200d959ed1c7fc5e8e288fb6','8dab9f2737a9513499f42b8f8c082400a10ad9b9'),(1895,1363927358,'42de895361b52026200d959ed1c7fc5e8e288fb6','306e62351c6d8b3a8fba7352a39f06559e4c4bd3'),(1889,1363927314,'42de895361b52026200d959ed1c7fc5e8e288fb6','71e470b9950a783c22ccb38df91a096527f69b80'),(1887,1363927303,'42de895361b52026200d959ed1c7fc5e8e288fb6','2a418a612a77ed37eb0b3637379842874b01dd98'),(1886,1363927303,'42de895361b52026200d959ed1c7fc5e8e288fb6','cf5790c7e89a4524da04e30751d1b73578879fdf'),(1885,1363927303,'42de895361b52026200d959ed1c7fc5e8e288fb6','80008247d8ee227a59f0b8b21e54c16c0585b5a3'),(1884,1363927302,'42de895361b52026200d959ed1c7fc5e8e288fb6','f902d44e1331d2f545b592f9b2740268ee26ad8d'),(1882,1363927299,'42de895361b52026200d959ed1c7fc5e8e288fb6','83dd0493a688da07e830098c274a1cb8f183ed17'),(1888,1363927313,'42de895361b52026200d959ed1c7fc5e8e288fb6','1b08dc2a7b5124394302c870a146c1e327d0da78'),(1881,1363927156,'42de895361b52026200d959ed1c7fc5e8e288fb6','d2206b97b00dfe55c27be55ca77dee5a6de5cd67'),(1876,1363927108,'42de895361b52026200d959ed1c7fc5e8e288fb6','bf60634bfd5fe6017d29a4cf761488e0b316716b'),(1875,1363927108,'42de895361b52026200d959ed1c7fc5e8e288fb6','788f0d4ba8cd5bd0d35e8846b7db6b879a38bc3f'),(1880,1363927156,'42de895361b52026200d959ed1c7fc5e8e288fb6','b4a32c2cb4f2e2c574b9001934e3dc4510d6defb'),(1857,1363926044,'42de895361b52026200d959ed1c7fc5e8e288fb6','991d4d0b1be2085a63f1917c104fa41e0c28144a'),(1858,1363926052,'42de895361b52026200d959ed1c7fc5e8e288fb6','3f74944dc05a174b7afdae9c5e6c352c27c48a8f'),(1859,1363926054,'42de895361b52026200d959ed1c7fc5e8e288fb6','b2977c7f530443bae07ba98a0fe6db656c81a032'),(1860,1363926054,'42de895361b52026200d959ed1c7fc5e8e288fb6','0f54ebc9f90a1a2c99e61041c2fd75124c6f81a4'),(1868,1363927000,'42de895361b52026200d959ed1c7fc5e8e288fb6','80da05220ad837d822416a4cb4622c1b3586d147'),(1862,1363926075,'42de895361b52026200d959ed1c7fc5e8e288fb6','70c6c041cec28339b174694f3125d31e55728e28'),(1863,1363926075,'42de895361b52026200d959ed1c7fc5e8e288fb6','3cad0726ca33849869f32bdc57ce734e75582610'),(1864,1363926075,'42de895361b52026200d959ed1c7fc5e8e288fb6','d78b5c09cf447694bac800354d736ad9bd6db8d1'),(1865,1363926075,'42de895361b52026200d959ed1c7fc5e8e288fb6','3703abfd3c3e82681d015d9c0a8daaee62af8783'),(1866,1363926140,'42de895361b52026200d959ed1c7fc5e8e288fb6','d979a4cdc34f461ab9b2ddffd312c1b71c3ac5b9'),(1872,1363927085,'42de895361b52026200d959ed1c7fc5e8e288fb6','1abf856f797e110f473d350a69cf99c2b09d945f'),(1873,1363927085,'42de895361b52026200d959ed1c7fc5e8e288fb6','c581e56d5eb9c48620b07efe19107b315ac03b24'),(1874,1363927085,'42de895361b52026200d959ed1c7fc5e8e288fb6','b8aab270bcf5485646115b15889bac1edb4d14fb'),(1892,1363927335,'42de895361b52026200d959ed1c7fc5e8e288fb6','9e8768bd9f35b2cbdbb6c0c4d678f386575440a1'),(1893,1363927335,'42de895361b52026200d959ed1c7fc5e8e288fb6','f2f8fb7958767eee3c82aa4a6de89c77a13b7b84'),(1894,1363927335,'42de895361b52026200d959ed1c7fc5e8e288fb6','7704525e1a42b531347ab2250757f203e9307286'),(1896,1363927358,'42de895361b52026200d959ed1c7fc5e8e288fb6','89c5d39aab2576215260811c2be76b7fc4d12c9e'),(1902,1363927389,'42de895361b52026200d959ed1c7fc5e8e288fb6','c66ec196fd81ddf6e4bd2e23f50871cde58ce472'),(1898,1363927369,'42de895361b52026200d959ed1c7fc5e8e288fb6','6ac7c4dd7a18adfe7f08e051f848085b79c4767b'),(1899,1363927370,'42de895361b52026200d959ed1c7fc5e8e288fb6','4714e340fb4b487c62317db9c71dd431ca4480bf'),(1903,1363927389,'42de895361b52026200d959ed1c7fc5e8e288fb6','891cc8cca55320e3da7b0d96da1d1ee98cbb1307'),(1901,1363927370,'42de895361b52026200d959ed1c7fc5e8e288fb6','51a595ec05325595c28e333b1d1c189305849ca8'),(1867,1363927000,'42de895361b52026200d959ed1c7fc5e8e288fb6','441c094e0645a723024e47ec283f8619d861a858');
/*!40000 ALTER TABLE `exp_security_hashes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_sessions`
--

DROP TABLE IF EXISTS `exp_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_sessions`
--

LOCK TABLES `exp_sessions` WRITE;
/*!40000 ALTER TABLE `exp_sessions` DISABLE KEYS */;
INSERT INTO `exp_sessions` VALUES ('42de895361b52026200d959ed1c7fc5e8e288fb6',1,1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22',1363929039);
/*!40000 ALTER TABLE `exp_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_sites`
--

DROP TABLE IF EXISTS `exp_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_sites`
--

LOCK TABLES `exp_sites` WRITE;
/*!40000 ALTER TABLE `exp_sites` DISABLE KEYS */;
INSERT INTO `exp_sites` VALUES (1,'Artelux','default_site',NULL,'YTo5Mzp7czoxMDoic2l0ZV9pbmRleCI7czo5OiJpbmRleC5waHAiO3M6ODoic2l0ZV91cmwiO3M6MjQ6Imh0dHA6Ly9sb2NhbC5hcnRlbHV4LmJlLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czozMToiaHR0cDovL2xvY2FsLmFydGVsdXguYmUvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjIwOiJzZWJhc3RpZW5Ac2ViaXNtZS5iZSI7czoxNDoid2VibWFzdGVyX25hbWUiO3M6MDoiIjtzOjIwOiJjaGFubmVsX25vbWVuY2xhdHVyZSI7czo3OiJjaGFubmVsIjtzOjEwOiJtYXhfY2FjaGVzIjtzOjM6IjE1MCI7czoxMToiY2FwdGNoYV91cmwiO3M6NDA6Imh0dHA6Ly9sb2NhbC5hcnRlbHV4LmJlL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfcGF0aCI7czo1NjoiL1VzZXJzL3NlYmEvRG9jdW1lbnRzL1dvcmtzcGFjZS9hcnRlbHV4L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo2OiJmcmVuY2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjM6IlVQMSI7czoxMzoic2VydmVyX29mZnNldCI7czowOiIiO3M6MTY6ImRheWxpZ2h0X3NhdmluZ3MiO3M6MToibiI7czoyMToiZGVmYXVsdF9zaXRlX3RpbWV6b25lIjtzOjM6IlVQMSI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJuIjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozOToiaHR0cDovL2xvY2FsLmFydGVsdXguYmUvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ3OiIvVXNlcnMvc2ViYS9Eb2N1bWVudHMvV29ya3NwYWNlL2FydGVsdXgvdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO3M6NjoiY3BfdXJsIjtzOjM2OiJodHRwOi8vbG9jYWwuYXJ0ZWx1eC5iZS9jcC9pbmRleC5waHAiO30=','YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==','YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6Mzk6Imh0dHA6Ly9sb2NhbC5hcnRlbHV4LmJlL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTU6Ii9Vc2Vycy9zZWJhL0RvY3VtZW50cy9Xb3Jrc3BhY2UvYXJ0ZWx1eC9pbWFnZXMvYXZhdGFycy8iO3M6MTY6ImF2YXRhcl9tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE3OiJhdmF0YXJfbWF4X2hlaWdodCI7czozOiIxMDAiO3M6MTM6ImF2YXRhcl9tYXhfa2IiO3M6MjoiNTAiO3M6MTM6ImVuYWJsZV9waG90b3MiO3M6MToibiI7czo5OiJwaG90b191cmwiO3M6NDU6Imh0dHA6Ly9sb2NhbC5hcnRlbHV4LmJlL2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxMDoicGhvdG9fcGF0aCI7czo2MToiL1VzZXJzL3NlYmEvRG9jdW1lbnRzL1dvcmtzcGFjZS9hcnRlbHV4L2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxNToicGhvdG9fbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNjoicGhvdG9fbWF4X2hlaWdodCI7czozOiIxMDAiO3M6MTI6InBob3RvX21heF9rYiI7czoyOiI1MCI7czoxNjoiYWxsb3dfc2lnbmF0dXJlcyI7czoxOiJ5IjtzOjEzOiJzaWdfbWF4bGVuZ3RoIjtzOjM6IjUwMCI7czoyMToic2lnX2FsbG93X2ltZ19ob3RsaW5rIjtzOjE6Im4iO3M6MjA6InNpZ19hbGxvd19pbWdfdXBsb2FkIjtzOjE6Im4iO3M6MTE6InNpZ19pbWdfdXJsIjtzOjUzOiJodHRwOi8vbG9jYWwuYXJ0ZWx1eC5iZS9pbWFnZXMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjY5OiIvVXNlcnMvc2ViYS9Eb2N1bWVudHMvV29ya3NwYWNlL2FydGVsdXgvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MjoiL1VzZXJzL3NlYmEvRG9jdW1lbnRzL1dvcmtzcGFjZS9hcnRlbHV4L2ltYWdlcy9wbV9hdHRhY2htZW50cy8iO3M6MjM6InBydl9tc2dfbWF4X2F0dGFjaG1lbnRzIjtzOjE6IjMiO3M6MjI6InBydl9tc2dfYXR0YWNoX21heHNpemUiO3M6MzoiMjUwIjtzOjIwOiJwcnZfbXNnX2F0dGFjaF90b3RhbCI7czozOiIxMDAiO3M6MTk6InBydl9tc2dfaHRtbF9mb3JtYXQiO3M6NDoic2FmZSI7czoxODoicHJ2X21zZ19hdXRvX2xpbmtzIjtzOjE6InkiO3M6MTc6InBydl9tc2dfbWF4X2NoYXJzIjtzOjQ6IjYwMDAiO3M6MTk6Im1lbWJlcmxpc3Rfb3JkZXJfYnkiO3M6MTE6InRvdGFsX3Bvc3RzIjtzOjIxOiJtZW1iZXJsaXN0X3NvcnRfb3JkZXIiO3M6NDoiZGVzYyI7czoyMDoibWVtYmVybGlzdF9yb3dfbGltaXQiO3M6MjoiMjAiO30=','YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo2NToiL1VzZXJzL3NlYmEvRG9jdW1lbnRzL1dvcmtzcGFjZS9hcnRlbHV4L3RoZW1lcy9hcnRlbHV4L3RlbXBsYXRlcy8iO30=','YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=','YToyOntzOjU0OiIvVXNlcnMvc2ViYXN0aWVuL0RvY3VtZW50cy9Xb3Jrc3BhY2UvYXJ0ZWx1eC9pbmRleC5waHAiO3M6MzI6IjM2NmVkYTQ0OWY0YjkyMzk0NjM1NGM4OTA2YjNlMjNlIjtzOjc6ImVtYWlsZWQiO2E6MDp7fX0=','YToxOntpOjE7YTozOntzOjM6InVybCI7czoyNDoiaHR0cDovL2xvY2FsLmFydGVsdXguYmUvIjtzOjQ6InVyaXMiO2E6MzA6e2k6MztzOjk6Ii9hY2N1ZWlsLyI7aTo0O3M6MTA6Ii9wcm9kdWl0cy8iO2k6OTtzOjQwOiIvcHJvZHVpdHMvam91cm5hdXgtbHVtaW5ldXgtZXQtZGlzcGxheXMvIjtpOjEwO3M6NzA6Ii9wcm9kdWl0cy9qb3VybmF1eC1sdW1pbmV1eC1ldC1kaXNwbGF5cy9qb3VybmFsLWx1bWluZXV4LXByby0wNS4yMC4wMy8iO2k6MTI7czo4NDoiL3Byb2R1aXRzL2pvdXJuYXV4LWx1bWluZXV4LWV0LWRpc3BsYXlzL2pvdXJuYWwtbHVtaW5ldXgtcHJvLTA1LjIwLjAzLXZlcnNpb24tdG90ZW0vIjtpOjExO3M6Mjk6Ii9wcm9kdWl0cy9tYXJxdW9pcnMtZGUtc2NvcmUvIjtpOjE3O3M6NjQ6Ii9wcm9kdWl0cy9tYXJxdW9pcnMtZGUtc2NvcmUvYWZmaWNoZXVyLWRlLXNjb3JlLXBvdXItYmFza2V0YmFsbC8iO2k6MTg7czoxMDI6Ii9wcm9kdWl0cy9tYXJxdW9pcnMtZGUtc2NvcmUvYWZmaWNoZXVyLWRlLXNjb3JlLWZvb3RiYWxsLWF2ZWMtMi1wYW5uZWF1eC1yb3RhdGlmcy1ldC1qb3VybmFsLWx1bWluZXV4LyI7aToxOTtzOjY5OiIvcHJvZHVpdHMvbWFycXVvaXJzLWRlLXNjb3JlL21hcnF1b2lyLWRlLXNjb3JlLXBvdXItaG9ja2V5LTMxdnMxLjIuMS8iO2k6MjA7czo2MjoiL3Byb2R1aXRzL21hcnF1b2lycy1kZS1zY29yZS9wYW5uZWF1LWRlLXNjb3JlLXBvdXItYXRobGV0aXNtZS8iO2k6MTM7czo0NDoiL3Byb2R1aXRzL2FmZmlpY2hldXItZGUtc2VjdXJpdGUtaW5kdXN0cmllbC8iO2k6MjE7czo3NzoiL3Byb2R1aXRzL2FmZmlpY2hldXItZGUtc2VjdXJpdGUtaW5kdXN0cmllbC9hZmZpY2hhZ2UtZGUtc2VjdXJpdGUtaW5kdXN0cmllbC8iO2k6MTQ7czo0NDoiL3Byb2R1aXRzL2VjcmFucy1nZWFudHMtYS1kaW9kZXMtZnVsbC1jb2xvci8iO2k6MTU7czo0NzoiL3Byb2R1aXRzL3NpZ25hbGlzYXRpb24tcm91dGllcmUtZXQtZGUtcGFya2luZy8iO2k6MTY7czozNjoiL3Byb2R1aXRzL2FmZmljaGFnZS1wcml4LWNhcmJ1cmFudHMvIjtpOjIyO3M6Mjk6Ii9wcm9kdWl0cy9jcm9peC1kZS1waGFybWFjaWUvIjtpOjIzO3M6NTA6Ii9wcm9kdWl0cy9lY3JhbnMtZ3JhcGhpcXVlcy1pbmZvLXZpbGxlLWZ1bGwtY29sb3IvIjtpOjI0O3M6Mjg6Ii9wcm9kdWl0cy9yYWRhcnMtcHJldmVudGlmcy8iO2k6MjY7czo2MDoiL3Byb2R1aXRzL2FmZmljaGV1cnMtZWxlY3Ryb25pcXVlcy1kaW5mb3JtYXRpb25zLWNvbW11bmFsZXMvIjtpOjU7czoxMDoiL3NlcnZpY2VzLyI7aToyODtzOjIwOiIvc2VydmljZXMvcGxhY2VtZW50LyI7aToyOTtzOjIwOiIvc2VydmljZXMvZW50cmV0aWVuLyI7aTozMDtzOjE5OiIvc2VydmljZXMvZ2FyYW50aWUvIjtpOjY7czoxMjoiL2FjdHVhbGl0ZXMvIjtpOjc7czo5OiIvY29udGFjdC8iO2k6ODtzOjc6Ii9saWVucy8iO2k6Mjc7czo5OiIvc2l0ZW1hcC8iO2k6MzE7czoxMjoiL2Rpc2NsYWltZXIvIjtpOjMyO3M6NTE6Ii9hY3R1YWxpdGVzL2NpdHktaW5mb3JtYXRpb24tYW5kLXByZXZlbnRpb24tc2NyZWVuLyI7aTozMztzOjU0OiIvYWN0dWFsaXRlcy96b25lLTMwLXBhbm5lYXV4LWR5bmFtaXF1ZXMtcHJvZ3JhbW1hYmxlcy8iO31zOjk6InRlbXBsYXRlcyI7YTozMDp7aTozO3M6MjoiMTUiO2k6NDtzOjI6IjE2IjtpOjU7czoyOiIxNiI7aTo2O3M6MjoiMTkiO2k6NztzOjI6IjIxIjtpOjg7czoyOiIxNSI7aTo5O3M6MjoiMTciO2k6MTA7czoyOiIyMiI7aToxMTtzOjI6IjE3IjtpOjEyO3M6MjoiMjIiO2k6MTM7czoyOiIxNyI7aToxNDtzOjI6IjE3IjtpOjE1O3M6MjoiMTciO2k6MTY7czoyOiIxNyI7aToxNztzOjI6IjIyIjtpOjE4O3M6MjoiMjIiO2k6MTk7czoyOiIyMiI7aToyMDtzOjI6IjIyIjtpOjIxO3M6MjoiMjIiO2k6MjI7czoyOiIxNyI7aToyMztzOjI6IjE3IjtpOjI0O3M6MjoiMTciO2k6MjY7czoyOiIxNyI7aToyNztzOjI6IjI0IjtpOjI4O3M6MjoiMTgiO2k6Mjk7czoyOiIxOCI7aTozMDtzOjI6IjE4IjtpOjMxO3M6MjoiMTUiO2k6MzI7czoyOiIyMCI7aTozMztzOjI6IjIwIjt9fX0=');
/*!40000 ALTER TABLE `exp_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_snippets`
--

DROP TABLE IF EXISTS `exp_snippets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_snippets`
--

LOCK TABLES `exp_snippets` WRITE;
/*!40000 ALTER TABLE `exp_snippets` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_snippets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_specialty_templates`
--

DROP TABLE IF EXISTS `exp_specialty_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_specialty_templates`
--

LOCK TABLES `exp_specialty_templates` WRITE;
/*!40000 ALTER TABLE `exp_specialty_templates` DISABLE KEYS */;
INSERT INTO `exp_specialty_templates` VALUES (1,1,'y','offline_template','','<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),(2,1,'y','message_template','','<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),(3,1,'y','admin_notify_reg','Notification of new member registration','New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),(4,1,'y','admin_notify_entry','A new channel entry has been posted','A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),(5,1,'y','admin_notify_mailinglist','Someone has subscribed to your mailing list','A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),(6,1,'y','admin_notify_comment','You have just received a comment','You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),(7,1,'y','mbr_activation_instructions','Enclosed is your activation code','Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),(8,1,'y','forgot_password_instructions','Login information','{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),(9,1,'y','reset_password_notification','New Login Information','{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),(10,1,'y','validated_member_notify','Your membership account has been activated','{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),(11,1,'y','decline_member_validation','Your membership account has been declined','{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),(12,1,'y','mailinglist_activation_instructions','Email Confirmation','Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),(13,1,'y','comment_notification','Someone just responded to your comment','{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),(14,1,'y','comments_opened_notification','New comments have been added','Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),(15,1,'y','private_message_notification','Someone has sent you a Private Message','\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),(16,1,'y','pm_inbox_full','Your private message mailbox is full','{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
/*!40000 ALTER TABLE `exp_specialty_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_stats`
--

DROP TABLE IF EXISTS `exp_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_stats`
--

LOCK TABLES `exp_stats` WRITE;
/*!40000 ALTER TABLE `exp_stats` DISABLE KEYS */;
INSERT INTO `exp_stats` VALUES (1,1,3,3,'artelux',33,0,0,0,1363928968,0,0,1359572797,4,1359572128,1364494301);
/*!40000 ALTER TABLE `exp_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_status_groups`
--

DROP TABLE IF EXISTS `exp_status_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_status_groups`
--

LOCK TABLES `exp_status_groups` WRITE;
/*!40000 ALTER TABLE `exp_status_groups` DISABLE KEYS */;
INSERT INTO `exp_status_groups` VALUES (1,1,'Statuses');
/*!40000 ALTER TABLE `exp_status_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_status_no_access`
--

DROP TABLE IF EXISTS `exp_status_no_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_status_no_access`
--

LOCK TABLES `exp_status_no_access` WRITE;
/*!40000 ALTER TABLE `exp_status_no_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_status_no_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_statuses`
--

DROP TABLE IF EXISTS `exp_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_statuses`
--

LOCK TABLES `exp_statuses` WRITE;
/*!40000 ALTER TABLE `exp_statuses` DISABLE KEYS */;
INSERT INTO `exp_statuses` VALUES (1,1,1,'open',1,'009933'),(2,1,1,'closed',2,'990000');
/*!40000 ALTER TABLE `exp_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_structure`
--

DROP TABLE IF EXISTS `exp_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_structure`
--

LOCK TABLES `exp_structure` WRITE;
/*!40000 ALTER TABLE `exp_structure` DISABLE KEYS */;
INSERT INTO `exp_structure` VALUES (0,0,0,0,0,1,58,'root','n'),(1,3,0,1,0,2,3,'','y'),(1,4,0,1,0,4,39,'','n'),(1,5,0,1,0,40,47,'','n'),(1,6,0,1,4,48,49,'','n'),(1,7,0,1,0,50,51,'','n'),(1,8,0,1,0,52,53,'','n'),(1,9,4,2,0,5,10,'','n'),(1,10,9,5,0,6,7,'','n'),(1,11,4,2,0,11,20,'','n'),(1,12,9,5,0,8,9,'','n'),(1,13,4,2,0,21,24,'','n'),(1,14,4,2,0,25,26,'','n'),(1,15,4,2,0,27,28,'','n'),(1,16,4,2,0,29,30,'','n'),(1,17,11,5,0,12,13,'','n'),(1,18,11,5,0,14,15,'','n'),(1,19,11,5,0,16,17,'','n'),(1,20,11,5,0,18,19,'','n'),(1,21,13,5,0,22,23,'','n'),(1,22,4,2,0,31,32,'','n'),(1,23,4,2,0,33,34,'','n'),(1,24,4,2,0,35,36,'','n'),(1,26,4,2,0,37,38,'','n'),(1,27,0,1,0,54,55,'','y'),(1,28,5,3,0,41,42,'','n'),(1,29,5,3,0,43,44,'','n'),(1,30,5,3,0,45,46,'','n'),(1,31,0,1,0,56,57,'','y');
/*!40000 ALTER TABLE `exp_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_structure_channels`
--

DROP TABLE IF EXISTS `exp_structure_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_structure_channels`
--

LOCK TABLES `exp_structure_channels` WRITE;
/*!40000 ALTER TABLE `exp_structure_channels` DISABLE KEYS */;
INSERT INTO `exp_structure_channels` VALUES (1,1,15,'page','n','y'),(1,2,17,'page','n','y'),(1,3,18,'page','n','y'),(1,4,20,'listing','n','y'),(1,5,22,'page','n','y'),(1,6,0,'asset','y','y');
/*!40000 ALTER TABLE `exp_structure_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_structure_listings`
--

DROP TABLE IF EXISTS `exp_structure_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_structure_listings`
--

LOCK TABLES `exp_structure_listings` WRITE;
/*!40000 ALTER TABLE `exp_structure_listings` DISABLE KEYS */;
INSERT INTO `exp_structure_listings` VALUES (1,32,6,4,20,'city-information-and-prevention-screen'),(1,33,6,4,20,'zone-30-panneaux-dynamiques-programmables');
/*!40000 ALTER TABLE `exp_structure_listings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_structure_members`
--

DROP TABLE IF EXISTS `exp_structure_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_structure_members`
--

LOCK TABLES `exp_structure_members` WRITE;
/*!40000 ALTER TABLE `exp_structure_members` DISABLE KEYS */;
INSERT INTO `exp_structure_members` VALUES (1,1,'[\"9\",\"11\",\"13\"]');
/*!40000 ALTER TABLE `exp_structure_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_structure_settings`
--

DROP TABLE IF EXISTS `exp_structure_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_structure_settings`
--

LOCK TABLES `exp_structure_settings` WRITE;
/*!40000 ALTER TABLE `exp_structure_settings` DISABLE KEYS */;
INSERT INTO `exp_structure_settings` VALUES (1,0,'action_ajax_move','44'),(2,0,'module_id','17'),(14,1,'perm_admin_structure_6','y'),(13,1,'add_trailing_slash','y'),(12,1,'hide_hidden_templates','y'),(11,1,'redirect_on_publish','n'),(10,1,'redirect_on_login','n'),(15,1,'perm_admin_channels_6','y'),(16,1,'perm_view_global_add_page_6','y'),(17,1,'perm_view_add_page_6','y'),(18,1,'perm_view_view_page_6','y'),(19,1,'perm_delete_6','all'),(20,1,'perm_reorder_6','all');
/*!40000 ALTER TABLE `exp_structure_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_template_groups`
--

DROP TABLE IF EXISTS `exp_template_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_template_groups`
--

LOCK TABLES `exp_template_groups` WRITE;
/*!40000 ALTER TABLE `exp_template_groups` DISABLE KEYS */;
INSERT INTO `exp_template_groups` VALUES (1,1,'home',1,'y'),(2,1,'inc',2,'n'),(3,1,'nsm_better_meta',3,'n'),(4,1,'pages',4,'n'),(5,1,'produits',5,'n'),(6,1,'services',6,'n'),(7,1,'actualites',7,'n');
/*!40000 ALTER TABLE `exp_template_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_template_member_groups`
--

DROP TABLE IF EXISTS `exp_template_member_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_template_member_groups`
--

LOCK TABLES `exp_template_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_template_member_groups` DISABLE KEYS */;
INSERT INTO `exp_template_member_groups` VALUES (6,1),(6,2),(6,3),(6,4),(6,5),(6,6),(6,7);
/*!40000 ALTER TABLE `exp_template_member_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_template_no_access`
--

DROP TABLE IF EXISTS `exp_template_no_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_template_no_access`
--

LOCK TABLES `exp_template_no_access` WRITE;
/*!40000 ALTER TABLE `exp_template_no_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_template_no_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_templates`
--

DROP TABLE IF EXISTS `exp_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_templates`
--

LOCK TABLES `exp_templates` WRITE;
/*!40000 ALTER TABLE `exp_templates` DISABLE KEYS */;
INSERT INTO `exp_templates` VALUES (1,1,1,'index','y','webpage','{embed=\"inc/head\"}\n<body id=\"index\" class=\"general\">\n	<div class=\"container\">\n		<div class=\"row\">\n			<div class=\"span3\">\n				{embed=\"inc/sidebar\"}\n			</div>\n			<div class=\"span9\">\n				{embed=\"inc/header\"}\n				<div class=\"page-header\">\n					{exp:channel:entries channel=\"pages\" limit=\"1\" entry_id=\"3\"}\n					{page_contenu_{user_language}}\n					{/exp:channel:entries}\n				</div>\n				\n				\n				<ul class=\"thumbnails\">\n					<li class=\"span3\">\n						<div class=\"thumbnail\">\n							<img src=\"http://placehold.it/260x200\">\n							<div class=\"caption\">\n								<h3>Thumbnail label</h3>\n								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n								<p><a href=\"#\" class=\"btn btn-primary\">Action</a> <a href=\"#\" class=\"btn\">Action</a></p>\n							</div>\n						</div>\n					</li>\n					<li class=\"span3\">\n						<div class=\"thumbnail\">\n							<img src=\"http://placehold.it/260x200\">\n							<div class=\"caption\">\n								<h3>Thumbnail label</h3>\n								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n								<p><a href=\"#\" class=\"btn btn-primary\">Action</a> <a href=\"#\" class=\"btn\">Action</a></p>\n							</div>\n						</div>\n					</li>\n					<li class=\"span3\">\n						<div class=\"thumbnail\">\n							<img src=\"http://placehold.it/260x200\">\n							<div class=\"caption\">\n								<h3>Thumbnail label</h3>\n								<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\n								<p><a href=\"#\" class=\"btn btn-primary\">Action</a> <a href=\"#\" class=\"btn\">Action</a></p>\n							</div>\n						</div>\n					</li>\n				</ul>\n				\n				\n			</div>\n		</div>\n		<div class=\"row\">\n			{embed=\"inc/footer\"}\n		</div>\n	</div>\n	\n	{embed=\"inc/jsinclusion\"}\n</body>\n</html>\n','',1359693468,1,'n',0,'','n','n','o',144),(2,1,2,'index','n','webpage','',NULL,1359506019,0,'n',0,'','n','n','o',0),(3,1,2,'head','y','webpage','<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->\n<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->\n<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->\n<head>\n	<meta charset=\"utf-8\">\n	<title>Artelux</title>\n	<meta name=\"description\" content=\"\">\n	<meta name=\"viewport\" content=\"width=device-width\">\n	\n	<link rel=\"stylesheet\" href=\"{theme_folder_url}artelux/css/bootstrap-responsive.css\">\n	<link rel=\"stylesheet\" href=\"{theme_folder_url}artelux/css/screen.css\">\n	\n	<script src=\"{theme_folder_url}deux-ourthes/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js\"></script>\n</head>\n','',1359693468,1,'n',0,'','n','n','o',0),(4,1,3,'.footer','y','webpage','	</body>\n</html>',NULL,1359508367,1,'n',0,'','n','n','o',0),(5,1,3,'.header','y','webpage','<!DOCTYPE html>\n<html>\n	<head>\n		{exp:nsm_better_meta:template}\n		<style type=\"text/css\" media=\"screen\">\n			html {font: normal normal normal 12px/18px Arial, sans-serif;}\n		</style>\n	</head>\n	<body>\n',NULL,1359508367,1,'n',0,'','n','n','o',0),(6,1,3,'category-index','y','webpage','{preload_replace:this_index_channel=\"news\"}\n\n<p>Template: nsm_better_meta/category-index</p>\n\n\n{exp:channel:category_heading channel=\"{this_index_channel}\"}\n	{if no_results}{redirect=\"404\"}{/if}\n	{embed=\"nsm_better_meta/.header\"\n		title=\"{category_name}\"\n		title_suffix=\"nsm_better_meta/category-archive\"\n		description=\"{category_description}\"\n	}\n	<h1>{category_name} <small> &mdash; Category archive</small></h1>\n{/exp:channel:category_heading}\n\n<ol>\n	{exp:channel:entries channel=\"{this_index_channel}\"}\n	{if no_results}<li>No entries have been assigned to this category</li>{/if}\n	<li><a href=\"{if page_uri}{page_uri}{if:else}{entry_id_path=\'nsm_better_meta/entry\'}{/if}\">{title}</a></li>\n	{/exp:channel:entries}\n</ol>\n\n{embed=\"nsm_better_meta/.footer\"}',NULL,1359508367,1,'n',0,'','n','n','o',0),(7,1,3,'entry','y','webpage','{preload_replace:this_page_channel=\"news\"}\n\n<p>Template: nsm_better_meta/entry</p>\n\n{exp:channel:entries\n	_cache=\"yes\"\n	disable=\"category_fields|pagination|member_data\"\n	limit=\"1\"\n	refresh=\"10\"\n	status=\"not closed\"\n	track_views=\"one\"\n	channel=\"{this_page_channel}\"\n}\n	{embed=\"nsm_better_meta/.header\"\n		entry_id=\"{entry_id}\"\n		title_suffix=\"{channel_title}\"\n	}\n		<h1>{title}</h1>\n		<p><a href=\"{path=nsm_better_meta/index}\">Back to entry index</a></p>\n	{embed=\"nsm_better_meta/.footer\"}\n{/exp:channel:entries}',NULL,1359508367,1,'n',0,'','n','n','o',0),(8,1,3,'index','y','webpage','{preload_replace:this_index_channel=\"news\"}\n\n<p>Template: nsm_better_meta/index</p>\n\n{exp:channel:info channel=\"{this_index_channel}\"}\n\n	{embed=\"nsm_better_meta/.header\"\n		title=\"{channel_title}\"\n		title_suffix=\"nsm_better_meta/index\"\n		description=\"{channel_description}\"\n	}\n	<h1>{channel_title} <small> &mdash; Index</small></h1>\n\n{/exp:channel:info}\n\n<h2>Published entries in {this_index_channel}</h2>\n<ol>\n	{exp:channel:entries\n		_cache=\"yes\"\n		disable=\"categories|category_fields|custom_fields|pagination|member_data\"\n		limit=\"10\"\n		refresh=\"10\"\n		status=\"not closed\"\n		channel=\"{this_index_channel}\"\n	}\n		{if no_results}<li>No entries published in this channel</li>{/if}\n		<li><a href=\"{if page_uri}{page_uri}{if:else}{entry_id_path=\'nsm_better_meta/entry\'}{/if}\">{title}</a></li>\n	{/exp:channel:entries}\n</ol>\n\n<h2>Categories</h2>\n{exp:channel:categories channel=\"{this_index_channel}\" style=\"nested\"}\n	<a href=\"{path=\'nsm_better_meta/category-index\'}\">{category_name}</a>\n{/exp:channel:categories}\n\n{embed=\"nsm_better_meta/.footer\"}',NULL,1359508367,1,'n',0,'','n','n','o',0),(9,1,2,'header','y','webpage','<div class=\"site-header\">\n	<h2>Solutions sur mesure pour journaux lumineux, displays et marquoirs de score</h2>\n</div>\n\n{embed=\"inc/navigation\"}\n\n{embed=\"inc/breadcrumb\"}','',1359693468,1,'n',0,'','n','n','o',0),(10,1,2,'navigation','y','webpage','<div class=\"navbar navbar-blued\">\n	<div class=\"navbar-inner\">\n		<div class=\"container\">\n			\n			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->\n			<a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">\n				<span class=\"icon-bar\"></span>\n				<span class=\"icon-bar\"></span>\n				<span class=\"icon-bar\"></span>\n			</a>\n			\n			<!-- Everything you want hidden at 940px or less, place within here -->\n			<div class=\"nav-collapse collapse\">\n				<!-- .nav, .navbar-search, .navbar-form, etc -->\n				<ul class=\"nav\">\n					<li class=\"active\">\n						<a href=\"#\">Accueil</a>\n					</li>\n					<li class=\"dropdown\">\n						<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Produits <b class=\"caret\"></b></a>\n						<ul class=\"dropdown-menu\">\n							<li class=\"nav-header\">\n								Notre gamme de produits\n							</li>\n							<li>\n								<a href=\"#\">Journaux lumineux et displays</a>\n							</li>\n							<li>\n								<a href=\"#\">Les marquoirs de score</a>\n							</li>\n							<li>\n								<a href=\"#\">Affiicheur de sécurité industriel</a>\n							</li>\n							<li>\n								<a href=\"#\">Ecrans géants à diodes full color</a>\n							</li>\n							<li>\n								<a href=\"#\">La signalisation routière et de parking</a>\n							</li>\n						</ul>\n					</li>\n					<li class=\"dropdown\">\n						<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Services <b class=\"caret\"></b></a>\n						<ul class=\"dropdown-menu\">\n							<li class=\"nav-header\">\n								Services et prestations\n							</li>\n							<li>\n								<a href=\"#\">Placement</a>\n							</li>\n							<li>\n								<a href=\"#\">Entretien</a>\n							</li>\n							<li>\n								<a href=\"#\">Garantie</a>\n							</li>\n						</ul>\n					</li>\n					<li>\n						<a href=\"#\">Actualité</a>\n					</li>\n					<li>\n						<a href=\"#\">Contact</a>\n					</li>\n					<li>\n						<a href=\"#\">Liens</a>\n					</li>\n				</ul>\n				\n				<ul class=\"nav pull-right\">\n					<li class=\"divider-vertical\"></li>\n					<li class=\"dropdown\">\n						<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Langue <b class=\"caret\"></b></a>\n						<ul class=\"dropdown-menu\">\n							<li>\n								<a href=\"#\">Français <i class=\"icon-ok\"></i></a>\n							</li>\n							<li>\n								<a href=\"#\">Neederlands</a>\n							</li>\n							<li>\n								<a href=\"#\">English</a>\n							</li>\n							<li>\n								<a href=\"#\">Deutsch</a>\n							</li>\n						</ul>\n					</li>\n				</ul>\n				\n			</div>\n			\n		</div>\n	</div>\n</div>\n','',1359693468,1,'n',0,'','n','n','o',0),(11,1,2,'footer','y','webpage','<div class=\"span9 offset3\">\n	<div class=\"copyright well well-small\">\n		&copy; 2009-2013 - Artelux - <a href=\"#\" title=\"Disclaimer &amp; Privacy\">Disclaimer &amp; Privacy</a> - <a href=\"#\" title=\"Map du site\">Map du site</a>\n	</div>\n</div>','',1359693468,1,'n',0,'','n','n','o',0),(12,1,2,'jsinclusion','y','webpage','	<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js\"></script>\n	<script>window.jQuery || document.write(\'<script src=\"{theme_folder_url}artelux/js/vendor/jquery-1.8.3.min.js\"><\\/script>\')</script>\n	\n	<script src=\"{theme_folder_url}artelux/js/vendor/bootstrap.min.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/plugins.js\"></script>\n	\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox-buttons.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox-media.js\"></script>\n	<script src=\"{theme_folder_url}artelux/js/fancybox/jquery.fancybox-thumbs.js\"></script>\n	\n	<script src=\"{theme_folder_url}artelux/js/main.js\"></script>\n	\n	<script>\n		// var _gaq=[[\'_setAccount\',\'UA-XXXXX-X\'],[\'_trackPageview\']];\n		// (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n		// g.src=(\'https:\'==location.protocol?\'//ssl\':\'//www\')+\'.google-analytics.com/ga.js\';\n		// s.parentNode.insertBefore(g,s)}(document,\'script\'));\n	</script>','',1359693468,1,'n',0,'','n','n','o',0),(13,1,2,'sidebar','y','webpage','<!-- Main logo -->\n<div class=\"main-logo\">\n	<img src=\"{theme_folder_url}artelux/img/logo_artelux.png\" width=\"270\" height=\"215\" alt=\"Artelux\" title=\"Artelux | No light, no business\">\n	<h1 class=\"company\">Artelux</h1>\n	<h2 class=\"baseline\">No light, no business</h2>\n</div>\n\n<!-- Address -->\n<div class=\"contact-infos\">\n	<div class=\"well well-small\">\n		<i class=\"icon-map-marker\"></i> <span class=\"blued embossed-on-lightgrey\">Contactez-nous</span>\n	</div>\n	<div class=\"vcard\">\n		<div class=\"org\"><strong>Artelux</strong></div>\n		<a class=\"email\" href=\"mailto:info@artelux.be\">info@artelux.be</a>\n		<div class=\"adr\">\n			<div class=\"street-address\">La Picherotte, 28</div>\n			<span class=\"postal-code\">B-4053</span>, <span class=\"locality\">Embourg</span><br>\n			<span class=\"region\">Liège</span>, <span class=\"country-name\">Belgium</span>\n		</div>\n		<div class=\"tel\">+32 4 365 17 00</div>\n		<div class=\"tel\">+32 4 367 27 07</div>\n		\n	</div>\n	<p>Vous pouvez aussi remplir le <a href=\"#\">formulaire de contact</a>.</p>\n</div>\n\n<!-- Actus -->\n<div class=\"actu-widget\">\n	<div class=\"well well-small\">\n		<i class=\"icon-tag\"></i> <span class=\"blued embossed-on-lightgrey\">Actualités</span>\n	</div>\n	<ul>\n		<li>\n			20/05/2009 - <a href=\"#\" title=\"City Information and Prevention Screen : CIPS\">City Information...</a>\n		</li>\n		<li>\n			20/05/2009 - <a href=\"#\" title=\"Panneaux zone 30\">Panneaux \"Zone 30\"...</a>\n		</li>\n		<li>\n			20/05/2009 - <a href=\"#\" title=\"Afficheurs communaux Info Ville\">Afficheurs commu...</a>\n		</li>\n	</ul>\n</div>\n\n<!-- Newsletter -->\n<div class=\"newsletter-enroll\">\n	<div class=\"well well-small\">\n		<i class=\"icon-envelope\"></i> <span class=\"blued embossed-on-lightgrey\">Newsletter</span>\n	</div>\n	<p>Abonnez vous à notre newsletter et restez au courant des dernières nouveautés!</p>\n	\n	<form class=\"\">\n		<fieldset>\n			<legend>Newsletter</legend>\n			<label for=\"firstname\">Prénom</label>\n			<input class=\"input-medium\" type=\"text\" name=\"firstname\" id=\"firstname\" placeholder=\"Prénom\">\n			<label for=\"name\">Nom</label>\n			<input class=\"input-medium\" type=\"text\" name=\"name\" id=\"name\" placeholder=\"Nom\">\n			<label for=\"email\">Email</label>\n			<input class=\"input-medium\" type=\"text\" name=\"email\" id=\"email\" placeholder=\"Email\">\n			<br>\n			<button type=\"submit\" class=\"btn\">Envoyer</button>\n		</fieldset>\n	</form>\n</div>',NULL,1359693468,1,'n',0,'','n','n','o',0),(14,1,2,'breadcrumb','y','webpage','<ul class=\"breadcrumb\">\n	<li>\n		<a href=\"#\">Artelux</a> <span class=\"divider\">›</span>\n	</li>\n	<li class=\"active\">\n		Accueil\n	</li>\n</ul>',NULL,1359517981,1,'n',0,'','n','n','o',0),(15,1,4,'index','y','webpage','','',1359526924,1,'n',0,'','n','n','o',1),(16,1,4,'first_child_redirect','y','webpage','{exp:structure:first_child_redirect}','',1359563494,1,'n',0,'','n','n','o',0),(17,1,5,'index','y','webpage','','',1359563544,1,'n',0,'','n','n','o',0),(18,1,6,'index','y','webpage','','',1359563839,1,'n',0,'','n','n','o',0),(19,1,7,'index','y','webpage','','',1359563924,1,'n',0,'','n','n','o',0),(20,1,7,'detail','y','webpage','','',1359563946,1,'n',0,'','n','n','o',0),(21,1,4,'contact','y','webpage','','',1359572796,1,'n',0,'','n','n','o',0),(22,1,5,'item','y','webpage','',NULL,1359693458,1,'n',0,'','n','n','o',0),(23,1,5,'itemlist','y','webpage','{exp:structure:entries parent_id=\"{embed:entry_id}\"}\n						<p>{title}</p>\n						<p><a href=\"{page_uri}\">{page_uri}</a></p>\n						{/exp:structure:entries}','',1359730310,1,'n',0,'','n','n','o',0),(24,1,4,'sitemap','y','webpage','{embed=\"inc/head\"}\n<body id=\"index\" class=\"general\">\n	<div class=\"container\">\n		<div class=\"row\">\n			<div class=\"span3\">\n				{embed=\"inc/sidebar\"}\n			</div>\n			<div class=\"span9\">\n				{embed=\"inc/header\"}\n				\n				{embed=\"inc/breadcrumb\"}\n				\n				<div class=\"row\">\n					<div class=\"span6\">\n						{exp:channel:entries}\n						<div class=\"maincol top-product-container\">\n							{prod_contenu_{user_language}}\n						</div>\n						\n						{embed=\"produits/itemlist\" entry_id=\"{entry_id}\"}\n						\n						{/exp:channel:entries}\n					</div>\n					\n					<div class=\"span3\">\n						{exp:channel:entries}\n						<!-- Galerie -->\n						{if \"{prod_galerie:total_files}\" >= 1}\n						<ul class=\"thumbnails\">\n							{prod_galerie}\n							<li>\n								<a href=\"{url}\" class=\"thumbnail galerie-th fancybox\" rel=\"inpage-galerie\" title=\"{title}\">\n									{exp:ce_img:pair:crpd src=\"{url}\" crop=\"yes\" max_width=\"60\" min_width=\"60\" max_height=\"60\" min_height=\"60\"}\n									<img src=\"{crpd:made}\" title=\"{title}\" width=\"{crpd:width}\" height=\"{crpd:height}\" class=\"\" alt=\"{alt_text}\"/>\n									{/exp:ce_img:pair:crpd}\n								</a>\n							</li>\n							{/prod_galerie}\n						</ul>\n						{/if}\n						\n						<!-- Documents -->\n						{if \"{prod_documents:total_rows}\" >= 1}\n						<ul class=\"linked-docs\">\n							<h3>{exp:multi_language:phrase index=\"documents_lies\"}</h3>\n							{prod_documents}\n							<li>\n								<h4>\n									<i class=\"icon-file\"></i> <a href=\"{doc_file}\" class=\"doc-dl\" title=\"{exp:multi_language:phrase index=\"telecharger\"} : {doc_title}\" data-placement=\"left\">{doc_title}</a>\n								</h4>\n								{doc_desc}\n							</li>\n							{/prod_documents}\n						</ul>\n						{/if}\n						{/exp:channel:entries}\n					</div>\n					\n				</div>\n			</div>\n		</div>\n		<div class=\"row\">\n			{embed=\"inc/footer\"}\n		</div>\n	</div>\n	\n	{embed=\"inc/jsinclusion\"}\n</body>\n</html>\n','',1363927156,1,'n',0,'','n','n','o',0);
/*!40000 ALTER TABLE `exp_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_throttle`
--

DROP TABLE IF EXISTS `exp_throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_throttle`
--

LOCK TABLES `exp_throttle` WRITE;
/*!40000 ALTER TABLE `exp_throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_upload_no_access`
--

DROP TABLE IF EXISTS `exp_upload_no_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_upload_no_access`
--

LOCK TABLES `exp_upload_no_access` WRITE;
/*!40000 ALTER TABLE `exp_upload_no_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `exp_upload_no_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_upload_prefs`
--

DROP TABLE IF EXISTS `exp_upload_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_upload_prefs`
--

LOCK TABLES `exp_upload_prefs` WRITE;
/*!40000 ALTER TABLE `exp_upload_prefs` DISABLE KEYS */;
INSERT INTO `exp_upload_prefs` VALUES (1,1,'Global (Images)','/Users/seba/Documents/Workspace/artelux/images/pages/','http://local.artelux.be/images/pages/','img','','','','','','','','','','',NULL),(2,1,'Global (Documents)','/Users/seba/Documents/Workspace/artelux/docs/','http://local.artelux.be/docs/','all','','','','','','','','','','',NULL),(3,1,'Items (Produits)','/Users/seba/Documents/Workspace/artelux/images/uploads/items/','http://local.artelux.be/images/uploads/items/','img','','','','','','','','','','1',NULL),(4,1,'Produits','/Users/seba/Documents/Workspace/artelux/images/uploads/products/','http://local.artelux.be/images/uploads/products/','img','','','','','','','','','','1',NULL),(5,1,'Services','/Users/seba/Documents/Workspace/artelux/images/uploads/services/','http://local.artelux.be/images/uploads/services/','img','','','','','','','','','','',NULL),(6,1,'Actualités','/Users/seba/Documents/Workspace/artelux/images/uploads/actus/','http://local.artelux.be/images/uploads/actus/','all','','','','','','','','','','',NULL);
/*!40000 ALTER TABLE `exp_upload_prefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exp_wygwam_configs`
--

DROP TABLE IF EXISTS `exp_wygwam_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exp_wygwam_configs`
--

LOCK TABLES `exp_wygwam_configs` WRITE;
/*!40000 ALTER TABLE `exp_wygwam_configs` DISABLE KEYS */;
INSERT INTO `exp_wygwam_configs` VALUES (1,'Basic','YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),(2,'Full','YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ=='),(3,'Artelux','YTo0OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e319');
/*!40000 ALTER TABLE `exp_wygwam_configs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-03-22  6:11:48
